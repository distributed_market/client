import argparse
import os,sys
os = os

import pathlib

if __name__ == '__main__':
    from sys import argv
    __file__ = pathlib.Path(__file__).resolve()
    path = os.path.dirname(__file__)
    modules = [m for m in os.listdir(path)
               if m != os.path.basename(__file__)]
    ap = argparse.ArgumentParser()
    ap.add_argument('module',
                    choices=[*modules,'*','all'],
                    help=f'runs specified module with stdin arguments')
    pa = ap.parse_args(argv[1:2])

    if pa.module in modules: modules = [pa.module]
    args = " ".join(argv[2:])
    for module in modules:
        print(f'running {module.upper()} with arguments: {args}')
        os.system(LINE.format(path,sys.executable,module,args))
        #os.system(f'PYTHONPATH="{path}" "{sys.executable}" -m {module} {args}')
