from .core import DBStructure,root_logger
import logging

class Account(DBStructure):
    __read__   = 'LSTACC'
    __create__ = 'CRTACC'
    __update__ = 'UPDACC'
    __delete__ = 'DELACC'

    __logger__ = logging.getLogger(f'{root_logger.__name__}.account')
    __credentials__ = {}

    @staticmethod
    def credentials(name): 
        Account.__logger__.debug(f'fetching credentials for {name}')
        return Account.__credentials__.get(name,None)
    @staticmethod
    def store(name:str,passwd:str,*,force=False):
        Account.__logger__.info(f'storing credentials for {name}...')
        if not isinstance(name,str) and isinstance(passwd,str):
            raise ValueError(f'invalid entry: {name}:{passwd}')
        if name in Account.__credentials__ and not force:
            raise KeyError(f'credentials for {name} already exist')
        Account.__credentials__[name] = passwd
    
    @staticmethod
    def login(name,passwd=None):
        if passwd is None: passwd = Account.credentials(name)
        if getattr(Account,'__conn__',None) is None:
            Account.__logger__.error('no active connection present')
            return False
        return {} == Account.__conn__.exch('LOGACC',
                                           {'name':name,
                                            'passwd':passwd})

    __slots__ = ['name','passwd','home']

