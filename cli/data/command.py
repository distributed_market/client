from .core import DBStructure,root_logger
import logging

class Command(DBStructure):
    __read__   = 'LSTCMD'
    
    __logger__ = logging.getLogger(f'{root_logger.name}.command')

    @staticmethod
    def help(command=None):
        if getattr(Command,'__conn__',None) is None:
            Command.__logger__.error('no active connection present')
            return None
        return Command.__conn__.exch('HELP',{'command':command} 
                                            if not command is None
                                            else {})

    __slots__ = ['code','name','desc']
