import os
from ..util import logging
root_logger = logging.getLogger(__package__,os.path.join('cli','database.log'))

class StructDict(dict):
    __slots__ = []
    def __setitem__(self,key,val):
        if key in self.__class__.__slots__:
            super().__setitem__(key,val)
        else: raise KeyError(f'no field ${key} allowed')
    def __setattr__(self,attr,val):
        try: self[attr] = val
        except KeyError: 
            raise AttributeError(f'class {self.__class__} has no attribute {attr}')
    def __getattr__(self,attr):
        try: return self[attr]
        except KeyError: 
            raise AttributeError(f'class {self.__class__} has no attribute {attr}')
    def __init__(self,**kwargs):
        for k,v in kwargs.items():
            self[k] = v

class DBStructure(StructDict):
    SELECT = 'SELECT {} FROM {} WHERE {} ORDER BY {}'
    INSERT = 'INSERT INTO {} VALUES {}'
    UPDATE = 'UPDATE {} SET {} WHERE {}'
    DELETE = 'DELETE FROM {} WHERE {}'

    @classmethod
    def read(cls,select=['*'],filter={},sorter={}):
        logger = logging.getPlainLogger(f'{cls.__logger__.name}.read')
        if getattr(cls,'__read__',None) is None: 
            e = AttributeError('no control message mapping for SELECT')
            logger.error(e)
            raise e
        logger.info(f'SELECT mapped: {cls.__read__}')
        SELECT = DBStructure.SELECT.format(select,cls.__name__,filter,sorter)
        logger.info(SELECT)
        if getattr(cls,'__conn__',None) is None: 
            logger.error(f'no active connection present')
            return None
        try: return [cls(**dbdata) for dbdata in 
                            cls.__conn__.exch(cls.__read__,
                                              {'select':select,
                                               'filter':filter,
                                               'sorter':sorter})]
        except Exception as e:
            logger.error(f'{SELECT} failed\n{"":<21}{f"{e._class__}: {e}":.<59}')
            return None

    @classmethod
    def create(cls,*argslist):
        logger = logging.getPlainLogger(f'{cls.__logger__.name}.create')
        if getattr(cls,'__create__',None) is None: 
            e = AttributeError('no control message mapping for INSERT')
            logger.error(e)
            raise e
        logger.info(f'INSERT mapped: {cls.__create__}')
        ins = 0
        for args in argslist:
            INSERT = DBStructure.INSERT.format(cls.__name__,args)
            logger.info(INSERT)
            if getattr(cls,'__conn__',None) is None: 
                logger.error(f'no active connection present')
                return ins
            try: 
                if {} == cls.__conn__.exch(cls.__create__,args):
                    ins += 1
            except Exception as e:
                logger.error(f'{INSERT} failed\n{"":<21}{f"{e._class__}: {e}":.<59}')
        return ins 

    @classmethod
    def update(cls,mapping,filter={}):
        logger = logging.getPlainLogger(f'{cls.__logger__.name}.update')
        if getattr(cls,'__update__',None) is None:
            e = AttributeError('no control message mapping for UPDATE')
            logger.error(e)
            raise e
        logger.info(f'UPDATE mapped: {cls.__update__}')
        UPDATE = DBStructure.UPDATE.format(cls.__name__,mapping,filter)
        logger.info(UPDATE)
        upd = 0
        ids = cls.read(select=['id'],filter=filter)
        if not ids is None:
            for id in ids:
                if getattr(cls,'__conn__',None) is None:
                    logger.error('no active connection present')
                    return upd
                try:
                    if {} == cls.__conn__.exch(cls.__update__,
                                               {**_id,**mapping}):
                        upd += 1
                except Exception as e:
                    logger.error(f'{INSERT} failed\n{"":<21}{f"{e._class__}: {e}":.<59}')
        return upd

    @classmethod
    def delete(cls,filter={}):
        logger = logging.getPlainLogger(f'{cls.__logger__.name}.delete')
        if getattr(cls,'__delete__',None) is None: 
            e = AttributeError('no control message mapping for DELETE')
            logger.error(e)
            raise e
        logger.info(f'DELETE mapped: {cls.__delete__}')
        DELETE = DBStructure.DELETE.format(cls.__name__,filter)
        logger.info(DELETE)
        dlt = 0
        ids = cls.read(select=['id'],filter=filter)
        if not ids is None:
            for id in ids:
                if getattr(cls,'__conn__',None) is None:
                    logger.error('no active connection present')
                    return dlt
                try:
                    if {} == cls.__conn__.exch(cls.__delete__,id):
                        dlt += 1
                except Exception as e:
                    logger.error(f'{INSERT} failed\n{"":<21}{f"{e._class__}: {e}":.<59}')
        return dlt

    @classmethod
    def notify(cls,sender,*args):
        cls.__logger__.info(f'notification from {sender}: {args}')
        if args[0] == 0:
            cls.__conn__ = sender
        else:
            cls.__conn__ = None
