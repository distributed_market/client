from .core import DBStructure,root_logger
import logging

class Auction(DBStructure):
    __read__   = 'LSTAUC'
    __create__ = 'CRTAUC'
    __update__ = 'UPDAUC'
    __delete__ = 'DELAUC'

    __logger__ = logging.getLogger(f'{root_logger.name}.auction')

    __slots__ = ['id','retention','title','ppu','vendor',
                 'min_q','max_q','descr','photos','category']
