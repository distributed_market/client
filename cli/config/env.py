import os
from . import config

## reads specified values from OS.GETENV to ENV section
def read_conf():
    config.add_section('env')
    for ev in config.get('global','RETENV',fallback='').split(','):
        config.set('env',ev,os.getenv(ev))

def validate_conf():
    pass
