import os
import sys
import importlib

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.WARN)

from ..errors import ConfigError

from .core import validate_sect,config
config.add_section('runtime')

__loaded_modules__ = {}

for modname in ['globals','env','paths']:
    try:
        confmod = importlib.import_module(f'.{modname}',__package__)
        confmod.read_conf()
    except Exception as e:
        raise ConfigError(f'{e.__class__} reading {modname} config') from e
    __loaded_modules__[modname] = confmod

for modname in config.get('global','CONFIGMODS').split(','):
    try:
        confmod = importlib.import_module(f'.{modname}',__package__)
        confmod.read_conf()
    except (AttributeError,ModuleNotFoundError) as e:
        ## if specified module doesn't exist or doesn't declare *_conf
        ## functions, skip module (without notice?)
        logger.error(f'{e.__class__} while reading module {modname}: {e}')
    except Exception as e:
        raise ConfigError(f'error reading config: {modname}') from e
    else: __loaded_modules__[modname] = confmod

for modname,confmod in __loaded_modules__.items():
    try: confmod.validate_conf()
    except AttributeError as e:
        ## if specified module doesn't declare *_conf
        ## functions, skip validation (without notice?)
        logger.error(f'{e.__class__} while verifying module {modname}: {e}')
    except Exception as e:
        raise ConfigError(f'error verifying config: {modname}') from e

config.set('runtime','CONFIGMODS',
           ','.join(__loaded_modules__.keys()))
