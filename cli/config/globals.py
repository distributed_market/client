import os

from . import config

def read_conf():
    with open(os.path.join(
                os.path.abspath(
	            os.path.dirname(__file__)),
	        'assets',
	        'global.conf'),
              'r') as conf_file:
        config.read_file(conf_file)

def validate_conf():
    pass
