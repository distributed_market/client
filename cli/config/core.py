import configparser

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.WARN)

from ..errors import ConfigError

config = configparser.ConfigParser(
                        allow_no_value=True,
                        empty_lines_in_values=False,
                        interpolation=configparser.ExtendedInterpolation(),
                        converters={'list':lambda arg:arg.split(',')})

def validate_sect(sect,fields):
    if not config.has_section(sect):
        e = ConfigError(f'missing config section{sect}')
        logger.error(e)
        raise e
    for field in fields:
        if not config.has_option(sect,field):
            e = ConfigError(f'missing config option: {sect,field}')
            logger.error(e)
            raise e


