import os
from . import config

def read_conf():
    paths = [i for i in [
                os.getenv('DISTORTION_CONF'),
                config.get('global','DISTORTION_CONF',fallback=None),
                os.path.join(
                    os.path.abspath(
                        os.path.dirname(__file__)),
                    'assets',
                    'daemon.conf')]
                if i]
    for conf_path in paths:
        conf_file = open(conf_path,'r')
        with conf_file:
            config.read_file(conf_file)
            break
    else:
        raise FileNotFoundError('daemon.conf not found under {paths}')

def validate_conf():
#    for field in ['DATA_DIR',
#                  'INSTALL_DIR',
#                  'CONFIG_DIR',]:
#                  #'LOGGING_DIR',
#                  #'WORKING_DIR']:
#        dir = config.get('paths',field)
#        assert os.path.exists(dir),f'{dir} does not exist'
    pass
