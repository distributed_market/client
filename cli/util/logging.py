import typing

from logging import getLogger as getPlainLogger
getPlainLogger = getPlainLogger
from logging import StreamHandler,FileHandler,Formatter, \
                    FATAL,CRITICAL,ERROR,WARNING,INFO,DEBUG

import os,sys
os = os

from ..config import config

STDOUT_HANDLER = StreamHandler(sys.stdout)

__date_fmt__ = '%Y-%m-%d %H:%M:%S'
__out_fmt__  = '[{asctime}]{name:.<49}[{levelname:^8}]\n                     {message:.<59}'
FORMATTER = Formatter(__out_fmt__,__date_fmt__,style='{')

def getLogger(name,path=None,mode='a'):
    logger = getPlainLogger(name)
    if path is None: path = f'{name.split(".")[-1]}.log'
    if hasattr(path,'write'): handler = StreamHandler(path)
    else:
        path = os.path.join(config.get('paths','LOGGING_DIR'),path)
        try: 
            parent = os.path.abspath(os.path.dirname(path))
            os.makedirs(parent,exist_ok=True)
            if os.path.exists(path):
                with open(path,'r') as infile, \
                     open(f'{path}.bak','w') as outfile:
                    outfile.write('# LAST RUNTIME LOG #\n')
                    outfile.write(infile.read())
            handler = FileHandler(path,mode)
        except: handler = STDOUT_HANDLER
    handler.setFormatter(FORMATTER)
    logger.handlers = [handler]
    logger.critical('LOGGER INITIALIZED')
    logger.propagate = False
    return logger
