import math
import socket

import sys,os,io
sys = sys
os = os
io = io

import logging
logger = logging.getLogger(__package__)
logger.propagate = False
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(logging.Formatter('[{levelname:^8}] {message}',style='{'))
logger.addHandler(handler)

from ..errors import MessageParsingError,ConnectionClosedError

from .header import MAX_LENGTH,MAX_BLOCKS,HeaderFactory,Header
from .json import encode,decode

class APIConnection(object):
    CLOSED = 1
    OPEN   = 0

    def notify(self):
        logger.debug('notifying observers')
        for o in self.__observers__:
            try: o.notify(self,self.state)
            except AttributeError: pass
    def add_observer(self,obj):
        logger.debug(f'adding observer {obj}')
        self.__observers__.add(obj)
        obj.notify(self,self.state)

    stype = { 'PORT' : socket.AF_INET,
              'SOCK' : socket.AF_UNIX }
    saddr = { 'PORT' : lambda arg: ('127.0.0.1',int(arg)),
              'SOCK' : lambda arg: os.path.abspath(arg) }

    def __init__(self,channel,fd):
        self.__observers__ = set()
        self.socktype = APIConnection.stype[channel]
        self.sockaddr = APIConnection.saddr[channel](fd)
        self.state = APIConnection.CLOSED
        self.Header = Header

    def open(self):
        logger.info('estabilishing connection')
        if self.state != APIConnection.CLOSED:
            raise RuntimeError('connection already open')

        self.s = socket.socket(self.socktype)
        self.s.connect(self.sockaddr)
        logger.info(f'connection estabilished: {self.s}')

        self.state = APIConnection.OPEN

        logger.info('sending INIT')
        self.send('INIT',{})
        try: 
            data = self.recv()
            self.Header = HeaderFactory(**{k,v for k,v in data.items()
                                           if k in ['MAX_LENGTH',
                                                    'MAX_BLOCKS',
                                                    'LONG_CODES']})
        except Exception as e:
            logger.error(f'failed to set header to {data}')
            self.Header = Header

        self.notify()

    def close(self):
        logger.info('closing connection')
        if self.state != APIConnection.OPEN:
            raise RuntimeError('connection already closed')
        self.state = APIConnection.CLOSED
        self.notify()
        self.s.close()

    def exch(self,t,msg):
        self.send(t,msg)
        return self.recv()

    def send(self,t,msg):
        logger.info('sending data via connection...')
        if self.state != APIConnection.OPEN:
            raise RuntimeError('connection closed')
        msg = encode(msg)
        with io.BytesIO() as data:
            data.write(bytes(msg,'utf-8'))              # dla wygody zapisanie danych do pseudopliku z kursorem do odczytu
            rest = data.tell()                          # przechowanie pełnej długości strumienia jako ilości pozostałych bajtów
            data.seek(0)                                # przewinięcie kursora do odczytu na początek
 
            bloc = math.ceil(rest/4096)                 # ilość bloków = długość(poz. kursora po zapisie całego łańcucha)/max_długość
            leng = min(4096,rest)                       # długość pierwszego bloku = max_długość lub rest, zależnie co mniejsze
            
            while bloc >= 1:                            # jeżeli blok == 1, wysyłka musi się odbyć co najmniej raz
                head = self.Header(blocks=bloc,length=leng,cntype=3,mscode=t)
                #head = bytes(f'{leng:<4}{bloc:<1}3{t:<6}',
                #             'utf-8')                   # header konstruowany z długości kolejnego bloku, ilości pozostałych bloków etc
                bloc -= 1
                logger.info(f'sending header: {head}')
                self.s.send(head)
                chunk = data.read(leng)                 # pobranie z bufora wszystkich znaków kolejnego bloku
                logger.debug(f'sending chunk: {chunk}')
                self.s.send(chunk)

                rest -= leng                            # uwzględnienie faktu wysłania bloku
                leng = min(4096,rest)                   # długość kolejnego bloku analogicznie do długości pierwszego

    def getbytes(self,target=None,*,accept=None):
        header = self.conn.recv(self.Header.SIZE)
        if not header: raise ConnectionClosedError()
        try: 
            header = self.Header(header)
            logger.info(f'received header: {bytes(head)}')
        except Exception as e: raise MessageParsingError(e) from e

        if not accept is None and not header.mstype & accept:
            raise MessageParsingError(f'invalid message: {header.mstype}')

        if not target is None:
            target.write(self.conn.recv(header.length))
            self.logger.debug(f'received chunk: {target.getvalue()}')
        else:
            target = self.conn.recv(header.length)
            self.logger.debug(f'received chunk: {target}')
        return header,target

    def recv(self):
        logger.info('reading data from connection...')
        if self.state != APIConnection.OPEN:
            raise RuntimeError('connection closed')
        with io.BytesIO() as data:
            try:
                header,_ = self.getbytes(data)
                while header.blocks > 1:
                    header.blocks -= 1
                    logger.info(f'awaiting block @{header.blocks}')
                    pheader,_ = self.getbytes(data,accept=header.mstype)
                logger.debug(f'received content: {data.getvalue()}')
                resp = decode(data.getvalue())
                if header.mscode == 'ERR':
                    raise DistortionCliError(
                            f'{resp["cause"]}: {resp["message"]}')
            except MessageParsingError as e:
                logger.warning(f'packet parsing error: {e}')
                try: self.send('ERR',{'cause':e.__class__,'message':str(e)})
                except: pass
