import enum

@enum.unique
class DataType(enum.IntFlag):
    STR = 1
    BIN = 2
    OBJ = 3
    RAW = 4

class MSG(object):
    __by_name__ = {
    }
    __by_desc__ = {
    }

    @staticmethod
    def register(name,code,desc=None):
        __by_name__[name] = code
        if not desc is None:
            __by_desc__[desc] = code

    @staticmethod
    def __find__(code):
        try: return 1<<int(code)
        except: pass
        try: return 1<<__by_name__[code]
        except: pass
        try: return 1<<__by_desc__[code]
        except: pass
        raise KeyError(f'codename {code} unknown')
