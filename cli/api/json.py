import json
json = json

def _default(o):
    if isinstance(o,type):
        return o.__name__
    if isinstance(o,(str,dict,list)):
        return json.JSONEncoder.default(o)
    if hasattr(o,'__iter__'):
        return list(iter(o))
    return json.JSONEncoder.default(o)

__encoder__ = json.JSONEncoder(separators=(',',':'),
                               default=_default)
__decoder__ = json.JSONDecoder()

def encode(data:dict=None):
    res = b'{}'
    if not data is None:
        res = bytes(__encoder__.encode(data),'utf-8')
    return res

def decode(data:bytes=None):
    res = {}
    if not data is None:
        res = __decoder__.raw_decode(data.decode('utf-8'))[0]
    return res
