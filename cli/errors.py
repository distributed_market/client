class DistortionError(RuntimeError):
    pass

class ConfigError(DistortionError):
    pass

class DistortionCliError(DistortionError):
    pass

class UnknownCommandError(DistortionCliError):
    pass

class MalformedCommandError(DistortionCliError):
    pass

class MissingArgumentError(MalformedCommandError):
    pass

class UnknownArgumentError(MalformedCommandError):
    pass

class IllegalArgumentError(MalformedCommandError):
    pass

class MessageParsingError(DistortionCliError):
    pass

class ConnectionClosedError(DistortionCliError):
    pass
