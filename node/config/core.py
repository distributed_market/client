import configparser

import logging
logger = logging.getLogger(__name__)

from ..errors import ConfigError,LoginStatusError

__unspec__ = object()

def validate_sect(sect,fields):
    from . import config
    if not config.has_section(sect):
        e = ConfigError(f'missing config section{sect}')
        logger.error(e)
        raise e
    for field in fields:
        if not config.has_option(sect,field):
            e = ConfigError(f'missing config option: {sect,field}')
            logger.error(e)
            raise e

class CP(object):
    __kwargs__ = {
            'allow_no_value' : True,
            'empty_lines_in_values' : False,
            'interpolation' : configparser.ExtendedInterpolation(),
            'converters' : {
                'list' : lambda arg:arg.split(',')
            }
        }

    @classmethod
    def get_cp(cls,conf=None,kwargs=None):
        res = configparser.ConfigParser(**CP.__kwargs__)
        res.optionxform = str
        if not conf is None: 
            try: res.read(conf)
            except Exception as e:
                pass
        if not kwargs is None:
            try: res.read_dict(kwargs)
            except Exception as e:
                pass
        return res

    def __init__(self):
        self.cattree = None
        self.cert = None
        self.__globals__ = CP.get_cp()
        self.__userloc__ = CP.get_cp()
        self.logger = logger

    def check(self):
        return self.__globals__.get('runtime','USER',fallback=None) != None

    def get_user(self):
        if self.__globals__['runtime']['STATE'] in ('ON','OFF'):
            raise LoginStatusError('not logged in')
        return self.__globals__['runtime']['USER'],\
                self.__globals__['runtime']['HOME'],\
                 self.__globals__['runtime']['PASS']

    def get(self,section,option,*,raw=False,vars=None,fallback=__unspec__):
        self.logger.debug(f'GET request for: {section}, {option}')
        if section == 'runtime':
            if option == 'CATEGORIES':
                return self.cattree
            if option == 'CERT':
                return self.cert
            return self.__globals__.get(section,option,raw=raw,vars=vars,fallback=None)
        try:
            try:
                if self.check(): return self.__userloc__.get(section,option,
                                                             raw=raw,vars=vars)
            except Exception as e: 
                #pass
                self.logger.debug(f'{e.__class__} on GET#{section},{option}: {e}')
            return self.__globals__.get(section,option,raw=raw,vars=vars)
        except Exception as e:
            self.logger.debug(f'{e.__class__} on GET#{section},{option}: {e}')
            if not fallback is __unspec__: return fallback
            raise

    def set(self,section,option,value):
        self.logger.debug(f'SET request for: {section}, {option}, {value}')
        if section == 'runtime':
            if option == 'CATEGORIES':
                self.cattree = value
            elif option == 'CERT':
                self.cert = value
            elif option == 'STATE':
                if self.get(section,option) == 'OFF':
                    self.logger.error('cannot interrupt exit procedure')
                    return
                self.logger.info(f'state changed to {value}')
                self.__globals__.set(section,option,value)
                if value != 'OFF': self.dump()
            elif option == 'USER':
                if value is None:
                    self.logger.info('user unset')
                    self.__userloc__.set(section,option,None)
                    self.__globals__.set(section,option,None)
                    self.__userloc__.set(section,'HOME',None)
                    self.__globals__.set(section,'HOME',None)
                    self.__userloc__.set(section,'PASS',None)
                    self.__globals__.set(section,'PASS',None)
                    self.set(section,'STATE','ON')
                else:
                    self.__userloc__.set(section,option,value)
                    self.__globals__.set(section,option,value)
                    self.set(section,'STATE','OFFLINE')
            else: 
                self.__globals__.set(section,option,value)
                self.__userloc__.set(section,option,value)
        else:
            if self.check(): self.__userloc__.set(section,option,value)
            else: self.__globals__.set(section,option,value)
   
    def read(self,path):
        try:
            with open(path,'r') as file:
                self.read_file(file)
        except Exception as e:
            self.logger.error(f'{e.__class__} reading from {path}: {e}')

    def read_file(self,file):
        try:
            if self.check(): self.__userloc__.read_file(file)
        except: pass
        self.__globals__.read_file(file)

    def add_section(self,section):
        self.__globals__.add_section(section)
        self.__userloc__.add_section(section)

    def has_section(self,section):
        return self.__globals__.has_section(section)

    def has_option(self,section,option):
        try: self.get(section,option,raw=True)
        except: return False
        return True

    def sections(self): return self.__globals__.sections()

    def options(self,section):
        self.logger.debug(f'{section} options: {self.__globals__.options(section)}')
        return self.__globals__.options(section)

    def write(self,fd):
        self.logger.debug(f'writing raw config to {fd}...')
        for s in self.sections():
            self.logger.debug(f'section: {s}')
            fd.write(f'\n[{s}]\n')
            for o in self.options(s):
                try:
                    val = self.get(s,o,raw=True)
                    self.logger.debug(f'option: {o} = {val}')
                    fd.write(f'{o} = {val}\n')
                except Exception as e:
                    self.logger.warning(f'{e.__class__} while writing raw config: {e}')
        #fd.close()

    def dump(self):
        self.logger.debug(f'writing runtime config...')
        try:
            with open(self.get('paths','CONFIGFILE'),'w') as fd:
                for s in self.sections():
                    self.logger.debug(f'section: {s}')
                    fd.write(f'\n[{s}]\n')
                    for o in self.options(s):
                        if o == 'PASS': continue
                        try:
                            val = self.get(s,o)
                            self.logger.debug(f'option: {o} = {val}')
                            fd.write(f'{o} = {val}\n')
                        except configparser.InterpolationError: pass
                        except Exception as e:
                            self.logger.warning(f'{e.__class__} while writing config: {e}')
        except Exception as e:
            self.logger.error(f'{e.__class__} while writing config: {e}')
