import os
import socket

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.WARN)

from ..errors import ConfigError

from . import config,validate_sect

def read_conf():
    try:
        with open(os.path.join(config.get('paths','CONFIG_DIR'),
                                'node.conf'),'r') as conf_file:
            config.read_file(conf_file)
    except OSError as e:
        logger.error(e)
        raise

def validate_tor():
    validate_sect('tor',['TOR_PORT','TOR_PASS','THS_DIR','THS_PORT','CHANNEL'])
    channel = config.get('tor','CHANNEL')
    chaddrs = config.get('tor',channel,fallback=None)
    if chaddrs is None:
        e = ConfigError(f'missing config option: tor.{channel}')
        logger.warn(str(e))
        raise e
    _args = { 'PORT' : ((),('',int(chaddrs))) }
    try: _args['SOCK'] = ((socket.AF_UNIX,),chaddrs)
    except: pass

    s = socket.socket(*_args[channel][0])
    try: s.bind(_args[channel][1])
    except Exception as e:
        logger.error(e)
        raise
    finally: s.close()

def validate_sync():
    validate_sect('synchronization',['MODE'])
    mode = config.get('synchronization','MODE')
    if mode == 'MANUAL':
        pass
    elif mode == 'STATIC':
        if not config.has_option('synchronization','RELAY'):
            e = ConfigError('missing config option: synchronization.RELAY')
            logger.error(e)
            raise e
    elif mode == 'REMOTE':
        if not config.has_option('synchronization','URL'):
            e = ConfigError('missing config option: synchronization.URL')
            logger.error(e)
            raise e
    else: 
        e = ValueError(f'invalid synchronization.MODE value: {mode}')
        logger.error(e)
        raise e

def validate_sec():
    validate_sect('security',['CERT','PGP_KEY_DIR',
                              'HS_SRC','HS_KEX','HS_KEX_MLEN',
                              'HS_ENC','HS_DGST'])
def validate_conf():
    validate_tor()
    validate_sync()
    validate_sec()
