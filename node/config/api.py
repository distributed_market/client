import os
from . import config

def read_conf():
    paths = [os.path.join(
                 config.get('paths','CONFIG_DIR'),
                 'api.conf'),
             os.path.join(
                 config.get('paths','CONFIG_DIR'),
                 'default',
                 'api.conf')]
    for conf_path in paths:
        with open(conf_path,'r') as conf_file:
            config.read_file(conf_file)
            break
    else:
        raise FileNotFoundError(f'cli.conf not found under {paths}')

def validate_conf():
    lang  = config.get('transmission','DATA')
    avail = config.get('global','DATALANG').split(',')
    if not lang in avail:
        raise TypeError(f'invalid CLI data language {lang}, available: {avail}')
