from .errors import ConfigError
from .config import config
from .util.args import add_noarg_opt 

from .util import logging
from . import logger as root_logger

import argparse,sys
argparse = argparse 

import os
os = os

import signal,time
signal = signal
time = time


def exit(*args):
    if config.get('runtime','STATE') == 'OFF':
        logger.info('already exiting')
        return
    config.set('runtime','STATE','OFF')
    from .api.ipc.server import APIServer
    from .account.session import logout
    try: logout()
    except RuntimeError: pass
    logger.critical(f'PROCESS {os.getpid()} EXITING')
    servs = []
    with APIServer.__lock__:
        for server in APIServer.__running__:
            logger.critical(f'stopping {server}')
            try: 
                server.stop()
                servs.append(server)
            except Exception as e:
                logger.error(f'{e.__class__} while stopping {server}: {e}')
    for server in servs:
        logger.info(f'joining {server}')
        try: server.join(3)
        except Exception as e:
            logger.error(f'{e.__class__} while joining {server}: {e}')
    try: os.remove(config.get('paths','CONFIGFILE'))
    except:
        logger.warning(f'failed to remove: {config.get("paths","CONFIGFILE")} (manual cleanup may be required)')
    try: os.remove(config.get('paths','PIDFILE'))
    except Exception as e:
        logger.critical(f'failed to remove: {config.get("paths","PIDFILE")} (manual cleanup may be required)')
    try:
        logger.info(f'cleaning {config.get("paths","TMP_DIR")}')
        for x in os.listdir(os.get('paths','TMP_DIR')):
            logger.info(f' - entry {x}')
            try: os.remove(os.path.join(config.get('paths','TMP_DIR'),x))
            except Exception as e:
                logger.warning(f'failed to remove: {x} (manual cleanup may be required)')
    except: pass
    sys.exit()

def __prep_env__():
    global logger
    logger = logging.getLogger(f'{root_logger.name}.daemon','daemon.log','w')
    for path in ('LOGGING_DIR','WORKING_DIR','TMP_DIR'):
        if not os.path.exists(config.get('paths',path)):
            try:
                os.makedirs(config.get('paths',path))
                if config.get('system','PLATFORM') != 'Windows':
                    task = os.popen(
                        f'chown -vR {os.geteuid()}:{os.getgid()} {config.get("paths",path)}')
                    logger.info(task.read())
                    if not task.close() in (0,None): 
                        raise OSError(f'failed to chown {path}')
                    task = os.popen(f'chmod -vR 2770 {config.get("paths",path)}')
            except Exception as e:
                logger.warning(f'failed to create {path}')
                logger.warning(f'{e.__class__.__name__}: {e}')
                raise
    from .util.security import SecurityModule
    SecurityModule.reload()
    os.chdir(config.get('paths','WORKING_DIR'))
                

def __prep_serv__(attached):
    logger.info('preparing main UI server...')
    from .api.ipc.server import SERVERS
    from .api.ipc.connection import EncryptedDaemonConnection,\
                                    EncryptedGUIConnection,PlainAPIConnection
    encrypt = config.get('transmission','ENCRYPT',fallback='remote')
    logger.info(f'encryption level: {encrypt}')
    if encrypt == 'all':
        for _,v in SERVERS['ui'].items(): v.set_conn_params(EncryptedGUIConnection)
        for _,v in SERVERS['node'].items(): v.set_conn_params(EncryptedDaemonConnection)
        for _,v in SERVERS['relay'].items(): v.set_conn_params(EncryptedDaemonConnection)
    elif encrypt == 'local':
        SERVERS['ui']['attached'].set_conn_params(PlainAPIConnection)
        SERVERS['ui']['detached'].set_conn_params(EncryptedGUIConnection)
        for _,v in SERVERS['node'].items(): v.set_conn_params(EncryptedDaemonConnection)
        for _,v in SERVERS['relay'].items(): v.set_conn_params(EncryptedDaemonConnection)
    elif encrypt == 'remote':
        for _,v in SERVERS['ui'].items(): v.set_conn_params(PlainAPIConnection)
        for _,v in SERVERS['node'].items(): v.set_conn_params(EncryptedDaemonConnection)
        for _,v in SERVERS['relay'].items(): v.set_conn_params(EncryptedDaemonConnection)
    elif encrypt == 'none':
        for _,v in SERVERS['ui'].items(): v.set_conn_params(PlainAPIConnection)
        for _,v in SERVERS['node'].items(): v.set_conn_params(PlainAPIConnection)
        for _,v in SERVERS['relay'].items(): v.set_conn_params(PlainAPIConnection)
    else:
        e = ConfigError(f'invalid encryption level: {encrypt}/all|local|remote|none')
        logger.critical(f'{e.__class__}: {e}')
        raise e
    GUI = SERVERS['ui']['attached'] if attached else SERVERS['ui']['detached']
    GUI.set_socket(config.get('transmission','CHANNEL'),
                   config.get('transmission',config.get('transmission','CHANNEL')))
    logger.info(f'GUI server class: {GUI.__class__}')
    GUI.start()

import time
def run():
    __prep_env__()
    try:
        with open(config.get('paths','PIDFILE'),'r') as pidfile:
            pid = int(pidfile.read())
        try: os.kill(pid,0)
        except:
            logger.info(f'no process {pid} exists, removing PIDFILE')
            os.remove(config.get('paths','PIDFILE'))
        else: raise OSError(f'another instance is already running: {pid}')
    except (FileNotFoundError,ValueError): pass

    if config.get('system','PLATFORM')!='Windows':
        for s in signal.SIGINT,signal.SIGQUIT,signal.SIGTERM:
            try:
                logger.info(f'masking signal {s}')
                signal.signal(s,exit)
            except Exception as e:
                logger.warning(f'{e.__class__} while masking signal {s}: {e}')
    
    pid = str(os.getpid())
    config.set('paths','CONFIGFILE',
               os.path.join(config.get('paths','TMP_DIR'),'runtime.conf'))
    try:
        __prep_serv__(config.get('runtime','ATTACHED',fallback='False')=='True')
        config.set('runtime','STATE','ON')
        while True:
            if os.path.exists(os.path.join(config.get('paths','TMP_DIR'),'.exit')):
                with open(os.path.join(config.get('paths','TMP_DIR'),'.exit'),
                          'r') as ownerfile: 
                    logger.critical(f'exit request on behalf of process {ownerfile.read()}')
                exit()
            if not os.path.exists(config.get('paths','PIDFILE')):
                with open(config.get('paths','PIDFILE'),'w') as pidfile:
                    pidfile.write(pid)
            time.sleep(5)

    except Exception as e:
        logger.fatal(f'PROCESS {pid} FAILED')
        logger.error(f'{e.__class__} encountered: {e}')
    finally: exit()

def stop(*,ignore_err=False):
    try:
        with open(config.get('paths','PIDFILE'),'r') as pidfile: 
            pid = int(pidfile.read())
        try: os.kill(pid,0)
        except:
            logger.info(f'no process {pid} exists, removing PIDFILE')
            os.remove(config.get('paths','PIDFILE'))
            return
        
        if config.get('system','PLATFORM') != 'Windows':
            logger.info(f'sending SIGTERM to process {pid}...')
            os.kill(pid,signal.SIGTERM)

            for i in range(1,6):
                logger.info(f'success check: {i}')
                time.sleep(5)
                if not os.path.exists(config.get('paths','PIDFILE')): return
            logger.info(f'sending SIGKILL to process {pid}...')
            os.kill(pid,signal.SIGKILL)
        with open(os.path.join(config.get('paths','TMP_DIR'),'.exit'),'w') as e:
            e.write(os.getpid())

        for i in range(1,6):
            logger.info(f'success check: {i}')
            time.sleep(5)
            if not os.path.exists(config.get('paths','PIDFILE')): return

        os.remove(config.get('paths','PIDFILE'))
        raise RuntimeError('PIDFILE cleared manually')
    except FileNotFoundError:
        logger.info(f'no instance is running')
    except Exception as e:
        logger.error(f'failed to stop running daemon: {pid}')
        logger.error(e)
        if not ignore_err: raise

if __name__ == '__main__':
    ap = argparse.ArgumentParser()

    verbosity = ap.add_mutually_exclusive_group()
    verbosity.add_argument('-s','--silent',
                           dest='logging_level',
                           action='store_const',
                           const=logging.FATAL)
    verbosity.add_argument('-q','--quiet',
                           dest='logging_level',
                           action='store_const',
                           const=logging.ERROR)
    verbosity.add_argument('--default',
                           dest='logging_level',
                           action='store_const',
                           const=logging.WARNING)
    verbosity.add_argument('-v','--verbose',
                           dest='logging_level',
                           action='store_const',
                           const=logging.INFO)
    
    add_noarg_opt(ap,'--force','force')
    add_noarg_opt(ap,'--attached','attached')

    action = ap.add_mutually_exclusive_group(required=True)
    add_noarg_opt(action,'--run','command',short=False,
                  help='runs new daemon instance if none exists')
    add_noarg_opt(action,'--stop','command',short=False,
                  help='stops currently running daemon process')
    add_noarg_opt(action,'--config','command',short=False,
                  help='prints default/current configuration and exits')
    add_noarg_opt(action,'--version','command',short=False,
                  help='prints current version and exits')

    ap.set_defaults(command='run',logging_level=logging.WARNING)
    pa = ap.parse_args(sys.argv[1:])
    config.set('runtime','EXEC',sys.argv[0])
    config.set('runtime','ARGV',' '.join(sys.argv[1:]))
    config.set('runtime','ATTACHED',str(pa.attached!=None))
    if not os.getenv('DEBUG') is None: pa.logging_level = logging.DEBUG
    if pa.command == 'run':
        root_logger.setLevel(pa.logging_level)
        run()
    elif pa.command == 'stop':  
        root_logger.setLevel(pa.logging_level)
        global logger
        logger = logging.getLogger(f'{root_logger.name}.kill',sys.stdout)
        stop(ignore_err=pa.force!=None)
    elif pa.command == 'config':
        try:
            with open(config.get('paths','PIDFILE'),'r') as pidfile, \
                 open(os.path.join(config.get('paths','TMP_DIR'),
                                   'runtime.conf'),'r') as conffile:
                sys.stdout.write(f'PROCESS {pidfile.read()} RUNNING WITH CONFIG:\n')
                sys.stdout.write(conffile.read())
        except FileNotFoundError:
            sys.stdout.write(f'NO INSTANCE ACTIVE, PRINTING DEFAULT CONFIG:\n')
            config.write(sys.stdout)
        except Exception as e:
            sys.stderr.write('{e.__class__} while reading config: {e}\n')
    elif pa.command == 'version':
        print(f'distortion-{root_logger.name} version: {config.get("global","VERSION",fallback="UNKNOWN")}')
