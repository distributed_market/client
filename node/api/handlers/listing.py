import re

true_filter = lambda arg: True
false_filter = lambda arg: False

regex_filter = lambda _: (
        lambda pattern: lambda arg: pattern.match(str(arg))
                            )(re.compile(_))

in_filter  = lambda _: (
        lambda pattern: lambda arg: str(arg) in pattern
                            )(_.split(','))

eq_filter  = lambda pattern: lambda arg: arg==pattern
neq_filter = lambda pattern: lambda arg: arg!=pattern

lt_filter = lambda pattern: lambda arg: arg <arg.__class__(pattern)
le_filter = lambda pattern: lambda arg: arg<=arg.__class__(pattern)
ge_filter = lambda pattern: lambda arg: arg>=arg.__class__(pattern)
gt_filter = lambda pattern: lambda arg: arg> arg.__class__(pattern)

__mapping__ = {
        '==' : eq_filter,
        'eq' : eq_filter,
        '!=' : neq_filter,
        'ne' : neq_filter,
        '< ' : lt_filter,
        ' <' : lt_filter,
        'lt' : lt_filter,
        '<=' : le_filter,
        'le' : le_filter,
        '>=' : ge_filter,
        'ge' : ge_filter,
        '> ' : gt_filter,
        ' >' : gt_filter,
        'gt' : gt_filter,
        'in' : in_filter,
        '=~' : regex_filter
}

def Filter(mode,pattern):
    print(f'FILTER DISPATCH##{mode} -> {__mapping__}')
    try: return __mapping__[mode](pattern)
    except KeyError: return true_filter
