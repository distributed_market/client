from ...util.listing import *
from ...config import config
from ...data.auction import *
from ...data.peer import *
from ...data.message import *
from ...util.data import Field
from ...util.parser import DataType
from ...errors import ConnectionClosedError

from ..messages.core import MSG
from ..messages.control import *

from .core import Handler,NotImplementedHandler,ListHandler,HandlerError

class InitHandler(Handler,mscode=INIT):
    def __call__(self,data):
        ac = getattr(self,'__conn__',None)
        if ac is None: return
        ac.set_header(**{k:v for k,v in data.to_dict().items()
                             if k in ['MAX_LENGTH',
                                      'MAX_BLOCKS',
                                      'LONG_CODES']})
        return DataType.OBJ,ac.Header.to_dict()

class HelpHandler(Handler,mscode=HELP):
    @staticmethod
    def __basicinfo__(msg):
        return { 'name' : msg.__name__,
                 'code' : msg.__code__,
                 'desc' : msg.__desc__,
                 'flds' : [k for k,v in msg.__dict__.items()
                                if isinstance(v,Field)] }

    @staticmethod
    def __richinfo__(msg):
        return { 'name' : msg.__name__,
                 'code' : msg.__code__,
                 'desc' : msg.__desc__,
                 'docs' : msg.__doc__,
                 'flds' : [
                           { 'name' : k,
                             'type' : [t.__name__ for t in v.__origin__
                                        if not t is None.__class__],
                             'const' : v.__const__,
                             'optional' : v.__optional__ }
                        for k,v in msg.__dict__.items()
                                if isinstance(v,Field) ] #} }
                 }
    
    def __call__(self,data):
        try: 
            msgs = (MSG.__find__(data.command),)
            info = HelpHandler.__richinfo__
        except: 
            msgs = MSG.__subclasses_by_name__.values()
            info = HelpHandler.__basicinfo__
        self.logger.info(f'help for items: {msgs}')
        return DataType.OBJ,[ info(msg) for msg in msgs ]

class ListCommandsHandler(ListHandler,mscode=LSTCMD):
    SELECT_ALL = ['name','code','desc']
    SELECT_DEFAULT = ['name','code']
    DATA_CLASS = MSG

    def __call__(self,data):
        data = self.__class__.parse(data)
        self.logger.info(f'SELECT {data.select} FROM APIMessage WHERE {data.filter} ORDER BY {data.sorter}')
        res = [ { s:getattr(msg,f'__{s}__') for s in data.select 
                if hasattr(msg,f'__{s}__') } for msg in 
                MSG.__subclasses_by_name__.values() if all(
                    ff(getattr(msg,f'__{f}__')) for f,ff in data.filter.items() 
                    if hasattr(msg,f'__{f}__')) ]
        for k,v in data.sorter.items():
            if v != 0: res.sort(key=lambda x: getattr(x,f'__{k}__'),reverse=v<0)
        return res

class StateHandler(NotImplementedHandler,mscode=STATE): pass
#    def __call__(self,data):
#        raise NotImplementedError(STATE.__name__)

class SyncHandler(NotImplementedHandler,mscode=SYNCDB): pass
class UISyncHandler(Handler,mscode=SYNCDB,name='ui'):
    def __call__(self,data):
        res = { 'peers' : PeerDB.get_susp(),
                'messages' : MessageDB.get_susp() }
        return DataType.OBJ,res

class QuitHandler(Handler,mscode=QUIT):
    def __call__(self,data):
        import threading,time,signal,shutil
        def stop():
            expath = config.get('components',
                               config.get('runtime','MODULE')).split(',')[1]
            cmd = f'{expath} --stop '
            if not data.restart is None and data.restart==True:
                cmd += f'&& {expath} --run {config.get("runtime","ARGV",raw=True)}'
            if not data.time is None: time.sleep(data.time)
            os.system(cmd)

        self.logger.critical('starting killer thread')
        stopper = threading.Thread(group=None,target=stop,name='STOPPER THREAD')
        stopper.start()
        try: stopper.join(30)
        except: 
            self.logger.fatal('failed to exit gracefuly')
            if config.get('system','PLATFORM') == 'Windows':
                self.logger.critical(f'calling TASKKILL on {os.getpid()}')
                if not os.popen(f'"{shutil.which("taskkill")}" /f {os.getpid()}'
                        ).close() in (0,None):
                    self.logger.fatal('failed to kill process')
                    raise HandlerError('FATAL: failed to kill')
            else:
                self.logger.critical(f'sending SIGKILL to {os.getpid()}')
                os.kill(os.getpid(),signal.SIGKILL)
            raise HandlerError('killed forcefully')
        return DataType.BIN,b'{}'

class ErrHandler(Handler,mscode=ERR):
    def __call__(self,data):
        self.logger.error(f'{data.cause}({data.message})')
        raise HandlerError(f'{data.cause}#{data.message}')

class DropHandler(Handler,mscode=DROP):
    def __call__(self,data):
        if getattr(self,'__conn__',None) is None: return
        self.logger.info('connection close request')
        raise ConnectionClosedError('connection closed on request')
