from ...messages.peers import *

from ..core import StateDependentHandler

class ListPeersHandler(StateDependentHandler,mscode=LSTUSR): pass
class StorePeerHandler(StateDependentHandler,mscode=ADDUSR): pass
class DeletePeerHandler(StateDependentHandler,mscode=DELUSR): pass
class RevokeCertHandler(StateDependentHandler,mscode=REVCRT): pass
