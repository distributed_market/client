from ...messages.peers import *

from ..core import NotLoggedInHandler

class NotLoggedInListPeersHandler(NotLoggedInHandler,name='ON',mscode=LSTUSR): pass
class NotLoggedInStorePeerHandler(NotLoggedInHandler,name='ON',mscode=ADDUSR): pass
class NotLoggedInDeletePeerHandler(NotLoggedInHandler,name='ON',mscode=DELUSR): pass
class NotLoggedInRevokeCertHandler(NotLoggedInHandler,name='ON',mscode=REVCRT): pass
