from ....account.security import SHA256
from ....config import config
from ....data.peer import *

from ...messages.peers import *

from ..core import ListHandler,StateDependentHandler,Handler

import os

class OfflineListPeersHandler(ListHandler,name='OFFLINE',mscode=LSTUSR):
    SELECT_DEFAULT = ['name']
    DATA_CLASS = Peer

    @classmethod
    def items(cls): return PeerDB.get()

class OfflineStorePeerHandler(Handler,name='OFFLINE',mscode=ADDUSR):
    def __call__(self,data):
        data.cert = SHA256.new(data.cert).hexdigest()
        p = Peer(**data.to_dict())
        p.__validate__()
        PeerDB.create(p)
        return DataType.OBJ,{}

class OfflineDeletePeerHandler(Handler,name='OFFLINE',mscode=DELUSR):
    def __call__(self,data):
        if data.id is None:
            return DataType.OBJ,[ { 'id':p.id,
                                    'name':p.name }
                                  for p in PeerDB.get() ]
        PeerDB.delete(data.id)
        return DataType.OBJ,{}

class OfflineRevokeCertHandler(Handler,name='OFFLINE',mscode=REVCRT):
    def __call__(self,data):
        if data.id is None:
            return DataType.OBJ,[ { 'id':p.id,'name':p.name } 
                                  for p in PeerDB.get() ]
        p = PeerDB[data.id]
        if p is None: raise ValueError(f'no user entry at @{id}')
        with open(os.path.join(config.get('runtime','HOME'),'data',
                               'peers','blacklist'),'a') as blacklist:
            blacklist.write(p.cert)
        PeerDB.delete(p.id)
        return DataType.OBJ,{}
