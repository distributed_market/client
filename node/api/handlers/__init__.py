from .core     import Handler
from .control  import *
from .auctions import *
from .accounts import *
from .peers    import *
from .messages import *

def handle(mscode,cntype,data,suite=None):
    return Handler[mscode,suite].handle(cntype,data)
