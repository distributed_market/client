from ...messages.messages import *

from ..core import NotLoggedInHandler

class NotLoggedInClearConversationsHandler(NotLoggedInHandler,name='ON',mscode=CLRMSH): pass
class NotLoggedInListConversationsHandler(NotLoggedInHandler,name='ON',mscode=LSTMSH): pass
class NotLoggedInCreateMessageHandler(NotLoggedInHandler,name='ON',mscode=CRTMSG): pass
class NotLoggedInDeleteConversationHandler(NotLoggedInHandler,name='ON',mscode=DELMSH): pass
class NotLoggedInGetConversationHandler(NotLoggedInHandler,name='ON',mscode=GETMSH): pass
