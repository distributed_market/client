from ...messages.messages import *

from ..core import NotImplementedHandler

class OnlineClearConversationsHandler(NotImplementedHandler,name='ONLINE',mscode=CLRMSH): pass
class OnlineListConversationsHandler(NotImplementedHandler,name='ONLINE',mscode=LSTMSH): pass
class OnlineCreateMessageHandler(NotImplementedHandler,name='ONLINE',mscode=CRTMSG): pass
class OnlineDeleteConversationHandler(NotImplementedHandler,name='ONLINE',mscode=DELMSH): pass
class OnlineGetConversationHandler(NotImplementedHandler,name='ONLINE',mscode=GETMSH): pass
