from ...messages.messages import *

from ..core import StateDependentHandler

class ClearConversationsHandler(StateDependentHandler,mscode=CLRMSH): pass
class ListConversationsHandler(StateDependentHandler,mscode=LSTMSH): pass
class CreateMessageHandler(StateDependentHandler,mscode=CRTMSG): pass
class DeleteConversationHandler(StateDependentHandler,mscode=DELMSH): pass
class GetConversationHandler(StateDependentHandler,mscode=GETMSH): pass
