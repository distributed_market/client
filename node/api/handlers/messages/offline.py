from ...messages.messages import *

from ..core import NotImplementedHandler

class OfflineClearConversationsHandler(NotImplementedHandler,name='OFFLINE',mscode=CLRMSH): pass
class OfflineListConversationsHandler(NotImplementedHandler,name='OFFLINE',mscode=LSTMSH): pass
class OfflineCreateMessageHandler(NotImplementedHandler,name='OFFLINE',mscode=CRTMSG): pass
class OfflineDeleteConversationHandler(NotImplementedHandler,name='OFFLINE',mscode=DELMSH): pass
class OfflineGetConversationHandler(NotImplementedHandler,name='OFFLINE',mscode=GETMSH): pass
