from ....errors import ConnectionRejectError
from ....util.parser import DataType

from ...messages.connections import *

from ..core import Handler

class REQHandler(Handler,mscode=REQ):
    def __call__(self,data):
        pass

class ACCHandler(Handler,mscode=ACC):
    def __call__(self,data):
        pass

class SYNHandler(Handler,mscode=SYN):
    def __call__(self,data):
        return DataType.BIN,b''

class REJHandler(Handler,mscode=REJ):
    def __call__(self,data):
        self.logger.error(f'{data.cause} caused connection reject: {e}')
        if not self.__conn__ is None:
            self.__conn__.send('DROP',b'')
        raise ConnectionRejectError(data.message)
