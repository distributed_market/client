from ....util.threading import PauseableThread

from ...messages.connections import LSTCON
from ...ipc.server import SERVERS

from ..core import ListHandler

class NotLoggedInListConnHandler(ListHandler,mscode=LSTCON,name='ON'):
    SELECT_DEFAULT = ['addr','start']
    SELECT_ALL = ['addr','start','encryption']
    DATA_CLASS = 'UI.APIConnection'
    @classmethod
    def items(cls):
        ui = None
        for s in SERVERS['ui'].values():
            if s.state == PauseableThread.RUNNING: ui = s
        else: raise RuntimeError('no UI server active')
        with ui._pool_lock:
            return list(ui._pool.values())

class OfflineListConnHandler(ListHandler,mscode=LSTCON,name='OFFLINE'):
    SELECT_DEFAULT = ['addr','start']
    SELECT_ALL = ['addr','start','encryption']
    DATA_CLASS = 'Offline.APIConnection'
    @classmethod
    def items(cls):
        api = []
        for s in SERVERS['node'].values():
            if s.state == PauseableThread.RUNNING: api.append(s)
        if len(api) == 0: raise RuntimeError('no API server active')
        res = []
        for a in api:
            with a._pool_lock:
                res += a._pool.values()
        return res
        with ui._pool_lock:
            return list(ui._pool.values())

class OnlineListConnHandler(ListHandler,mscode=LSTCON,name='ONLINE'):
    SELECT_DEFAULT = ['addr','start']
    SELECT_ALL = ['addr','start','encryption']
    DATA_CLASS = 'Online.APIConnection'
    @classmethod
    def items(cls):
        relay = None
        for s in SERVERS['relay'].values():
            if s.state == PauseableThread.RUNNING: relay = s
        else: raise RuntimeError('no relay server active')
        api = SERVERS['node'][relay.proto]
        if api.state != PauseableThread.RUNNING:
            raise RuntimeError(f'no API server for protocol `{relay.proto}` active')
        with api._pool_lock:
            return list(api._pool.values())
