from .....data.peer import *

from ....messages.connections import *

from ...core import StateDependentHandler

from ..core import ConnRequestHandler

class OfflineConnRequestHandler(ConnRequestHandler,name='OFFLINE',mscode=REQ):
    def __call__(self,data):
        try: target = find_verified(data.target)
        except: target = data.target




class OfflineConnAcceptHandler(StateDependentHandler,name='OFFLINE',mscode=ACC): pass
class OfflineConnCloseHandler(StateDependentHandler,name='OFFLINE',mscode=CLSCON): pass
class OfflineConnRejectHandler(StateDependentHandler,name='OFFLINE',mscode=REJ): pass
