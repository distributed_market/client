from ....messages.connections import *

from ...core import NotLoggedInHandler

class NotLoggedInConnRequestHandler(NotLoggedInHandler,name='ON',mscode=REQ): pass
class NotLoggedInConnAcceptHandler(NotLoggedInHandler,name='ON',mscode=ACC): pass
class NotLoggedInConnCloseHandler(NotLoggedInHandler,name='ON',mscode=CLSCON): pass
class NotLoggedInConnRejectHandler(NotLoggedInHandler,name='ON',mscode=REJ): pass
