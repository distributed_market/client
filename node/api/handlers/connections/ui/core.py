from ....messages.connections import *

from ...core import StateDependentHandler

class ConnRequestHandler(StateDependentHandler,mscode=REQ): pass
class ConnAcceptHandler(StateDependentHandler,mscode=ACC): pass
class ConnCloseHandler(StateDependentHandler,mscode=CLSCON): pass
class ConnRejectHandler(StateDependentHandler,mscode=REJ): pass
