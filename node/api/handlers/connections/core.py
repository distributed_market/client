from ....util.parser import DataType

from ...messages.connections import *

from ..core import Handler,StateDependentHandler,ListHandler

class ConnAcknowledgeHandler(Handler,mscode=ACK):
    def __call__(self,data): return DataType.BIN,b''

class ListConnHandler(StateDependentHandler,mscode=LSTCON,name='ui'): pass

class ConnSynchroniseHandler(Handler,mscode=SYN):
    def __call__(self,data): return DataType.BIN,b''
