from ...errors import UsernameTakenError,LoginStatusError
from ...config import config
from ...util.parser import DataType

from ...account.manager.store import *
from ...account.manager.update import *
from ...account.session import *

from ..messages.accounts import *

from .core import Handler,ListHandler

class LogoutFromAccount(Handler,mscode=LOGOUT):
    def __call__(self,data):
        logout()
        return DataType.BIN,b'{}'

class ListAccountsHandler(ListHandler,mscode=LSTACC):
    DATA_CLASS = Account
    @classmethod
    def items(cls): return get_accounts().values()

    def __call__(self,data):
        data = self.__class__.parse(data)
        self.logger.info(
            f'SELECT {data.select} FROM {self.__class__.DATA_CLASS} WHERE {data.filter} ORDER BY {data.sorter}')
        self.logger.debug(f'data set: {self.__class__.items()}')
        res = [ { s : x[s] for s in data.select if s in x }
                for x in self.__class__.items() if all(
                    f(x[s]) for attr,f in data.filter.items()
                                                  if s in x) ]
        for k,v in data.sorter.items():
            self.logger.debug(f'sorting by k: v')
            if v != 0: res.sort(key=lambda x: getattr(x,k),reverse=v<0)
        return DataType.OBJ,res
        

class CreateAccountHandler(Handler,mscode=CRTACC):
    def __call__(self,data):
        try: create_account(**data.to_dict())
        except UsernameTakenError as e:
            self.logger.error(e)
            raise
        except Exception as e:
            self.logger.error(f'{e.__class__} on create account: {e}')
            delete_account(data.name,data.passwd)
            raise
        return DataType.OBJ,{}

class UpdateAccountHandler(Handler,mscode=UPDACC):
    def __call__(self,data):
        try: update_account(**data.to_dict())
        except Exception as e:
            self.logger.error(f'{e.__class__} on update account: {e}')
            raise
        return DataType.OBJ,{}

class DeleteAccountHandler(Handler,mscode=DELACC):
    def __call__(self,data):
        try: logout(data.name)
        except LoginStatusError: pass
        except: raise
        delete_account(**data.to_dict())
        return DataType.OBJ,{}

class InvalidateAccountHandler(Handler,mscode=INVACC):
    def __call__(self,data): 
        try: gen_cert(invalidate=True) 
        except Exception as e:
            self.logger.error(f'{e.__class__} on invalidate certificate: {e}')
            raise

class LoginHandler(Handler,mscode=LOGACC):
    def __call__(self,data):
        try: login(data.name,data.passwd)
        except Exception as e:
            self.logger.error(f'{e.__class__} on login: {e}')
            raise
        return DataType.OBJ,{}
    

