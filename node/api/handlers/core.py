from ...config import config

from ...util.listing import Filter
from ...util.data import Field
from ...util.parser import DataType
from ...errors import *


class HandlerMeta(type):
    @staticmethod
    def __wrapper__(fun):
        def __wrapper__(self,data):
            try:
                if isinstance(data,dict): data = self.__class__.__mscode__(**data)
                return fun(self,data)
            except (HandlerError,ConnectionError,BrokenPipeError) as e: raise
            except Exception as e: raise HandlerError(f'{str(self)}#{e}') from e
        return __wrapper__

    def __getitem__(cls,idx):
        i,j = idx,None
        if isinstance(idx,tuple):
            try:
                i = idx[0]
                j = idx[1]
            except: pass
        formsg = cls.__handlers__[i]
        try: return formsg[j]
        except: pass
        return formsg[None]

    def __new__(cls,clsname,bases,attr,*,
                mscode=None,name=None,
                _handlers={}):
        if '__call__' in attr: 
            attr['__call__'] = HandlerMeta.__wrapper__(attr['__call__'])
        inst = super().__new__(cls,clsname,bases,attr)
        inst.__handlers__ = _handlers

        inst.__fstr__ = f'{clsname}@{{}}'

        if not mscode is None:
            inst.__code__ = mscode
            inst.__mscode__ = mscode
            inst.__suite__ = name
            if not mscode in inst.__handlers__:
                inst.__handlers__[mscode] = {}
            inst.__handlers__[mscode][name] = inst
            inst.__fstr__ = f'{inst.__fstr__}[mscode={mscode},name={name}]'
        return inst

class Handler(object,metaclass=HandlerMeta):
    def __init__(self,conn=None):
        self.__conn__ = conn
        if conn is None:
            from ...util import logging
            self.logger = logging.getLogger(__name__,'handlers.log','w')
        else: self.logger = conn.logger

    def __str__(self):
        return self.__class__.__fstr__.format(self.__conn__)

class ListHandler(Handler):
    @classmethod
    def items(cls): return []

    @classmethod
    def parse(cls,data):
        if data.select is None:
            try: data.select = cls.SELECT_DEFAULT
            except: data.select = ['*']
        if '*' in data.select:
            try: data.select = cls.SELECT_ALL
            except:
                try: data.select = [ k for k,v in cls.DATA_CLASS.__dict__.items()
                                               if isinstance(v,Field) ]
                except: data.select = []
        if data.filter is None: data.filter = {}
        try: data.filter = { k:Filter(v[0:2],v[2:]) for k,v in data.filter.items() }
        except: data.filter = {}
        if data.sorter is None: data.sorter = {}
        return data

    def __call__(self,data):
        data = self.__class__.parse(data)
        self.logger.info(
            f'SELECT {data.select} FROM {self.__class__.DATA_CLASS} WHERE {data.filter} ORDER BY {data.sorter}')
        self.logger.debug(f'data set: {self.__class__.items()}')
        res = [ { s : getattr(x,s) for s in data.select if hasattr(x,s) }
                for x in self.__class__.items() if all(
                    f(getattr(x,attr)) for attr,f in data.filter.items()
                                                  if hasattr(x,attr)) ]
        for k,v in data.sorter.items():
            self.logger.debug(f'sorting by k: v')
            if v != 0: res.sort(key=lambda x: getattr(x,k),reverse=v<0)
        return DataType.OBJ,res

class StateDependentHandler(Handler):
    def __call__(self,data):
        return Handler[self.__class__.__code__,
                       config.get('runtime','STATE',fallback='ON')](
                               self.__conn__)(data)
class NotLoggedInHandler(StateDependentHandler):
    def __call__(self,data): raise RuntimeError('not logged in')

class NotImplementedHandler(Handler):
    def __call__(self,data): 
        raise NotImplementedHandler(f'{self.__class__.__name__} not implemented')
