from ...messages.auctions import *

from ..core import NotImplementedHandler

class OnlineGetCategoryTreeHandler(NotImplementedHandler,name='ONLINE',mscode=GETCAT): pass
class OnlineListAuctionsHandler(NotImplementedHandler,name='ONLINE',mscode=LSTAUC): pass
class OnlineCreateAuctionHandler(NotImplementedHandler,name='ONLINE',mscode=CRTAUC): pass
class OnlineUpdateAuctionHandler(NotImplementedHandler,name='ONLINE',mscode=UPDAUC): pass
class OnlineDeleteAuctionHandler(NotImplementedHandler,name='ONLINE',mscode=DELAUC): pass
class OnlineSuspendAuctionHandler(NotImplementedHandler,name='ONLINE',mscode=SSPAUC): pass
class OnlineResumeAuctionHandler(NotImplementedHandler,name='ONLINE',mscode=RESAUC): pass
