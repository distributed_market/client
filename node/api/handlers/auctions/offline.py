from ....util.listing import *
from ....util.parser import DataType
from ....data.auction import *

from ...messages.auctions import *

from ..core import Handler,ListHandler

class OfflineGetCategoryTreeHandler(Handler,mscode=GETCAT,name='OFFLINE'):
    def __call__(self,data): 
        return DataType.OBJ,config.get('runtime','CATEGORIES').to_dict()

class OfflineListAuctionsHandler(ListHandler,mscode=LSTAUC,name='OFFLINE'):
    SELECT_DEFAULT = ['title','descr','ppu']
    DATA_CLASS = Auction
    @classmethod
    def items(cls): return AuctionDB.get()

class OfflineCreateAuctionHandler(Handler,mscode=CRTAUC,name='OFFLINE'):
    def __call__(self,data):
        a = Auction(**data.to_dict())
        a.__validate__()
        AuctionDB.create(a)
        return DataType.OBJ,{}

class OfflineUpdateAuctionHandler(Handler,mscode=UPDAUC,name='OFFLINE'):
    def __call__(self,data):
        if data.id is None:
            return DataType.OBJ,[ { 'id':a.id,
                                    'title':a.title,
                                    'descr':a.descr }
                                   for a in AuctionDB.get() ]
        a = AuctionDB[data.id]
        AuctionDB.update(**data.to_dict())
        return DataType.OBJ,{}

class OfflineDeleteAuctionHandler(Handler,mscode=DELAUC,name='OFFLINE'):
    def __call__(self,data):
        if data.id is None:
            return DataType.OBJ,[ { 'id':a.id,
                                    'title':a.title,
                                    'descr':a.descr }
                                   for a in AuctionDB.get() ]
        AuctionDB.delete(data.id)
        return DataType.OBJ,{}

class OfflineSuspendAuctionHandler(Handler,mscode=SSPAUC,name='OFFLINE'):
    def __call__(self,data):
        if data.id is None:
            return DataType.OBJ,[ { 'id':a.id,
                                    'title':a.title,
                                    'descr':a.descr }
                                   for a in AuctionDB.get() ]
        AuctionDB.suspend(data.id)
        return DataType.OBJ,{}

class OfflineResumeAuctionHandler(Handler,mscode=RESAUC,name='OFFLINE'):
    def __call__(self,data):
        if data.id is None:
            return DataType.OBJ,[ { 'id':a.id,
                                    'title':a.title,
                                    'descr':a.descr }
                                    for a in AuctionDB.get() ]
        AuctionDB.resume(data.id)
        return DataType.OBJ,{}
