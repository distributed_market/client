from ...messages.auctions import *

from ..core import NotLoggedInHandler

class NotLoggedInGetCategoryTreeHandler(NotLoggedInHandler,name='ON',mscode=GETCAT): pass
class NotLoggedInListAuctionsHandler(NotLoggedInHandler,name='ON',mscode=LSTAUC): pass
class NotLoggedInCreateAuctionHandler(NotLoggedInHandler,name='ON',mscode=CRTAUC): pass
class NotLoggedInUpdateAuctionHandler(NotLoggedInHandler,name='ON',mscode=UPDAUC): pass
class NotLoggedInDeleteAuctionHandler(NotLoggedInHandler,name='ON',mscode=DELAUC): pass
class NotLoggedInSuspendAuctionHandler(NotLoggedInHandler,name='ON',mscode=SSPAUC): pass
class NotLoggedInResumeAuctionHandler(NotLoggedInHandler,name='ON',mscode=RESAUC): pass
