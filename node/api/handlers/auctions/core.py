from ...messages.auctions import *

from ..core import StateDependentHandler

class GetCategoryTreeHandler(StateDependentHandler,mscode=GETCAT): pass
class ListAuctionsHandler(StateDependentHandler,mscode=LSTAUC): pass
class CreateAuctionHandler(StateDependentHandler,mscode=CRTAUC): pass
class UpdateAuctionHandler(StateDependentHandler,mscode=UPDAUC): pass
class DeleteAuctionHandler(StateDependentHandler,mscode=DELAUC): pass
class SuspendAuctionHandler(StateDependentHandler,mscode=SSPAUC): pass
class ResumeAuctionHandler(StateDependentHandler,mscode=RESAUC): pass
