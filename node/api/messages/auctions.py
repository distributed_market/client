import os

from ...config import config

from ...util.data import *
from ...data.auction import Auction,AuctionDB

from .core import MSG

# AUCTIONS

class GETCAT(MSG,code=8,desc='get-category-tree'):
    general = TypedDictField(V=dict,optional=True)

class LSTAUC(MSG,code=9,desc='list-auctions'):
    filter = TypedDictField(optional=True)
                            #kall_constr=[lambda k:hasattr(Auction,k)])
    select = TypedListField(type=str,optional=True,
                            constr=[lambda val:val==['*'] or all(hasattr(Auction,k)
                                                   for k in val)])
    sorter = TypedDictField(V=int,optional=True,
                            kall_constr=[lambda k:hasattr(Auction,k)],
                            vall_constr=[lambda val:val in (-1,0,1)])

class CRTAUC(MSG,code=10,desc='create-auction'):
    id = SingleTypeField(type=int,optional=True)
    title = SingleTypeField(type=str,const=True,
                            constr=[int(150).__ge__])
    retention = SingleTypeField(type=int,optional=True)
    descr = SingleTypeField(type=str)
    category = SingleTypeField(type=str,optional=True)
    ppu   = Field(origin=(int,float),
                  constr=[float(0.0).__le__])
    photos= TypedListField(type=str,
                           constr=[os.path.exists])
    min_q = SingleTypeField(type=int,optional=True,
                            constr=[int(0).__le__])
    max_q = SingleTypeField(type=int,optional=True,
                            constr=[int(0).__le__])

class UPDAUC(MSG,code=11,desc='update-auction'):
    id = SingleTypeField(type=int,optional=True)
    descr = SingleTypeField(type=str,optional=True)
    ppu   = SingleTypeField(type=float,optional=True,
                            constr=[float(0.0).__le__])
    photos= TypedListField(type=str,optional=True,
                           constr=[os.path.exists])
    min_q = SingleTypeField(type=int,optional=True,
                            constr=[int(0).__le__])
    max_q = SingleTypeField(type=int,optional=True,
                            constr=[int(0).__le__])

class DELAUC(MSG,code=12,desc='delete-auction'):
    id = SingleTypeField(type=int,optional=True)

class SSPAUC(MSG,code=13,desc='suspend-auction'):
    id = SingleTypeField(type=int,optional=True)

class RESAUC(MSG,code=14,desc='resume-auction'):
    id = SingleTypeField(type=int,optional=True)
        
