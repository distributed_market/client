import datetime

from ...util.data import Field,SingleTypeField,TypedListField,TypedDictField
from ...data.message import Message

from .core import MSG

class CLRMSH(MSG,code=29,desc='clear-conversations'):
    filter = TypedDictField(V=str,optional=True,
                            kall_constr=[lambda k:hasattr(Message,k)])

class LSTMSH(MSG,code=30,desc='list-conversations'):
    filter = TypedDictField(V=str,optional=True,
                            kall_constr=[lambda k:hasattr(Message,k)])
    select = TypedListField(type=str,optional=True,
                            all_constr=[lambda k:hasattr(Message,k)])
    sorter = TypedDictField(V=int,optional=True,
                            kall_constr=[lambda k:hasattr(Message,k)],
                            vall_constr=[lambda v:v in (-1,0,1)])

class CRTMSG(MSG,code=31,desc='create-message'):
    target = Field(origin=(int,str,bytes),optional=True)
               #constr=[lambda val:isinstance(val,str) or Peer.__contains__(val)]
    content = SingleTypeField(type=str)
    expires = SingleTypeField(type=datetime.datetime,optional=True)

#class (MSG,code=32,desc=''):

class DELMSH(MSG,code=33,desc='delete-message-history'):
    target = Field(origin=(int,str,bytes),optional=True)

#class (MSG,code=34,desc=''):

class GETMSH(MSG,code=35,desc='get-message-history'):
    target = Field(origin=(int,str,bytes),optional=True)

