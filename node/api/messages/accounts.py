import os

from ...util.data import SingleTypeField,TypedListField,TypedDictField
from ...data.account import Account

from .core import MSG

class LOGOUT(MSG,code=15,desc='logout-from-account'):
    pass

class LSTACC(MSG,code=16,desc='list-accounts'):
    filter = TypedDictField(optional=True)
    select = TypedListField(type=str,optional=True,
                            all_constr=[lambda val:val=='*' or val=='name' or val=='home'])
    sorter = TypedDictField(V=int,optional=True,
                            vall_constr=[lambda v:v in (-1,0,1)])
class CRTACC(MSG,code=17,desc='create-account'):
    name = SingleTypeField(type=str,constr=[lambda val:len(val)<=50])
    home = SingleTypeField(type=str,optional=True,
                           constr=[lambda val: not os.path.exists(val) or
                                               os.listdir(val)==0])
    passwd = SingleTypeField(type=str,constr=[lambda val:len(val)>=12])
    #refs = TypedListField(type=str,
    #                      constr=[lambda val:len(val)>=2])#,
    #                      #vall_constr=[lambda val:len(v)>=4096])
    #conf = SingleTypeField(type=str,optional=True,
    #                       constr=[lambda val:os.path.isfile(val)])
    conf = TypedListField(type=str,optional=True,
                          all_constr=[lambda val: os.path.isfile(val)])
    TOR_PORT = SingleTypeField(type=int,optional=True)
    TOR_PASS = SingleTypeField(type=str,optional=True)
    THS_DIR = SingleTypeField(type=str,optional=True)
    THS_PORT = SingleTypeField(type=int,optional=True)
    CHANNEL = SingleTypeField(type=str,optional=True,
                           constr=[lambda val:
                                   val in Account.TorChannel.__members__])
    PORT = SingleTypeField(type=int,optional=True)
    SOCK = SingleTypeField(type=str,optional=True)
    MODE = SingleTypeField(type=str,optional=True,
                           constr=[lambda val:
                                   val in Account.SyncMode.__members__])
    RELAY = TypedListField(type=str,optional=True,
                           all_constr=[lambda val: val.endswith('.onion')])
    URL = TypedListField(type=str,optional=True)
    CERT = SingleTypeField(type=str,optional=True,
                           constr=[lambda val: os.path.isfile(val)])
    PGP_KEY_DIR = SingleTypeField(type=str,optional=True)
    HS_SRC = TypedListField(type=str,optional=True)#,
                            #all_constr=[LIBRARIES.__contains__])
    HS_KDF = TypedListField(type=str,optional=True)
    HS_KDF_ITER = SingleTypeField(type=str,optional=True,
                                  constr=[lambda val:val.startswith('@i')])
    HS_KDF_HMAC = TypedListField(type=str,optional=True)
    HS_KDF_HASH = TypedListField(type=str,optional=True)
    HS_KEX = TypedListField(type=str,optional=True)#,
                            #all_constr=[LIBRARIES.__get_proto__])
    HS_KEX_MLEN = SingleTypeField(type=str,optional=True,
                                  constr=[lambda val:val.startswith('@i')])
    HS_ENC = TypedListField(type=str,optional=True)#,
                            #all_constr=[LIBRARIES.__get_cipher__])
    HS_DGST = TypedListField(type=str,optional=True)#,
                            #all_constr=[LIBRARIES.__get_digest__])

class UPDACC(MSG,code=18,desc='update-account'):
    name = SingleTypeField(type=str,constr=[lambda val:len(val)<=50])
    passwd = SingleTypeField(type=str,constr=[lambda val:len(val)>=12])
    newpasswd = SingleTypeField(type=str,optional=True,
                                constr=[lambda val:len(val)>=12])
    newhome = SingleTypeField(type=str,optional=True,
                              constr=[lambda val: not os.path.exists(val) or
                                                  os.listdir(val)==0])
    conf = SingleTypeField(type=str,optional=True,
                           constr=[lambda val:os.path.isfile(val)])
    TOR_PORT = SingleTypeField(type=int,optional=True)
    TOR_PASS = SingleTypeField(type=str,optional=True)
    THS_DIR = SingleTypeField(type=str,optional=True)
    THS_PORT = SingleTypeField(type=int,optional=True)
    CHANNEL = SingleTypeField(type=str,optional=True,
                           constr=[lambda val:
                                   val in Account.TorChannel.__members__])
    PORT = SingleTypeField(type=int,optional=True)
    SOCK = SingleTypeField(type=str,optional=True)
    MODE = SingleTypeField(type=str,optional=True,
                           constr=[lambda val:
                                   val in Account.SyncMode.__members__])
    RELAY = TypedListField(type=str,optional=True,
                           all_constr=[lambda val: val.endswith('.onion')])
    URL = TypedListField(type=str,optional=True)
    CERT = SingleTypeField(type=str,optional=True,
                           constr=[lambda val: os.path.isfile(val)])
    PGP_KEY_DIR = SingleTypeField(type=str,optional=True)
    HS_SRC = TypedListField(type=str,optional=True)#,
                            #all_constr=[LIBRARIES.__contains__])
    HS_KEX = TypedListField(type=str,optional=True)#,
                            #all_constr=[LIBRARIES.__get_proto__])
    HS_KEX_MLEN = SingleTypeField(type=int,optional=True)
    HS_ENC = TypedListField(type=str,optional=True)#,
                            #all_constr=[LIBRARIES.__get_cipher__])
    HS_DGST = TypedListField(type=str,optional=True)#,
                            #all_constr=[LIBRARIES.__get_digest__])

class DELACC(MSG,code=19,desc='delete-account'):
    name = SingleTypeField(type=str,constr=[lambda val:len(val)<=50])
    passwd = SingleTypeField(type=str,constr=[lambda val:len(val)>=12])

class INVACC(MSG,code=20,desc='invalidate-account-certificate'):
    name = SingleTypeField(type=str,constr=[lambda val:len(val)<=50])
    passwd = SingleTypeField(type=str,constr=[lambda val:len(val)>=12])

class LOGACC(MSG,code=21,desc='login-with-account'):
    name = SingleTypeField(type=str,constr=[lambda val:len(val)<=50])
    passwd = SingleTypeField(type=str,constr=[lambda val:len(val)>=12])

