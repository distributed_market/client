from ...util.data import Field,SingleTypeField,TypedListField,TypedDictField
from ...data.peer import Peer

from .core import MSG

#class (MSG,code=22,desc='')

class LSTUSR(MSG,code=23,desc='list-peers'):
    filter = TypedDictField(V=str,optional=True,
                            kall_constr=[lambda val:hasattr(Peer,val)])
    select = TypedListField(type=str,optional=True,
                            all_constr=[lambda val:hasattr(Peer,val)])
    sorter = TypedDictField(optional=True,
                            kall_constr=[lambda val:hasattr(Peer,val)])

class ADDUSR(MSG,code=24,desc='add-peer'):
    id = SingleTypeField(type=int,optional=True)
    name = Field(origin=(int,str,bytes))
    cert = SingleTypeField(type=bytes)
                           #constr=[lambda val:len(val)>=4096])
    addr = SingleTypeField(type=str,optional=True)
                            #constr=[lambda val:val.endswith('.onion')])
    secret = SingleTypeField(type=bytes)

class DELUSR(MSG,code=26,desc='delete-peer'):
    id = SingleTypeField(type=int,optional=True,
                         constr=[Peer.__contains__])

class REVCRT(MSG,code=27,desc='revoke-peer-certificate'):
    id = SingleTypeField(type=int,optional=True,
                         constr=[Peer.__contains__])
