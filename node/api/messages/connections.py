from ...util.data import Field,SingleTypeField,TypedListField,TypedDictField

from .core import MSG

class ACK(MSG,code=36,desc='connection-acknowledge'):
    pass

class LSTCON(MSG,code=37,desc='list-connections'):
    filter = TypedDictField(V=str,optional=True)
    select = TypedListField(type=str,optional=True)
    sorter = TypedDictField(V=int,optional=True,
                            vall_constr=[lambda v:v in (-1,0,1)])

class REQ(MSG,code=38,desc='connection-request'):
    target = Field(origin=(int,str,bytes))
    HS_KEX = TypedListField(type=str,optional=True)
    HS_KEX_MLEN = SingleTypeField(type=int,constr=[int(2048).__lt__],optional=True)
    HS_ENC = TypedListField(type=str,optional=True)
    HS_KDF = TypedListField(type=str,optional=True)
    HS_KDF_ITER = SingleTypeField(type=int,constr=[int(0).__lt__],optional=True)
    HS_KDF_DGST = TypedListField(type=str,optional=True)
    HS_KDF_HMAC = TypedListField(type=str,optional=True)
    HS_DGST = TypedListField(type=str,optional=True)

class ACC(MSG,code=39,desc='connection-accept'):
    target = Field(origin=(int,str,bytes),optional=True)

class CLSCON(MSG,code=40,desc='close-connection'):
    target = Field(origin=(int,str,bytes))

class REJ(MSG,code=41,desc='connection-reject'):
    cause = SingleTypeField(type=str)
    message = SingleTypeField(type=str,optional=True)

class SYN(MSG,code=42,desc='connection-synchronise'):
    pass
