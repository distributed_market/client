from ...util.data import Field,FieldMeta

class MessageMeta(FieldMeta):
    def __str__(self):
        return f"{self.__name__}:{getattr(self,'__code__','')}:{getattr(self,'__desc__','')}"
    
    def __int__(self):
        try: return 1<<self.__code__
        except AttributeError: return 0

    def __and__(self,other): return int(self)&int(other)
    def __or__(self,other):  return int(self)|int(other)
    def __xor__(self,other): return int(self)^int(other)
    def __inv__(self): return ~int(self)
    def __rshift__(self,num): return int(self)<<int(num)
    def __lshift__(self,num): return int(self)>>int(num)
    
    def __lt__(self,other): return int(self)< int(other)
    def __le__(self,other): return int(self)<=int(other)
    def __ge__(self,other): return int(self)>=int(other)
    def __gt__(self,other): return int(self)> int(other)

    @classmethod
    def __prepare__(cls,clsname,bases,_root=False,code=None,desc=None):
        attr = super().__prepare__(clsname,bases)
        if not _root:
            if not code is None: attr['__code__'] = code
            if not desc is None: attr['__desc__'] = desc
        return attr
    def __new__(cls,clsname,bases,attr,_root=False,code=None,desc=None):
        inst = super().__new__(cls,clsname,bases,attr)
        if not _root: inst.__root__.__register__(inst)
        else: inst.__root__ = inst
        return inst
    def __init__(cls,clsname,bases,attr,_root=False,code=None,desc=None):
        super().__init__(clsname,bases,attr)

class __dict_mixin__(object):
    def to_dict(self,*,skip_empty=True):
        result = {}
        for c in self.__class__.mro():
            result.update({ fname:getattr(self,fname)
                                for fname,field in c.__dict__.items()
                                    if isinstance(field,Field) and
                                        ( not getattr(self,fname) is None or
                                          not field.__optional__ or
                                          not skip_empty ) })
        return result

class MSG(__dict_mixin__,metaclass=MessageMeta,_root=True):
    __subclasses_by_code__ = {}
    __subclasses_by_name__ = {}
    __subclasses_by_desc__ = {}

    __all__ = 0
    
    @staticmethod
    def __register__(cls):
        res = 0
        try:
            if cls.__code__ in MSG.__subclasses_by_code__:
                raise RuntimeError('subclass code @{} already taken'.format(
                                        cls.__code__))
            res |= 0b001
        except AttributeError: pass
        try: 
            if cls.__name__ in MSG.__subclasses_by_name__:
                raise RuntimeError('subclass name @{} already taken'.format(
                                        cls.__name__))
            res |= 0b010
        except AttributeError: pass
        try: 
            if cls.__desc__ in MSG.__subclasses_by_desc__:
                raise RuntimeError('subclass descriptive name @{} already taken'\
                                    .format(cls.__desc__))
            res |= 0b100
        except AttributeError: pass
        if res&0b001: 
            MSG.__subclasses_by_code__[cls.__code__] = cls
            MSG.__all__ |= int(cls)
        if res&0b010: MSG.__subclasses_by_name__[cls.__name__] = cls
        if res&0b100: MSG.__subclasses_by_desc__[cls.__desc__] = cls
        return res
    
    @staticmethod
    def __unregister__(cls):
        res = 0
        try: 
            del MSG.__subclasses_by_code__[cls.__code__]
            MSG.__all__ ^= int(cls)
            res |= 0b001
        except: pass
        try: 
            del MSG.__subclasses_by_name__[cls.__name__]
            res |= 0b010
        except: pass
        try: 
            del MSG.__subclasses_by_desc__[cls.__desc__]
            res |= 0b100
        except: pass
        return res

    @staticmethod
    def __find__(id):
        try: return MSG.__subclasses_by_code__[id]
        except KeyError: pass
        try: return MSG.__subclasses_by_name__[id]
        except KeyError: pass
        try: return MSG.__subclasses_by_desc__[id]
        except KeyError: pass
        try:
            if MSG in id.mro(): return id
        except: pass
        raise KeyError('no subclass with code, name or desc @{} found'.format(id))

    def __new__(cls,code=None,**kwargs):
        if cls is MSG: 
            inst = MSG.__find__(code)(**kwargs)
            inst.__validate__()
        else:
            inst = super().__new__(cls)
            for k,v in kwargs.items(): setattr(inst,k,v)
        return inst

