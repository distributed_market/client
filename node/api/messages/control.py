from ...util.data import SingleTypeField,TypedListField,TypedDictField
from ...data.auction import Auction,AuctionDB
from ...data.peer import Peer,PeerDB
from ...data.message import Message,MessageDB

from .core import MSG

# COMMANDS

class INIT(MSG,code=0):
    LONG_CODES = SingleTypeField(type=bool,optional=True)
    
    SIZE = SingleTypeField(type=int,optional=True)
    
    MAX_LENGTH = SingleTypeField(type=int,optional=True)
    MAX_BLOCKS = SingleTypeField(type=int,optional=True)
    
    LENGTH = SingleTypeField(type=int,optional=True)
    BLOCKS = SingleTypeField(type=int,optional=True)
    CNTYPE = SingleTypeField(type=int,optional=True)
    MSCODE = SingleTypeField(type=int,optional=True)

class HELP(MSG,code=1,desc='help'):
    command = SingleTypeField(type=str,optional=True,
                    constr=[lambda val:not MSG.__find__(val) is None])

class LSTCMD(MSG,code=2,desc='list-commands'):
    filter = TypedDictField(optional=True)
                            #kall_constr=[lambda val:hasattr(MSG,f'__{val}__')])
    select = TypedListField(type=str,optional=True)
    sorter = TypedDictField(V=int,optional=True,
                            kall_constr=[lambda val:hasattr(MSG,f'__{val}__')],
                            vall_constr=[lambda val:val in (-1,0,1)])

class STATE(MSG,code=3,desc='change-status'):
    state = SingleTypeField(type=str,
                            constr=[
                                lambda val:v=='ON' or \
                                           v=='ONLINE' or \
                                           v=='OFFLINE' or \
                                           v=='OFF'])

class SYNCDB(MSG,code=4,desc='synchronize-database'):
    __ops__ = ['create','update','delete']
    auctions = TypedDictField(V=dict,optional=True,
                              kall_constr=[__ops__.__contains__],
                              vall_constr=[lambda v:isinstance(v,list)])
    peers    = TypedDictField(V=dict,optional=True,
                              kall_constr=[__ops__.__contains__],
                              vall_constr=[lambda v:isinstance(v,list)])
    messages = TypedDictField(V=dict,optional=True,
                              kall_constr=[__ops__.__contains__],
                              vall_constr=[lambda v:isinstance(v,list)])
    


class QUIT(MSG,code=5,desc='shutdown'):
    time = SingleTypeField(type=int,optional=True)
    restart = SingleTypeField(type=bool,optional=True)


class ERR(MSG,code=6,desc='runtime-error'):
    cause = SingleTypeField(type=type,optional=True)
    message = SingleTypeField(type=str,optional=True)

class DROP(MSG,code=7,desc='drop-connection'):
    pass
