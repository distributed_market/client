from ....util.security import SecurityModule
from ....account.session.auth import get_key
from ....config import config
from ....errors import InvalidRouteError,ConnectionRejectError

from ...messages.connections import *

def get_ciphers(conn):
    _,content = conn.recv(accept=REQ)
    content = REQ(**content)
    here = config.get('runtime','ID',fallback=None)
    res = {}
    if content.target != here:
        raise InvalidRouteError(f'invalid address: {content.target}/{here}')
    for sect in 'HS_KEX','HS_KDF','HS_KDF_DGST','HS_KDF_HMAC','HS_ENC','HS_DGST':
        for remote in getattr(content,sect):
            local = SecurityModule.get(sect,remote)
            if not local is None:
                res[sect] = local.default[0]
                conn.args[sect] = local.default[1]
                break
        else: raise ConnectionRejectError(f'no common {sect} algorithm')
    res['HS_KEX_MLEN'] = max(content.HS_KEX_MLEN,SecurityModule.get('HS_KEX_MLEN'))
    res['HS_KDF_ITER'] = max(content.HS_KDF_ITER,SecurityModule.get('HS_KDF_ITER'))
    res['pubkey'] = get_key(res['HS_KEX'],res['HS_KEX_MLEN'])
    conn._kex = res['HS_KEX']
    conn.send(REQ(**res))
    

