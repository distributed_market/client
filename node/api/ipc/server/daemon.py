from ....config import config

from ...messages.core import MSG
from ...messages.peers import REVCRT
from ...messages.control import INIT,DROP,SYNCDB,ERR
from ...messages.auctions import GETCAT,LSTAUC
from ...messages.messages import CRTMSG,DELMSH
from ...messages.accounts import INVACC
from ...messages.connections import *

from ..connection import EncryptedDaemonConnection

from .core import APIServer

DEFAULT_MASK = int(INIT) | int(DROP) | int(SYNCDB) | int(ERR) | int(REVCRT) | \
               int(INVACC) | int(ACK) | int(REQ) | int(ACC) | int(REJ) | int(SYN)
NODE_MASK = int(CRTMSG) | int(DELMSH) | int(LSTAUC)

class TorRelayServer(APIServer):
    def __init__(self,*,handler_suite='relay',handlers=None):
        self.proto = 'onion'
        super().__init__(addr='',
                name=f'serv.relay-{config.get("runtime","USER",fallback="NOACC")}.onion',
                max_con=1,
                accept=DEFAULT_MASK | int(LSTAUC),handler_suite=handler_suite,
                handlers=handlers)

class HttpRelayServer(APIServer):
    def __init__(self,*,handler_suite='relay',handlers=None):
        self.proto = 'http'
        super().__init__(addr='',
                name=f'serv.relay-{config.get("runtime","USER",fallback="NOACC")}',
                max_con=1,
                accept=DEFAULT_MASK | int(LSTAUC),handler_suite=handler_suite,
                handlers=handlers)

class TorNodeServer(APIServer):
    def __init__(self,*,handler_suite='node',handlers=None):
        self.proto = 'onion'
        super().__init__(addr='',
                name=f'serv.node-{config.get("runtime","USER",fallback="NOACC")}.onion',
                accept=DEFAULT_MASK | NODE_MASK,max_con=8,
                handler_suite=handler_suite,handlers=handlers)

class HttpNodeServer(APIServer):
    def __init__(self,*,handler_suite='node',handlers=None):
        self.proto = 'http'
        super().__init__(addr='',
                name=f'serv.node-{config.get("runtime","USER",fallback="NOACC")}',
                accept=DEFAULT_MASK | NODE_MASK,max_con=32,
                handler_suite=handler_suite,handlers=handlers)
