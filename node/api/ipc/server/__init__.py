from ....config import config

from ..connection import PlainAPIConnection,EncryptedGUIConnection

from .ui import *
from .daemon import *

SERVERS =  {
                'ui' : {
                    'detached' : DetachedGUIServer(),
                    'attached' : AttachedGUIServer(),
                },'relay' : {
                    'onion'  : TorRelayServer(),
                    'http' : HttpRelayServer(),
                },'node' : {
                    'onion'  : TorNodeServer(),
                    'http' : HttpNodeServer(),
                }
            }

