from ...messages.core import MSG
from ...messages.control import DROP,QUIT

from ..connection import PlainAPIConnection

from .core import APIServer

class AttachedGUIServer(APIServer):
    def __init__(self,*,handler_suite='ui',handlers=None):
        if handlers is None: handlers = {}
        super().__init__(name='GUI-attached',max_con=1,
                         handler_suite=handler_suite,
                         handlers=handlers)
        self.handlers[DROP] = self.handler(QUIT)
        self.set_logger()

class DetachedGUIServer(APIServer):
    def __init__(self,*,handler_suite='ui',handlers=None):
        super().__init__(name='GUI-detached',handlers=handlers,
                         accept=MSG.__all__^int(QUIT),handler_suite=handler_suite)
        self.set_logger()
