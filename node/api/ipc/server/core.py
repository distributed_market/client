import typing
from threading import Lock

import socket
import io,os
io = io
os = os

from ....errors import *
from ....util.threading import PauseableThread,threading
from ....util.func import sanitize_str
from ....util.parser import DataType

from ...messages import MSG,INIT

from ..connection.plain import PlainAPIConnection

class APIServer(PauseableThread):
    __running__ = set()
    __lock__ = Lock()
    def __str__(self): return self.name

    def set_logger(self):
        from ....util import logging
        if os.getenv('DEBUG') is None: self.logger = logging.getPlainLogger('serv')
        else: self.logger = logging.getLogger(f'{__name__}.{self.name}',os.path.join('serv',f'{self.name}.log'))
        self.logger.info(f'accept mask: {hex(self.__accept__):>46} of\n{hex(MSG.__all__):>80}')
        self.logger.info(f'connection limit: {self._pool_max}')
        for msg in MSG.__subclasses_by_name__.values():
            try: self.logger.info(f'mapped: {msg.__name__:>8} -> {self.handlers[msg]}')
            except: self.logger.warning(f'failed to find candidate handler for {msg.__name__}[{self.__suite__}]')

    def __init__(self,addr:str='127.0.0.1',*,name=None,
                 max_con:int=5,accept=MSG.__all__,handler_suite:str=None,
                 handlers:typing.Dict[MSG,typing.Callable[
                     [PauseableThread],typing.Callable[[MSG],typing.Tuple[int,
                                                        typing.Any]]]]=None):
        super().__init__(name=name if not name is None else sanitize_str(f'serv-{mode}:{addr}'))

        if not accept & int(INIT): raise ConfigError(f'server must accept INIT message')
        self.__accept__ = accept

        self._pool = {}
        max_con = int(max_con)
        if max_con <= 0: raise ValueError(f'invalid MAX_CONNECTION count: {max_con}')
        self._pool_max = max_con
        self._pool_lock = threading.Lock()

        self.__suite__ = handler_suite
        self.handlers = {}
        from ...handlers.core import Handler
        for msg in MSG.__subclasses_by_name__.values():
            try: self.handlers[msg] = handlers[msg]
            except:
                try: self.handlers[msg] = Handler[msg,self.__suite__]
                except: pass
        self._conn_class = PlainAPIConnection
        self._conn_params = {}
        self.sock = None
        self.addr = addr

    def set_conn_params(self,conn_class,**conn_params): 
        self._conn_class,self._conn_params = conn_class,conn_params
    
    def set_socket(self,mode:str,fp:typing.Union[str,int]):
        try:
            if mode == 'SOCK':
                mode = socket.AF_UNIX
                try: os.remove(fp)
                except: pass
            elif mode == 'PORT':
                mode = socket.AF_INET
                fp = (self.addr,int(fp))
            else: raise ConfigError(f'invalid transmission channel: {mode}/(SOCK|PORT)')
            self.logger.info(f'connection attempt for {mode}:{fp}')
            self.sock = socket.socket(mode,socket.SOCK_STREAM)
            self.sock.settimeout(3.0)
            self.sock.bind(fp)
            self.logger.info(f'setting connection pool size to {self._pool_max}')
            self.sock.listen(self._pool_max)
            self.logger.info(f'instantiated socket connection:\n{str(self.sock):>80}')
        except Exception as e:
            self.logger.critical(f'{e.__class__} while instantiating socket connection: {e}')
            raise

    def handler(self,msgtype):
        try: return self.handlers[msgtype]
        except KeyError: None

    def _add_worker(self,worker):
        with self._pool_lock:
            self.logger.debug(f'adding connection {worker}...')
            if worker.addr in self._pool:
                raise ConnectionExistsError(f'connection for {addr} open')
            if len(self._pool) >= self._pool_max:
                raise RuntimeError('maximum connection pool size exceeded')
            self._pool[worker.addr] = worker
            self.logger.debug(f'starting connection {worker}...')
            try: 
                worker.start()
                self.logger.info(f'added connection {worker.addr}')
            except Exception as e:
                raise RuntimeError(f'{e.__class__}#{e}') from e

    def _del_worker(self,worker):
        with self._pool_lock:
            self.logger.debug(f'removing connection {worker}...')
            try: 
                del self._pool[worker.addr]
                self.logger.info(f'deleted connection: {worker.addr}')
            except: pass

    def _drop_workers(self):
        self.logger.info('dropping connections')
        cons = []
        with self._pool_lock:
            for con in self._pool.values():
                if con is None: continue
                self.logger.critical(f'stopping connection: {con.addr}')
                try: 
                    con.stop()
                    cons.append(con)
                except Exception as e:
                    self.logger.critical(f'{e.__class__} while stopping connection {con.addr}: {e}')
        for con in cons: 
            self.logger.info(f'joining connection: {con.addr}')
            con.join()
        self.logger.info(f'{len(cons)} connections dropped')
        return len(cons)

    def broadcast(self,mstype,content,cntype=DataType.OBJ):
        with self._pool_lock:
            for conn in self._pool.values():
                try: conn.send(mstype,content,cntype)
                except Exception as e: 
                    self.logger.warning(f'failed to broadcast to {conn.addr}')
                    self.logger.warning(f'{e.__class__}: {e}')

    def send(self,addr,mstype,content,cntype=DataType.OBJ):
        with self._pool_lock:
            try: self._pool[addr].send(mstype,content,cntype)
            except KeyError: pass
            except Exception as e: 
                self.logger.warning(f'failed to send to {addr}')
                self.logger.warning(f'{e.__class__}: {e}')
    
    def __enter__(self):
        if self.sock is None: raise RuntimeError('socket not instantiated')
        with APIServer.__lock__: APIServer.__running__.add(self)

    def __exit__(self,t,v,tb):
        self.logger.critical(f'exiting')
        self._drop_workers()
        self.logger.critical(f'closing socket: {self.sock.family}')
        self.sock.close()
        self.sock = None
        self.logger.info(f'exited')
        with APIServer.__lock__: APIServer.__running__.remove(self)
        
    def tick(self,*args):
        try:
            conn,addr = self.sock.accept()
            self.logger.info(f'got connection from {addr}')
            self._add_worker(self._conn_class(conn,addr,source=self,**self._conn_params))
        except socket.timeout: pass
        except ConnectionExistsError as e:
            self.logger.warning(f'connection for {addr} exists')
            self.send(addr,'ERR',{'cause':e.__class__,'message':str(e)})
        except RuntimeError as e:
            self.logger.error(f'{e.__class__} during tick: {e}')
            self.send(addr,'ERR',{'cause':e.__class__,'message':str(e)})
            conn.close()
