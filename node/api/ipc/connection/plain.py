from ....util.parser import DataType,encode,decode
from ...messages.control import INIT

from .header import Header
from .core import APIConnection

class PlainAPIConnection(APIConnection):
    def __enter__(self):
        self.logger.critical('awaiting INIT')
        try:
            _,content = self.recv(accept=INIT)
            resp = encode(*self.handlers[INIT](content))
            header = Header(blocks=1,length=len(resp),
                            cntype=DataType.OBJ,mscode='INIT')
            with self._conn_lock:
                self.logger.info(f'sending header: {bytes(header)}')
                self.conn.send(bytes(header))
                self.logger.debug(f'sending {resp}')
                self.conn.send(resp)
            self.encryption = None
        except Exception as e:
            self.logger.critical(f'{e.__class__} during INIT operation: {e}')
            raise
