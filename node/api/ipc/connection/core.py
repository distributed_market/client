import typing
import socket
import io,os
io = io
os = os


def __ceil__(num):
    res = int(num)
    return res if res >= num else res + 1

from ....errors import *

from ....util.threading import PauseableThread,threading
from ....util.func import sanitize_str
from ....util.parser import DataType,encode,decode

from ...messages import MSG

from .header import MAX_LENGTH,MAX_BLOCKS,HeaderFactory,Header

class APIConnection(PauseableThread):
    def __str__(self): return self.name
    def set_header(self,MAX_LENGTH:int=MAX_LENGTH,
                            MAX_BLOCKS:int=MAX_BLOCKS,
                            LONG_CODES:bool=False):
        with self._header_lock:
            try: self.Header = HeaderFactory(MAX_LENGTH,MAX_BLOCKS,LONG_CODES)
            except: self.Header = Header

    def __init__(self,conn,addr,*,source=None):
        from datetime import datetime
        self.started = datetime.now().strftime('%Y:%m:%d %H:%M:%S')

        self.addr = sanitize_str(str(addr)).replace('_',':')
        super().__init__(name=f'{self.__class__.__name__}:{self.addr.replace(":","_")}')

        from ....util import logging
        if os.getenv('DEBUG') is None: self.logger = logging.getPlainLogger('conn')
        else: self.logger = logging.getLogger(f'{__name__}.{self.name}',os.path.join('conn',f'{self.name}.log'))

        self.logger.info('preparing connection...')
        self.conn = conn
        self.conn.settimeout(3.0)
        self._conn_lock = threading.RLock()
        self.logger.info(self.conn)

        self.logger.info(f'owned by server: {source}')
        self.source = source
        self._header_lock = threading.Lock()
        self.Header = Header

        if source is None:
            from ...handlers.core import Handler
            self.__accept__ = MSG.__all__
            self.handlers = { msg : hnds[None](self) for msg,hnds in Handler.__handlers__.items() }
        else:
            self.__accept__ = source.__accept__
            self.handlers = { msg : hnd(self) for msg,hnd in source.handlers.items() }
        self.logger.info(f'ACCEPT mask: {self.__accept__ }/{MSG.__all__}')

        for msg,handler in self.handlers.items():
            self.logger.info(f'mapped: {msg.__name__:>8} -> {handler}')

    def __exit__(self,t,v,tb):
        self.logger.critical(f'exiting')
        self.conn.close()
        if not self.source is None: self.source._del_worker(self)
        return t in (ConnectionClosedError,BrokenPipeError)

    # subclasses should override these functions e.g. to include encryption
    def read(self,bytesnum): 
        result = self.conn.recv(bytesnum)
        self.logger.debug(f'received {result}')
        return result
    def write(self,content): 
        self.logger.debug(f'sending {content}')
        return self.conn.send(content)

    def __get__(self,*,accept:int=None):
        """Thread-unsafe byte transfer function for internal use only"""
        if accept is None: accept = self.__accept__
        elif accept|self.__accept__ > self.__accept__:
            raise IllegalCommandError(
                f'connection {self} does not accept message codes: {accept}')
        header = self.read(self.Header.SIZE)
        if not header: raise ConnectionClosedError()
        try: header = self.Header(header)
        except Exception as e: raise MessageParsingError(e) from e
        if not int(header.mstype) & int(accept):
            raise IllegalCommandError(
                    f'got message code {header.mstype}, expecting {accept}')
        return header,self.read(header.length)

    def recv(self,*,accept:int=None):
        """Basic interface for INBOUND packet transmission.
        
        Handles combining multiple-block messages into continuous byte stream.

        Arguments:
            accept    int   specifies ACCEPT mask for received message;
                            if it does not match $accept, it will be discarded
                            raising @IllegalCommandError

        Returns:
            mstype    MSG   class of API message received
            content  object decoded message content"""
        with self._conn_lock:
            header,data = self.__get__()
            packet = [None] * header.blocks
            packet[0] = data
            while header.blocks > 1:
                header.blocks -= 1
                self.logger.info(f'awaiting block @{header.blocks}')
                pheader,data = self.__get__(accept=header.mstype)
                packet[len(packet)-pheader.blocks] = data
            return header.mstype,decode(header.cntype,b''.join(packet))

    def __put__(self,mscode:typing.Union[int,str],
                 cntype:DataType,
                 msg:typing.Union[str,bytes],
                 block:int=1):
        """Thread-unsafe byte transfer function for internal use only"""
        try: header = self.Header(blocks=block,
                                  length=len(msg),
                                  cntype=cntype,
                                  mscode=mscode)
        except Exception as e: raise MessageParsingError(e) from e
        if not isinstance(msg,bytes): msg = str.encode(msg,'utf-8')
        self.write(bytes(header))
        self.write(msg)

    def send(self,mscode:typing.Union[int,str,MSG],data,cntype=DataType.OBJ):
        """Basic interface for OUTBOUND packet transmission.

        Handles data encoding, header creation and splitting data into 
        chunks of appropriate length.

        Arguments:
            mscode [int,str,MSG] type of message to send
            data       object    message content
           (cntype)   DataType   message content type for encoding"""
        with io.BytesIO() as packet:
            if isinstance(mscode,type): mscode = mscode.__name__
            packet.write(encode(cntype,data))
            blocks = __ceil__(packet.tell()/self.Header.MAX_LENGTH)
            packet.seek(0)
            with self._conn_lock:
                while blocks >= 1:
                    self.__put__(mscode,cntype,packet.read(
                                    self.Header.MAX_LENGTH),blocks)
                    blocks -= 1

    def exchange(self,mscode:typing.Union[int,str,MSG],data,cntype:DataType=DataType.OBJ):
        """Convenience interface for query-response handling.

        Wraps @send($mscode,$data,$cntype) and @recv() calls with single lock 
        acquisition, returns results of recv()."""
        with self._conn_lock:
            self.send(mscode,cntype,data)
            return self.recv()

    def tick(self,*args):
        try:
            mscd,data = self.recv()
            rcnt,resp = self.handlers[mscd](data)
            self.send(mscd,resp,rcnt)
        except socket.timeout: pass
        except (MessageParsingError,HandlerError) as e:
            self.logger.warning(f'{e.__class__} while parsing packet: {e}')
            self.send('ERR',{'cause':e.__class__.__name__,'message':str(e)})
        except (ConnectionClosedError,BrokenPipeError) as e:
            self.logger.critical('connection closed by peer')
            raise
        except Exception as e:
            self.logger.error(f'{e.__class__} during tick: {e}')
            self.send('ERR',{'cause':e.__class__.__name__,'message':str(e)})
            raise
