import typing
import math
math = math

__all__ = ['HeaderFactory','Header']

LONG_CODES = False
MAX_LENGTH = 4096
MAX_BLOCKS = 8

def HeaderFactory(MAX_LENGTH:int=MAX_LENGTH,
                  MAX_BLOCKS:int=MAX_BLOCKS,
                  LONG_CODES:bool=LONG_CODES):
    from ...messages import MSG
    from ....util.parser import DataType
    class Header(object):
        __slots__ = ['length','blocks','cntype','mscode','mstype']

        @classmethod
        def __parse__(cls,source:bytes):
            return { 'length' : int(source[cls.LENGTH[1]:cls.LENGTH[2]]),
                     'blocks' : int(source[cls.BLOCKS[1]:cls.BLOCKS[2]]),
                     'cntype' : int(source[cls.CNTYPE[1]:cls.CNTYPE[2]]),
                     'mscode' : source[cls.MSCODE[1]:cls.MSCODE[2]]\
                                                    .lstrip().rstrip()\
                                                    .decode('utf-8') }
        
        def __new__(cls,source:bytes=None,**kwargs):
            inst = super().__new__(cls) if source is None \
                        else cls.__new__(cls,**cls.__parse__(source))
            for attr in cls.__slots__[:-1]:
                try: setattr(inst,attr,kwargs[attr])
                except KeyError:
                    if not hasattr(inst,attr):
                        raise TypeError(f'missing header field: {attr}')
            if inst.length > cls.MAX_LENGTH:
                raise ValueError(f"message too big: {res['length']}")
            if inst.blocks > cls.MAX_BLOCKS:
                raise ValueError(f"transmission too long: {res['blocks']}")
            inst.mstype = MSG.__find__(inst.mscode)
            return inst
        
        @classmethod
        def to_dict(cls):
            res = { k:getattr(cls,k) for k in ['SIZE',
                                                'MAX_LENGTH',
                                                'MAX_BLOCKS',
                                                'LONG_CODES'] }
            res.update(
                  {  k:getattr(cls,k)[0] for k in ['LENGTH',
                                                   'BLOCKS',
                                                   'CNTYPE',
                                                   'MSCODE'] })
            return res

        def __bytes__(self):
            return bytes(f'\
{self.length:<{self.__class__.LENGTH[0]}}\
{self.blocks:<{self.__class__.BLOCKS[0]}}\
{self.cntype:<{self.__class__.CNTYPE[0]}}\
{self.mscode:<{self.__class__.MSCODE[0]}}','utf-8')

    Header.MAX_LENGTH = MAX_LENGTH
    Header.MAX_BLOCKS = MAX_BLOCKS
    Header.LONG_CODES = LONG_CODES
    
    length,blocks,cntype = math.ceil(math.log(MAX_LENGTH,10)), \
                           math.ceil(math.log(MAX_BLOCKS,10)), \
                           math.ceil(math.log(len(DataType),10))
    mscode = max(len(n) for n in (MSG.__subclasses_by_desc__
                                  if LONG_CODES else
                                  MSG.__subclasses_by_name__))
    Header.LENGTH = ( length,0,length )
    Header.BLOCKS = ( blocks,Header.LENGTH[2],Header.LENGTH[2]+blocks )
    Header.CNTYPE = ( cntype,Header.BLOCKS[2],Header.BLOCKS[2]+cntype )
    Header.MSCODE = ( mscode,Header.CNTYPE[2],Header.CNTYPE[2]+mscode )

    Header.SIZE = Header.LENGTH[0] \
                + Header.BLOCKS[0] \
                + Header.CNTYPE[0] \
                + Header.MSCODE[0]
    return Header

Header = HeaderFactory()

