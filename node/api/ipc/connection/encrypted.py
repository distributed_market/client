from ....config import config
from ....errors import *
from ....util.parser import DataType,encode,decode

from ...messages.connections import *
from ...messages.control import INIT

from .core import APIConnection

class EncryptedGUIConnection(APIConnection):
    def __enter__(self):
        pass

class EncryptedDaemonConnection(APIConnection):
    def __handshake__(self):
        _,content = self.recv(accept=REQ)
        content = REQ(**content)
        if config.get('runtime','ID',fallback=None) != content.target:
            raise InvalidRouteError(f'{content.target} does not match {config.get("runtime","ID",fallback=None)}')


    def __enter__(self):
        self.logger.critical('awaiting REQ')
        try:
            _,content = self.recv(accept=REQ)
            content = REQ(**content)
            if config.get('runtime','ID',fallback=None) != content.target:
                self.send('REJ',{'cause':'InvalidRouteError',
                          'message':f'packet target: {content.target} is not {config.get("runtime","ID",fallback=None)}'})
        except Exception as e:
            self.logger.critical(f'{e.__class__} on connection init: {e}')
            raise
