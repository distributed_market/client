class DistortionError(RuntimeError): pass

class ConfigError(DistortionError): pass

class DistortionAPIError(DistortionError): pass
class HandlerError(DistortionAPIError): pass
class IllegalCommandError(DistortionAPIError): pass
class UnknownCommandError(DistortionAPIError): pass
class MessageParsingError(DistortionAPIError): pass
class MissingArgumentError(MessageParsingError): pass
class UnknownArgumentError(MessageParsingError): pass
class IllegalArgumentError(MessageParsingError): pass

class ConnectionError(DistortionError): pass
class InvalidRouteError(ConnectionError): pass
class ConnectionExistsError(ConnectionError): pass
class ConnectionClosedError(ConnectionError): pass
class ConnectionRejectError(ConnectionError): pass

class UserManagerError(DistortionError): pass
class LoginStatusError(UserManagerError): pass
class HomeFileTreeError(UserManagerError): pass
class UsernameTakenError(UserManagerError): pass
class InvalidUsernameError(UserManagerError): pass
