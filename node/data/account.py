import enum

from ..config import config 
from ..util.data import SingleTypeField,TypedListField,TypedDictField

from .core import RootDataStructure

class Account(RootDataStructure):
    TorChannel = enum.Enum('TorChannel','PORT SOCK')
    SyncMode = enum.Enum('SyncMode','MANUAL STATIC REMOTE')

    name = SingleTypeField('username',str,const=True,
                           constr=[lambda val: len(val)<=50])
    home = SingleTypeField('homedir',str)
    #refs = TypedListField('references',bytes,const=True,
    #                constr=[lambda val:all(len(i)>=4096 for i in val)])
    #config = TypedDictField('configuration',V=(str,int,list),optional=True)


