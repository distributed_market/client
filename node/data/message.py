import datetime

from ..util.data import SingleTypeField

from .core import RootDataStructure
from .storage import DataRegistry

class Message(RootDataStructure,capture=('to',)):
    to   = SingleTypeField(type=bytes,const=True,
                           constr=[lambda val:len(val)>=4096])
    sender  = SingleTypeField(type=bytes,const=True,optional=True)
    content = SingleTypeField(type=str)
    expires = SingleTypeField(type=datetime.datetime,optional=True)

MessageDB = DataRegistry(Message)
