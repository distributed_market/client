import threading 
import typing

class __CRUD__(object):
    __slots__ = ['create','update','delete']
    def __init__(self):
        self.create = []
        self.update = []
        self.delete = []
    
    def to_dict(self):
        return { s : getattr(self,s) for s in __CRUD__.__slots__ }
    
    def on_delete(self,id):
        self.create = [ c for c in self.create if not c['id'] == id ]
        self.update = [ u for u in self.update if not u['id'] == id ]
        self.delete.append(id)

    def on_create(self,arg):
        for c in self.create:
            if c['id'] == arg['id']:
                raise KeyError(f'cannot override entry: @{c["id"]}')
        self.create.append(arg)

    def on_update(self,arg):
        for u in self.update:
            if u['id'] == arg['id']:
                u.update(arg)
                break
        else: self.update.append(arg)

class TypedDict(dict):
    def __init__(self,type):
        self.__type__ = type
    
    def add(self,**kwargs):
        elem = self.__type__(**kwargs)
        self[elem.id] = elem
        
class DataRegistry(dict):
    def __getitem__(self,key):
        with self.__lock__:
            try: return self.__active__[key]
            except: pass
            try: return self.__suspnd__[key]
            except: return None

    def __init__(self,type):
        self.__lock__ = threading.RLock()
        self.__type__ = type
        
        self.__active__ = TypedDict(type)
        self.__suspnd__ = TypedDict(type)
        self.__cached__ = TypedDict(type.__CACHE__)

        self.__sync__ = __CRUD__()

    def get(self):
        return self.get_act() + self.get_susp()
    def get_act(self):
        with self.__lock__: return list(self.__active__.values())
    def get_susp(self):
        with self.__lock__: return list(self.__suspnd__.values())

    def create(self,item):
        if not isinstance(item,self.__type__):
            raise TypeError(
                    f'invalid value type; {item.__class__}/{self.__type__}')
        with self.__lock__:
            self.__active__[item.id] = item
            self.__sync__.on_create(item.to_dict())
    
    def delete(self,id):
        with self.__lock__:
            #print(f'DELETING FROM {self} AT @{id}')
            try: 
                del self.__active__[id]
                self.__sync__.on_delete(id)
            except KeyError: pass# print(f'NO @{id} IN ACTIVE')
            try: del self.__suspnd__[id]
            except KeyError: pass#print(f'NO @{id} IN SUSPENDED')

    def update(self,id,**kwargs):
        with self.__lock__:
            a = self[id]
            if a is None: return
            old = {}
            try:
                for k,v in kwargs.items():
                    try: old[k] = getattr(a,k)
                    except: continue
                    setattr(a,k,v)
                self.__sync__.on_update({'id':id,**kwargs})
            except:
                for k,v in old.items():
                    try: setattr(a,k,v)
                    except: pass
                raise

    def cache(self,item):
        with self.__lock__:
            self.__cached__[item.id] = self.__type__.__CACHE__(item)

    def suspend(self,id):
        with self.__lock__:
            try:
                self.__suspnd__[id] = self.__active__[id]
                self.__suspnd__[id].status = 'suspended'
                del self.__active__[id]
                self.__sync__.delete.append(id)
            except KeyError: pass

    def resume(self,id):
        with self.__lock__:
            try:
                self.__active__[id] = self.__suspnd__[id]
                self.__active__[id].status = 'active'
                del self.__suspnd__[id]
                self.__sync__.create.append(self.__active__[id])
            except KeyError: pass

    def get_sync(self):
        with self.__lock__: return self.__CRUD__.to_dict()
    
    def clear(self):
        with self.__lock__:
            self.__active__.clear()
            self.__suspnd__.clear()
            self.__cached__.clear()
