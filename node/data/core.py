import enum
import typing

from .. import logger as root_logger
import logging
logger = logging.getLogger(f'{root_logger.name}.daemon.data')

from ..util.data import StoredInstanceMeta,SingleTypeField,FieldMeta,Field

class RetentionPolicy(enum.IntEnum):
    RUNTIME_ONLY = 1
    STORE_ENCRYPTED = 2
    STORE_PLAIN = 3

class RootMeta(StoredInstanceMeta,FieldMeta):
    @classmethod
    def __prepare__(cls,clsname,bases,*,capture:typing.List[str]=None):
        attr = super().__prepare__(clsname,bases,_instance_key=lambda self: self.id)
        if not capture is None:
            def _getattr(self,attr):
                if attr=='__target__': return self.__target__
                if attr in capture: return getattr(self.__target__,attr)
                raise AttributeError(f'no attribute named {attr}')
            attr['__CACHE__'] = type.__new__(type,
                                             f'{clsname}.__CACHE__',
                                             (object,),
                                             {'__slots__':['__target__'],
                                              '__init__':lambda self,target:
                                                setattr(self,'__target__',target),
                                              '__getattr__':_getattr,
                                              'to_dict':lambda self:{
                                                  x:self.x for x in capture}})
        return attr

class __dict_mixin__(object):
    def to_dict(self,*,skip_empty=True):
        result = {}
        for c in self.__class__.mro():
            result.update({ fname:getattr(self,fname)
                                for fname,field in c.__dict__.items()
                                    if isinstance(field,Field) and
                                        ( not getattr(self,fname) is None or
                                          not field.__optional__ or
                                          not skip_empty ) })
        return result

class RootDataStructure(__dict_mixin__,metaclass=RootMeta):
    """Provides common functionality for data structures used in program.
    
    By convention $id is assignable once-only.

    Fields:
    __slots__       define field structure

    Methods:
    __new__(cls[id,**kwargs])   handles instance creation
    validate(self)              verifies all mandatory fields have been assigned
    """
    id = SingleTypeField(type=int,const=True)
    retention = SingleTypeField(type=int,
                                constr=[
                                    lambda v:
                                        v in RetentionPolicy.__members__.values()])
    status = SingleTypeField(type=str,
                             constr=[lambda v:v in ('active','suspended')])
#    state = SingleTypeField(type=StructState)

    @classmethod
    def __get_next_id__(cls):
        return max(cls.__instances__)+1 if cls.__instances__ else 0

    #def __new__(cls,id:int=None,**kwargs:typing.Dict[str,typing.Any]):
    def __new__(cls,**kwargs:typing.Dict[str,typing.Any]):
        #if id is None: id = cls.__get_next_id__()
        instance = super().__new__(cls)
        #print(f'CREATE OBJECT##{cls}:{kwargs}')
        logger.debug('creating {cls} object for {kwargs}')
        #instance.id = id
        for entry in kwargs.items():
            try: setattr(instance,entry[0],entry[1])
            except AttributeError: pass
        if instance.id is None:
            instance.id = cls.__get_next_id__()
        if instance.retention is None:
            instance.retention = RetentionPolicy.RUNTIME_ONLY
        instance.status = 'active'
        return instance
    def __init__(self,*args,**kwargs): pass
