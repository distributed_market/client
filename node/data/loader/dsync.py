import os,io
os = os
io = io

from ...errors import LoginStatusError
from ...config import config
from ...util.logging import getPlainLogger

from ..core import RetentionPolicy
from ..auction import AuctionDB
from ..peer import PeerDB

class DiskLoader(object):
    def __init__(self,db,target,parser,mapping):
        self.__db__  = db
        self.target  = target
        self.parser  = parser
        self.mapping = mapping
        self.logger  = getPlainLogger(__name__)

    def __enter__(self):
        self.__curdir__ = os.path.abspath(os.curdir)
        os.makedirs(self.target,exist_ok=True)
        os.chdir(self.target)

    def __exit__(self,type,value,tb):
        if not self.__curdir__ is None:
            os.chdir(self.__curdir__)
            self.__curdir__ = None

    def load(self,decrypt,passwd):
        self.__db__.clear()
        self.logger.info(f'loading {self.target} from disk...')
        for db,fd in self.mapping:
            self.logger.info(f'mapping {fd} -> {db}')
            try:
                with open(fd,'r') as nenc:
                    for entry in self.parser.decode(nenc.read()):
                        self.logger.debug(f'read plain entry: {entry}')
                        elem = db.__type__(**entry)
                        db[elem.id] = elem
                self.logger.info('plain data loaded')
            except Exception as e:
                self.logger.warning(f'{e.__class__} on plain read: {e}')
            try:
                with open(f'{fd}.enc','rb') as enc,io.BytesIO() as dec:
                    decrypt(enc,dec,passwd)
                    for entry in self.parser.decode(bytes.decode(dec.getvalue(),'utf-8')):
                        self.logger.debug(f'read encrypted entry: {entry}')
                        elem = db.__type__(**entry)
                        db[elem.id] = elem
                self.logger.info('encrypted data loaded')
            except Exception as e:
                self.logger.warning(f'{e.__class__} on encrypted read: {e}')
    def flush(self,encrypt,passwd):
        self.logger.info(f'flushing {self.target} to disk...')
        for db,fd in self.mapping:
            self.logger.info(f'mapping {db} -> {fd}')
            try:
                with open(fd,'w') as nenc:
                    nenc.write(self.parser.encode([x.to_dict() for x in db.values() 
                                if x.retention==RetentionPolicy.STORE_PLAIN]))
                    nenc.write('\n')
                    self.logger.info('plain data flushed')
            except Exception as e:
                self.logger.warning(f'{e.__class__} on plain write: {e}')
            try:
                with io.BytesIO(str.encode(self.parser.encode([x.to_dict() for x
                        in db.values() if x.retention==RetentionPolicy.STORE_ENCRYPTED]),
                        'utf-8')) as dec,open(f'{fd}.enc','wb') as enc:
                    encrypt(dec,enc,passwd)
                    self.logger.info('encrypted data flushed')
            except Exception as e:
                self.logger.warning(f'{e.__class__} on encrypted write: {e}')
        self.__db__.clear()

def AuctionLoader(root,parser):
    return DiskLoader(AuctionDB,os.path.join(root,'data','auctions'),parser,mapping=[
                                (AuctionDB.__active__,'active.json'),
                                (AuctionDB.__suspnd__,'suspended.json'),
                                (AuctionDB.__cached__,'observed.json') ])

def PeerLoader(root):
    return DiskLoader(PeerDB,os.path.join(root,'data','peers'),parser,mapping=[
                                (PeerDB.__active__,'trusted.json') ])

