from ..util.data import Field,TypedListField,SingleTypeField

from .core import RootDataStructure,RetentionPolicy
from .storage import DataRegistry

from enum import IntEnum
class PeerStatus(IntEnum):
    VERIFIED = 0
    PENDING = 1

class Peer(RootDataStructure,capture=('name','cert')):
    name    = Field(origin=(int,str,bytes),const=True)
    cert    = SingleTypeField(type=bytes,const=True)
    token   = SingleTypeField(type=bytes,optional=True)
    secrets = TypedListField(type=bytes,optional=True)
    addr    = SingleTypeField(type=str,optional=True)
    def __str__(self):
        return f'{name}#{cert}#{addr}'

PeerDB = DataRegistry(Peer)

def find_pending(key):
    for peer in PeerDB.get_susp():
        if key == peer.id: return peer
        if key == peer.name or key == peer.cert: return peer
    raise KeyError(f'no pending peer entry at @{key}')

def find_verified(key):
    for peer in PeerDB.get_act():
        if key == peer.id: return peer
        if key == peer.name or key == peer.cert: return peer
    raise KeyError(f'no verified peer entry at @{key}')

