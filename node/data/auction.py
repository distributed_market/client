import os

from ..config import config
from ..util.data import SingleTypeField,TypedListField,Field

from .core import RootDataStructure
from .storage import DataRegistry

class CategoryTree(object):
    def __init__(self,name,subcategories):
        self.name = name
        self.subcategories = []
        for sub in subcategories:
            self.subcategories.append(
                    CategoryTree(sub['name'],sub['subcategories']))

    def __contains__(self,path):
        if isinstance(path,str):
            return path == self.name
        for sub in self.subcategories:
            if path in sub:
                return True
        return False
    def to_dict(self):
        return { 'name':self.name,
                 'subcategories':
                    [ x.to_dict() for x in self.subcategories ] }

class Auction(RootDataStructure,capture=('vendor','title','ppu')):
    vendor = SingleTypeField(type=bytes,const=True,optional=True
                    #default_generator=lambda:config['runtime']['USER'],
                    )
    title  = SingleTypeField(type=str,constr=[int(150).__ge__])
    descr  = SingleTypeField(type=str)
    category = SingleTypeField(type=str,optional=True)
    #ppu    = SingleTypeField(type=float,constr=[float(0.0).__le__])
    ppu    = Field(origin=(int,float),constr=[float(0.0).__le__])
    photos = TypedListField(type=str,optional=True,
                            constr=[os.path.exists])
    min_q  = SingleTypeField(type=int,optional=True,
                    constr=[int(0).__le__])
    max_q  = SingleTypeField(type=int,optional=True,
                    constr=[int(0).__le__])

AuctionDB = DataRegistry(Auction)

Categories = CategoryTree('general',[{'name':'unspecified','subcategories':[]}])

config.set('runtime','CATEGORIES',Categories)
