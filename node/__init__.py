import logging
logger = logging.getLogger(__name__)

from .config import config

import os
for path in config.__globals__.options('paths'):
    config.__globals__.set('paths',
                           path,os.path.abspath(config.get('paths',path)))

