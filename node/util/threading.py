import threading
import typing
import logging

class PauseableThread(threading.Thread):
    RUNNING = 0b001
    PAUSED  = 0b011
    STOPPED = 0b100

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        # thread state synchronization
        self._state = PauseableThread.STOPPED
        self._state_lock = threading.Lock()
        # synchronized condition to wait on
        self._pause = threading.Condition(threading.Lock())
        self._pause_lock = threading.Lock()
        self.logger = logging.getLogger(self.getName())

    @property
    def state(self)->int:
        """Thread-safe state check."""
        with self._state_lock: return self._state
    @state.setter
    def state(self,state):
        """Thread-safe state assignment."""
        with self._state_lock: 
            self.logger.info(f'THREAD{self.name} changing state to: {state}')
            self._state = state

    def tick_args(self)->typing.Tuple[typing.Any]:
        """Generates/fetches arguments for next tick."""
        return ()
    def tick(self,*args:typing.Tuple[typing.Any]):
        """Called on every main service loop iteration.

        Arguments are inferred from tick_args."""
        pass

    def __enter__(self): pass
    def __exit__(self,type,value,traceback): pass

    def run(self):
        with self:
            while True: # main service loop
                with self._pause: # assure thread-safety
                    # if thread gets paused immediately after it's resumed,
                    # PAUSED will be set before _pause is acquired
                    while self.state == PauseableThread.PAUSED:
                        self.logger.debug(f'THREAD{self.name} waiting on @PAUSE state')
                        self._pause.wait()
                    if self.state == PauseableThread.STOPPED:
                        self.logger.critical(f'THREAD{self.name} exiting main loop')
                        break

                #self.logger.debug(f'THREAD{self.name} tick')
                self.tick(*self.tick_args())

    def start(self):
        self.logger.critical(f'THREAD{self.name} starting')
        with self._pause_lock:
            if self.state != PauseableThread.STOPPED:
                raise RuntimeError('thread already started')
        self.state = PauseableThread.RUNNING
        super().start()
        self.logger.info(f'THREAD{self.name} started')

    def pause(self):
        self.logger.critical(f'THREAD{self.name} pausing')
        with self._pause_lock:
            if self.state != PauseableThread.RUNNING:
                raise RuntimeError('cannot pause non-running thread')
            self.state = PauseableThread.PAUSED
            if not self._pause.acquire():
                raise RuntimeError(
                        'error ocurred while acquiring pause condition')
        self.logger.info(f'THREAD{self.name} paused')

    def resume(self):
        self.logger.critical(f'THREAD{self.name} resuming')
        with self._pause_lock:
            if self.state != PauseableThread.PAUSED:
                raise RuntimeError('cannot resume non-paused thread')
            self.state = PauseableThread.RUNNING
            self._pause.notify()
            self._pause.release()
        self.logger.info(f'THREAD{self.name} resumed')

    def stop(self):
        self.logger.critical(f'THREAD{self.name} stopping')
        with self._pause_lock:
            if self.state == PauseableThread.STOPPED:
                raise RuntimeError('thread already stopped')
            self.state = PauseableThread.STOPPED
            try:
                self._pause.notify()
                self._pause.release()
            except: pass
        self.logger.info(f'THREAD{self.name} stopped')

