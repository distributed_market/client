import enum
import typing

from ...config import config

from .core import Parser,StringParser,BinaryParser
from .json import JSONParser

if config.get('transmission','DATA') == 'json':
    ObjectParser = JSONParser
else: 
    raise RuntimeError('unsupported data language: {}'.format(
                            config.get('transmission','DATA')))

@enum.unique
class DataType(enum.IntFlag):
    __types__ = {}
    @classmethod
    def parser(cls,i): return cls.__types__[int(i)].parser
    STR = (1,StringParser)
    BIN = (2,BinaryParser)
    OBJ = (3,ObjectParser)
    RAW = (4,Parser)
    def __new__(cls,i,parser):
        inst = super().__new__(cls,i)
        inst.parser = parser
        cls.__types__[i] = inst
        return inst

def decode(type,data:bytes=None):
    try: return DataType.parser(type).decode(data)
    except: return None

def encode(type,data:typing.Any=None):
    try: return DataType.parser(type).encode(data)
    except: return b''
