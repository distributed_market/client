import typing

class Parser(object):
    @classmethod
    def encode(cls,data:typing.Any=None):
        return bytes(data) if not data is None else b''
   
    @classmethod
    def decode(cls,data:bytes=None): return data

class StringParser(object):
    @classmethod
    def encode(cls,data:str=None): 
        return bytes(data,'utf-8') if not data is None else b''

    @classmethod
    def decode(cls,data:bytes=None): 
        return data.decode('utf-8') if not data is None else ''

class BinaryParser(object):
    @classmethod
    def encode(cls,data:bytes=None): return data if not data is None else b''

    @classmethod
    def decode(cls,data:bytes=None): return data if not data is None else b''
