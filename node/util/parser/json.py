import json
json = json

from .core import Parser

def _default(o):
    #print(f'JSON ENCODER##token: {o}')
    if isinstance(o,type):
        return o.__name__
    if isinstance(o,(str,dict,list)):
        return json.JSONEncoder.default(o)
    if hasattr(o,'to_dict'):
        return json.JSONEncoder.default(o.to_dict())
    if hasattr(o,'__iter__'):
        return list(iter(o))
    return json.JSONEncoder.default(o)

class JSONParser(Parser):

    __encoder__ = json.JSONEncoder(separators=(',',':'),
                                   default=_default)

    __decoder__ = json.JSONDecoder()
    
    @classmethod
    def encode(cls,data:dict=None):
        #print(f'JSON ENCODER##encoding {data}')
        res = b'{}'
        if not data is None:
            res = bytes(JSONParser.__encoder__.encode(data),'utf-8')
        #print(f'JSON ENCODER##encoded {res}')
        #return bytes(JSONParser.__encoder__.encode(data),
        #             'utf-8') if not data is None else b''
        return res

    @classmethod
    def decode(cls,data:bytes=None): 
        #print(f'JSON DECODER##decoding {data}')
        res = {}
        if not data is None:
            res =JSONParser.__decoder__.raw_decode(
                    data.decode('utf-8'))[0]
        #print(f'JSON DECODER##decoded {res}')
        return res
