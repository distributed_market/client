import typing

from logging import getLogger as getPlainLogger
getPlainLogger = getPlainLogger
from logging import StreamHandler,FileHandler,Formatter, \
                    FATAL,CRITICAL,ERROR,WARNING,INFO,DEBUG

import os,sys
os = os

from ..config import config

STDOUT_HANDLER = StreamHandler(sys.stdout)

__date_fmt__ = '%Y-%m-%d %H:%M:%S'
__out_fmt__  = '[{asctime}]{name:.<49}[{levelname:^8}]\n                     {message:.<59}'
FORMATTER = Formatter(__out_fmt__,__date_fmt__,style='{')

def getLogger(name,path=None,mode='a'):
    logger = getPlainLogger(name)
    if path is None: path = f'{name.split(".")[-1]}.log'
    if hasattr(path,'write'): handler = StreamHandler(path)
    else:
        path = os.path.join(config.get('paths','LOGGING_DIR'),path)
        try: 
            parent = os.path.abspath(os.path.dirname(path))
            os.makedirs(parent,exist_ok=True)
            if os.path.exists(path):
                with open(path,'r') as infile, \
                     open(f'{path}.bak','w') as outfile:
                    outfile.write('# LAST RUNTIME LOG #\n')
                    outfile.write(infile.read())
            handler = FileHandler(path,mode)
        except: handler = STDOUT_HANDLER
    handler.setFormatter(FORMATTER)
    for h in logger.handlers: logger.removeHandler(h)
    logger.handlers = [handler]
    logger.propagate = False
    logger.critical('LOGGER INITIALIZED')
    return logger

def concat(target:str,source,lvl:int=0):
    if isinstance(source,dict):
        for k,v in source.items():
            if isinstance(v,(list,dict)):
                target = concat(target+f'{"":<{lvl}}[{k}]\n',v,lvl+4)
            else: target += f'{"":<{lvl}}{k} = {v}\n'
    elif isinstance(source,list):
        for i in range(len(source)):
            v = source[i]
            if isinstance(v,(list,dict)):
                target = concat(target+f'{"":<{lvl}}{i:>2}) ',v,lvl+8)
            else: target += f'{"":<{lvl}}{i:>2}) {v}\n'
    else: target += f'{"":<{lvl}}{source}\n'
    return target

