def add_noarg_opt(target,optname,dest=None,short=True,**kwargs):
    if optname.startswith('--'): 
        if dest is None: dest = optname[2:].replace('-','_').upper()
        const = optname[2:]
        optname = (optname[1:3],optname) if short else (optname,)
    elif optname.startswith('-'):
        if dest is None: dest = optname[1:].replace('-','_').upper()
        const = optname[1:]
        optname = (optname,)
    else: return False
    target.add_argument(*optname,dest=dest,
                        action='store_const',
                        const=const,**kwargs)
    return True               
