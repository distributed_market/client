import typing

from .simple import SingleTypeField

class DictField(SingleTypeField):
    def __kall_lambda__(func):
        def __wrapper__(val): return all(func(i) for i in val.keys())
        return __wrapper__
    def __kany_lambda__(func):
        def __wrapper__(val): return any(func(i) for i in val.keys())
        return __wrapper__
    def __vall_lambda__(func):
        def __wrapper__(val): return all(func(i) for i in val.values())
        return __wrapper__
    def __vany_lambda__(func):
        def __wrapper__(val): return any(func(i) for i in val.values())
        return __wrapper__

    def __init__(self,name:str=None,*,
                    const:bool=False,optional:bool=False,
                    constr:typing.List[typing.Callable[[typing.Any],bool]]=[],
                    kall_constr:typing.List[typing.Callable[
                                                [typing.Any],bool]]=[],
                    kany_constr:typing.List[typing.Callable[
                                                [typing.Any],bool]]=[],
                    vall_constr:typing.List[typing.Callable[
                                                [typing.Any],bool]]=[],
                    vany_constr:typing.List[typing.Callable[
                                                [typing.Any],bool]]=[]):
        super().__init__(name,dict,
                         const=const,optional=optional,
                         constr=[*constr,
                                 *[DictField.__kall_lambda__(f)
                                     for f in kall_constr],
                                 *[DictField.__kany_lambda__(f)
                                     for f in kany_constr],
                                 *[DictField.__vall_lambda__(f)
                                     for f in vall_constr],
                                 *[DictField.__vany_lambda__(f)
                                     for f in vany_constr]])

class TypedDictField(DictField):
    def __init__(self,name:str=None,K=str,V=object,*,
                    const:bool=False,optional:bool=False,
                    constr:typing.List[typing.Callable[[typing.Any],bool]]=[],
                    kall_constr:typing.List[typing.Callable[
                                                [typing.Any],bool]]=[],
                    kany_constr:typing.List[typing.Callable[
                                                [typing.Any],bool]]=[],
                    vall_constr:typing.List[typing.Callable[
                                                [typing.Any],bool]]=[],
                    vany_constr:typing.List[typing.Callable[
                                                [typing.Any],bool]]=[]):
        self.__key_type__ = K
        self.__val_type__ = V
        super().__init__(name,const=const,optional=optional,
                         constr=constr,
                         kall_constr=[lambda val:isinstance(val,K),
                                      *kall_constr],
                         kany_constr=kany_constr,
                         vall_constr=[lambda val:isinstance(val,V),
                                      *vall_constr],
                         vany_constr=vany_constr)

