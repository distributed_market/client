import typing
import copy

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.WARN)

#__FIELD_DEFAULT__ = object()

def err_handling_lambda(expr:typing.Callable[[typing.List,
                                              typing.Dict],
                                             typing.Any],
                        callbacks:typing.Dict[type,
                                              typing.Callable[
                                                  [Exception,
                                                   typing.List,
                                                   typing.Dict],
                                                  typing.Any]],*,
                        doc:str=None):
    def __wrapper__(*args,**kwargs):
        try: return expr(*args,**kwargs)
        except tuple(callbacks) as e: return callbacks[e.__class__](e,
                                                                    *args,
                                                                    **kwargs)
    __wrapper__.__doc__ = doc if not doc is None else \
                        expr.__doc__ if hasattr(expr,'__doc__') \
                            else expr
    return __wrapper__

class Field(object):
    def optional(self):
        return self.__optional__

    def types(self):
        return [ t for t in self.__origin__ if not t is None.__class__ ]

    def __init__(self,name:str=None,
                    origin:typing.Tuple[typing.List[type]]=(object,),*,
                    const:bool=False,optional=False,
                    #default_generator:typing.Generator=iter(lambda:None,1),
                    constr:typing.List[typing.Callable[[typing.Any],bool]]=[]):
        #logger.info('Field.init({},{})'.format(name,origin))
        #logger.info('    Field.init##constant: {}'.format(const))
        #logger.info('    Field.init##default value generator: {}'.format(default_generator))
        #logger.info('    Field.init##constraints: {}'.format(constr))
        if not all(isinstance(o,type) for o in origin):
            raise TypeError('origins must be types')

        self.__name__    = name
        self.__targ__    = '_{}'.format(name)
        self.__origin__  = origin if not optional else (*origin,None.__class__)
        self.__const__   = const
        self.__constr__  = [err_handling_lambda(c if not optional else
                                                  lambda val: True if val is None
                                                                   else c(val),
                                                {Exception:lambda e,val:False})
                                for c in constr]
        self.__optional__ = optional
        #self.__default__ = default_generator
        #self.__default__.__next__()

    def __get__(self,instance,owner=None):
        return getattr(instance,self.__targ__,None)
    
    #def __set__(self,instance,value=__FIELD_DEFAULT__):
    def __set__(self,instance,value):
        if self.__const__ and \
             hasattr(instance,self.__targ__) \
                and not getattr(instance,self.__targ__) is value:
                    raise RuntimeError('field ${} is not reassignable'.format(
                                            self.__name__))
        #defgen = copy.copy(self.__default__)
        #if value is __FIELD_DEFAULT__: value = defgen.__next__()
        for o in self.__origin__:
            if isinstance(value,o): break
        else: raise TypeError('invalid type for field ${}: {}'.format(
                                self.__name__,value.__class__))
        for c in self.__constr__:
            if not c(value): 
                raise ValueError('failed constraint for field ${}: {}'.format(
                                  self.__name__,c.__doc__ if c.__doc__ else c))
        #self.__default__ = defgen
        setattr(instance,self.__targ__,value)

    def __del__(self,instance=None):
        if not instance is None: delattr(instance,self.__targ__)

class FieldMeta(type):
    @staticmethod
    def __init_wrapper__(__init__:typing.Callable):
        def __wrapper__(self,*args,**kwargs):
            __init__(self) if __init__ is object.__init__ \
                           else __init__(self,*args,**kwargs)
            if not self.__validate__():
                raise TypeError('object constraints violation')
        return __wrapper__

    @staticmethod
    def __validate__(self):
        try:
            for fname,field in self.__class__.__dict__.items():
                if isinstance(field,Field):
                    setattr(self,fname,getattr(self,fname))
            return True
        except Exception as e:
            #logger.warn(f'field validation failed: {e}')
            return False

    def __new__(cls,clsname,bases,attr,*,
                validator:typing.Callable[[typing.Any],bool]=None):
                #dictifier:typing.Callable[[typing.Any],dict]=None):
        #logger.info('FieldMeta.new({},{},{})'.format(clsname,bases,attr))
        if not hasattr(attr,'__slots__'): attr['__slots__'] = []
        for a,atr in attr.items():
            if isinstance(atr,Field):
                #logger.info('    FieldMeta.new##found Field object: {}'.format(a))
                #logger.info('    FieldMeta.new##creating private field...')
                if atr.__name__ == None: 
                    atr.__name__ = a
                    atr.__targ__ = f'_{a}'
                if not atr.__targ__ in attr['__slots__']: 
                    attr['__slots__'].append(atr.__targ__)
        if not validator is None:
            attr['__validate__'] = validator
        elif not '__validate__' in attr:
            attr['__validate__'] = FieldMeta.__validate__
        #if not dictifier is None:
        #    attr['to_dict'] = dictifier
        #elif not 'to_dict' in attr:
        #    attr['to_dict'] = FieldMeta.to_dict
        inst = super().__new__(cls,clsname,bases,attr)
        #inst.__init__ = FieldMeta.__init_wrapper__(inst.__init__)
        return inst
