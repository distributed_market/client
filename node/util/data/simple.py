import typing

from .core import Field

class SingleTypeField(Field):
    def __init__(self,name:str=None,
                    type:type=object,*,
                    const:bool=False,optional:bool=False,
                    constr:typing.List[typing.Callable[[typing.Any],bool]]=[]):
        super().__init__(name,(type,),
                         const=const,optional=optional,constr=constr)

class StringField(SingleTypeField):
    def __init__(self,name:str=None,*,
                    const:bool=False,optional:bool=False,
                    constr:typing.List[typing.Callable[[typing.Any],bool]]=[]):
        super().__init__(name,type=str,
                         const=const,optional=optional,
                         constr=constr)

class NumericField(Field):
    def __init__(self,name:str=None,*,
                    const:bool=False,optional:bool=False,
                    min:typing.Union[int,float]=None,
                    max:typing.Union[int,float]=None,
                    constr:typing.List[typing.Callable[[typing.Any],bool]]=[]):
        super().__init__(name,origin=(int,float),
                         const=const,optional=optional,
                         constr=[*constr,
                                 lambda val: min is None or val>=min,
                                 lambda val: max is None or val<max])
