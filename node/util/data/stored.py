"""Instance-tracking metaclass. 

Maintainer: Aachne Dirkschneider, <n45t31@protonmail.com>, ©14.10.2019

Classes:
    StoredInstanceMeta(type)    implements instance-tracking

Imports:
    typing
    copy
"""

import typing
import copy

import sys
import _ctypes

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.WARN)

# CPython makes direct memory addressing available, thus allowing for
# unintrusive instance tracking without reference counter incrementation
# as of 0.0.1, under any other implementation cleanup __del__ calls must
# be run manually, as <class>.__instances__ hold redundant references
# to all created objects - thus, auto-cleanup is run at each assignment
if sys.implementation.name=='cpython':
    def __reference__(obj): return id(obj)
    def __dereference__(id):
        if not isinstance(id,int):
            raise TypeError(f'invalid argument type: {id.__class__}, int expected')
        return _ctypes.PyObj_FromPtr(id)
else:
    # to avoid potentially costly resizes, override with usable empty slots
    def __sanitize_dict__(store):
        for key in store.keys():
            if sys.getrefcount(store[key])==2:
                store[key] = None
    def __sanitize_list__(store):
        for i in range(len(store)):
            if sys.getrefcount(store[i])==2:
                store[i] = None
    def __dereference__(obj): return obj
    def __dereference__(obj): return obj


class StoredInstanceMeta(type):
    def store(cls,inst):
        cls[cls.__key__(inst)] = inst
    def __contains__(self,key):
        return key in self.__instances__
    def __getitem__(cls:type,key):
        try:
            if not cls.__sanitized__: cls.__sanitize__(cls.__instances__)
            cls.__sanitized__ = True
        except: pass
        return __dereference__(cls.__instances__[key])
    def __setitem__(cls:type,key,inst):
        if not isinstance(inst,cls):
            raise TypeError('invalid argument type: {}, {} expected'.format(
                                inst.__class__,cls))
        if not cls.__key__(inst)==key:
            raise ValueError('cannot store {}(id={}) at @{}'.format(
                                inst,cls.__key__(inst),key))
        try:
            if not cls.__sanitized__: cls.__sanitize__(cls.__instances__)
            cls.__sanitized__ = True
        except: pass
        try:
            curr = cls[key]
            # empty slot can be overwritten
            if not ((curr is None)or(curr is inst)):
                raise RuntimeError('{} object at @{} exists'.format(cls,key))
        except KeyError:
            pass
        cls.__instances__[key] = __reference__(inst)
        cls.__sanitized__ = False
    def __delitem__(cls:type,key): cls.__instances__.__delitem__(key)

    @staticmethod
    def __new_wrapper__(__new__:typing.Callable[ 
                            [type,tuple,dict],typing.Any]):
        """Modifies class instance creation to satisfy unique indexing.

        By convention, if class already contains an instance at specified index,
        RuntimeError will be raised.

        Handles this single awful edge-case of @object.__new__(*args,**kwargs)
        not actually accepting *args nor **kwargs in Py3.
        """
        def wrapper(cls:type,*args:tuple,**kwargs:dict):
            instance = __new__(cls) \
                        if __new__ is object.__new__ \
                        else __new__(cls,*args,**kwargs)
            cls[cls.__key__(instance)] = instance
            return instance
        return wrapper
    @staticmethod
    def __del_wrapper__(__del__:typing.Optional[typing.Callable[
                            [typing.Any],None]]):
        if not __del__:
            __del__ = lambda self: None
        def wrapper(self:typing.Any):
            try: self.__class__.__delitem__(self.__class__.__key__(self))
            except KeyError as e: pass #print(e)
            __del__(self)
        return wrapper

    @classmethod
    def __prepare__(cls:type,clsname:str,
                    bases:typing.List[type],*,
                    _instance_key:typing.Callable[[typing.Any],typing.Any]\
                            =lambda inst: id(inst),
                    _instance_store:typing.Optional[typing.Dict]=None,
                    _copy_config:typing.Optional[type]=None,
                    _sanitizer=None):
        """Prepares class namespace.
        
        To resolve name conflicts when deriving (multiple) StoredInstanceMeta
        classes @_copy_config should specify whose $__key__ and $__instances__
        should be inherited (or None if argument values are to be used).
        """
        logger.info('StoredInstanceMeta.prepare({},{})'.format(clsname,bases))
        
        attr = super().__prepare__(clsname,bases)

        __key__       = _instance_key
        __origin__    = _copy_config \
                            if (_copy_config in bases)and \
                               (isinstance(_copy_config,StoredInstanceMeta)) \
                            else None
        __instances__ = _instance_store \
                            if _instance_store is not None else dict()
        
        __sanitize__  = None
        try:
            if isinstance(__instances__,dict): __sanitize__ = __sanitize_dict__
            elif isinstance(__instances__,list): __sanitize__ = __sanitize_list__
        except: pass
        if _sanitizer is not None: __sanitize__ = _sanitizer

        __sanitized__ = True

        logger.info('    StoredInstanceMeta.prepare##key picker: {}'.format(__key__))
        logger.info('    StoredInstanceMeta.prepare##origin: {}'.format(__origin__))
        logger.info('    StoredInstanceMeta.prepare##instance store: {}'.format(__instances__))
        logger.info('    StoredInstanceMeta.prepare##sanitizer: {}'.format(__sanitize__))
        if __origin__:
            __key__ = __origin__.__key__
            __instances__ = copy.copy(__origin__.__instances__)
            for k in __origin__.__instances__:
                __instances__.__delitem__(k)
        # test instance container indexing capacity
        elif not all(hasattr(__instances__,a) for \
                       a in ['__getitem__','__setitem__','__delitem__']):
            raise ArgumentError('{} missing (some) indexing capacity'.format(__instances__))

        attr['__key__'] = __key__
        attr['__origin__'] = __origin__
        attr['__instances__'] = __instances__
        attr['__sanitize__'] = __sanitize__
        attr['__sanitized__'] = __sanitized__
        return attr

    def __new__(cls:type,clsname:str,
                bases:typing.List[type],
                attr:typing.Dict[str,typing.Any],
                **kwargs:typing.Dict[str,typing.Any]):
        logger.info('StoredInstanceMeta.new({},{},{})'.format(clsname,bases,attr))
        instance = super().__new__(cls,clsname,bases,attr)
        source = instance.__origin__ \
                    if instance.__origin__ \
                    else StoredInstanceMeta
        logger.info('    StoredInstanceMeta.new##new wrapper: {}'.format(source.__new_wrapper__))
        logger.info('    StoredInstanceMeta.new##del wrapper: {}'.format(source.__del_wrapper__))
        instance.__new__ = source.__new_wrapper__(instance.__new__)
        instance.__del__ = source.__del_wrapper__(
                                getattr(instance,'__del__',None))
        return instance

    def __init__(cls:type,clsname:str,
                 bases:typing.List[type],
                 attr:typing.Dict[str,typing.Any],
                 **kwargs:typing.Dict[str,typing.Any]):
        super().__init__(clsname,bases,attr) 

