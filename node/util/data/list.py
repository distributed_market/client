import typing

from .simple import SingleTypeField

class ListField(SingleTypeField):
    def __list_lambda__(func):
        def __wrapper__(val): return all(func(i) for i in val)
        return __wrapper__
    def __elem_lambda__(func):
        def __wrapper__(val): return any(func(i) for i in val)
        return __wrapper__

    def __init__(self,name:str=None,*,
                    const:bool=False,optional:bool=False,
                    constr:typing.List[typing.Callable[[typing.Any],bool]]=[],
                    all_constr:typing.List[typing.Callable[
                                               [typing.Any],bool]]=[],
                    any_constr:typing.List[typing.Callable[
                                               [typing.Any],bool]]=[]):
        super().__init__(name,list,
                         const=const,optional=optional,
                         constr=[*constr,
                                 *[ListField.__list_lambda__(ac)
                                        for ac in all_constr],
                                 *[ListField.__elem_lambda__(ac)
                                        for ac in any_constr]])

class TypedListField(ListField):
    def __init__(self,name:str=None,type=object,*,
                    const:bool=False,optional:bool=False,
                    constr:typing.List[typing.Callable[[typing.Any],bool]]=[],
                    all_constr:typing.List[typing.Callable[[typing.Any],bool]]=[],
                    any_constr:typing.List[typing.Callable[[typing.Any],bool]]=[]):
        self.__item_type__ = type
        super().__init__(name,const=const,optional=optional,
                         constr=constr,
                         all_constr=[lambda val:isinstance(val,type),
                                     *all_constr],
                         any_constr=any_constr)

#class SelectField(TypedListField):
#    def __init__(self,name:str=None,target:type=object,*,
#                 const:bool=False,optional:bool=False,
#                 constr:typing.List[typing.Callable[[typing.Any],bool]]=[],
#                 all_constr:typing.List[typing.Callable[[typing.Any],bool]]=[],
#                 any_constr:typing.List[typing.Callable[[typing.Any],bool]]=[]):
#        self.__target__ = target
#        super().__init__(name,type=str,const=const,optional=optional,
#                         constr=constr,
#                         all_contr=[lambda val:hasattr(target,val),*all_constr],
#                         any_constr=any_constr)
#    def __set__(self,instance,value):
#        if isinstance(value,str) and value == '*':
#            value = [ k for k,v in target.__dict__.items() if isinstance(v,Field) ]
#        super().__set__(instance,value)
