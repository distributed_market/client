import typing

def err_handling_lambda(expr:typing.Callable[[typing.List,
                                              typing.Dict],
                                             typing.Any],
                        callbacks:typing.Dict[type,
                                              typing.Callable[[Exception,
                                                               typing.List,
                                                               typing.Dict],
                                                              typing.Any]],
                        *,doc:str=None):
    def __wrapper__(*args,**kwargs):
        try: return expr(*args,**kwargs)
        except tuple(callbacks) as e: return callbacks[e.__class__](e,  
                                                                    *args,
                                                                    **kwargs)
    __wrapper__.__doc__ = doc if not doc is None else \
                          expr.__doc__ if hasattr(expr,'__doc__') \
                          else expr
    return __wrapper__

def optional_lambda(expr:typing.Callable[[typing.Any],bool]):
    def __wrapper__(arg):
        return arg is None or expr(arg)
    return __wrapper__

def all_lambda(expr:typing.Callable[[typing.Collection],bool]):
    def __wrapper__(args):
        return all(expr(a) for a in args)
    return __wrapper__

def any_lambda(expr:typing.Callable[[typing.Collection],bool]):
    def __wrapper__(args):
        return any(expr(a) for a in args)
    return __wrapper__

__sanitized__ = ',?"\'`$~!&|%^()<>{}[];!='
__substituted__ = ':/\\'
def sanitize_str(string:str):
    for c in __sanitized__: string = string.replace(c,'')
    for c in __substituted__: string = string.replace(c,'-')
    return string.replace(' ','_')
