from ...logging import getPlainLogger
logger = getPlainLogger(__name__)
logger.info('importing pycrypto mappings...')

try:
    from Crypto.Cipher import AES,DES3,Blowfish
    from Crypto.Hash import HMAC,MD5,SHA,SHA256,SHA384,SHA512
    from Crypto.PublicKey import RSA,DSA,ElGamal
    from Crypto.Protocol.KDF import PBKDF1,PBKDF2
except Exception as e:
    logger.error(f'{e.__class__} while importing pycrypto: {e}')

class BlockCipher(object):
    def __init__(self,algo,bs,ks,mode):
        self.block_size = bs
        self.key_size = ks
        self.__algo__ = algo
        self.mode = mode
    def __call__(self,key,iv):
        if self.mode == self.__algo__.MODE_ECB:
            return self.__algo__.new(key,mode=self.mode)
        return self.__algo__.new(key,mode=self.mode,IV=iv)

class KDF(object):
    def __init__(self,caller):
        self.__call__ = caller
    def __call__(self,p,s,i,h,m):
        self.__pass__ = p
        self.__salt__ = s
        self.__iter__ = i
        self.__hash__ = h
        self.__hmac__ = m
        return self
    def read(self,bytesnum):
        return self.__call__(self.__pass__,self.__salt__,
                self.__iter__,self.__hash__,self.__hmac__,bytesnum)

def __algo_mods__(cls,ks=None):
    if ks is None: ks = cls.key_size
    return { m.split('_')[1].lower() : BlockCipher(cls,cls.block_size,
                                                 ks,getattr(cls,m))
             for m in ('MODE_ECB','MODE_CBC','MODE_CFB','MODE_OFB') }

CIPHERS = {}
CIPHERS['aes'] = {
        str(int(ks*8)) : __algo_mods__(AES,ks)
        for ks in AES.key_size }
CIPHERS['des3'] = __algo_mods__(DES3)
CIPHERS['des'] = { '3' : CIPHERS['des3'] }
CIPHERS['blowfish'] = __algo_mods__(Blowfish)

DIGESTS = {
        'md5' : MD5,
        'sha1' : SHA,
        'sha' : {
            '256' : SHA256,
            '384' : SHA384,
            '512' : SHA512,
        },
        'hmac' : HMAC,
}

ASMALGS = {
        'rsa' : RSA,
        'dsa' : DSA,
        'el_gamal' : ElGamal,
}

### CONFIG MAPPINGS ###

HS_KEX = ASMALGS

HS_KDF = {
        'pbkdf1' : KDF(lambda p,s,i,h,_,kl : PBKDF1(p,s,kl,i,h)),
        'pbkdf2' : KDF(lambda p,s,i,h,m,kl : PBKDF2(p,s,kl,i,
                                                    lambda key,msg:m.new(key,msg,h)
                                                                        .digest()))
}
HS_KDF_DGST = DIGESTS
HS_KDF_HMAC = { 'hmac' : HMAC }

HS_ENC = CIPHERS
HS_DGST = DIGESTS

