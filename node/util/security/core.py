from collections import OrderedDict
import os
os = os

from ...config import config

from ..logging import getLogger,concat
logger = getLogger(__name__,'security.log')

def __update__(target,source):
    if not isinstance(target,dict): return False
    if isinstance(source,dict):
        for k,v in source.items():
            if not k in target:
                target[k] = OrderedDict()
                target[k].parent = target
                target[k].key = k
            if not __update__(target[k],v): target[k] = v
    else:
        try: target.parent[target.key] = source
        except: target[None] = source
    return True

class SecurityModule(object):
    def __new__(cls):
        try:
            if cls.__loaded__ == None:
                raise AttributeError()
        except AttributeError: 
            cls.__sources__ = OrderedDict()
            cls.__loaded__ = {}
        return cls.__loaded__

    __converters__ = { 'i' : int, 's' : str, 'c' : ord, 'f' : float }
    @classmethod
    def __parse__(cls,algo:str):
        if algo is None: return (),cls.__sources__
        if len(algo) == 0: return None,{}
        if algo[0] == '@':
            return SecurityModule.__converters__[algo[1]](algo[2:]),None
        algo = algo.split(':')
        if len(algo) == 1: sources = cls.__sources__
        else: algo,sources = algo[1:],{algo[0]:cls.__sources__[algo[0]]}
        return algo[0].split('-'),sources

    @classmethod
    def reload_section(cls,section):
        if section == 'HS_SRC': return cls.reload()
        logger.info(f'updating section {section}...')
        try:
            for algo in config.get('security',section).split(','):
                try: 
                    algo,sources = cls.__parse__(algo)
                    if sources is None: 
                        cls.__loaded__[section] = algo
                        continue
                    logger.info(concat(f'${algo} lookup path:\n',sources))
                except Exception as e:
                    logger.error(f'{e.__class__} while resolving lookup path: {e}')
                    continue
                if not section in cls.__loaded__:
                    cls.__loaded__[section] = OrderedDict()
                for modname,source in sources.items():
                    logger.debug(f'updating {section}[{modname}]...')
                    try:
                        res = {}
                        ptr1,ptr2 = res,getattr(source,section)
                        for a in algo[:-1]:
                            ptr1[a] = {}
                            ptr1,ptr2 = ptr1[a],ptr2[a]
                        ptr1[algo[-1]] = ptr2[algo[-1]]
                        logger.info(concat(f'updating {section}[{modname}] with data:\n',res))
                    except Exception as e:
                        logger.warning(f'{e.__class__}: {source.__name__} does not support ${algo} - {e}')
                        continue
                    if not modname in cls.__loaded__[section]:
                        cls.__loaded__[section][modname] = OrderedDict()
                    __update__(cls.__loaded__[section][modname],res)
        except Exception as e:  
            logger.error(f'{e.__class__} while updating {section} : {e}')
            raise 

    @classmethod
    def reload(cls):
        logger.info(f'reloading security module')
        from importlib import import_module
        cls.__sources__ = OrderedDict()
        cls.__loaded__ = {}
        for source in config.get('security','HS_SRC',fallback='').split(','):
            if len(source) == 0: continue
            logger.info(f'importing module {source}...')
            try: cls.__sources__[source] = import_module(f'.{source}',__package__)
            except Exception as e: 
                logger.error(f'{e.__class__}: no support for {source}')
                logger.error(e)
        try: 
            for section in config.options('security'): 
                if section.startswith('HS_') and not section == 'HS_SRC':
                    cls.reload_section(section)
            logger.info(concat('SECURITY MODULES:\n',cls.__loaded__))
        except Exception as e:
            logger.error(f'{e.__class__} while loading security module:  {e}')

    @classmethod
    def get(cls,section,option=None,*,fallback=None):
        try:
            if not isinstance(cls.__loaded__[section],dict):
                #return { section : cls.__loaded__[section] }
                return cls.__loaded__[section]
            algo,sources = cls.__parse__(option)
            logger.info(concat('GET @{section},{option} lookup path:\n',source))
            result = OrderedDict()
            def trace(source,path=algo):
                if isinstance(source,dict):
                    for k,v in source.items():
                        trace(v,[*path,k])
                else: result[f'{modname}:{"-".join(path)}'] = source
            for modname in sources:
                try:
                    source = cls.__loaded__[section][modname]
                    logger.debug(f'searching {source.__name__}...')
                    for a in algo:
                        try: 
                            source = source[a]
                            logger.debug(f'{a} matched')
                        except: 
                            logger.debug(f'{a} not found, aborting')
                            break
                    else:
                        trace(source)
                        logger.info(concat('entries found:',result))
                except Exception as e: 
                    logger.warning(f'{e.__class__} while searching for {modname}: {e}')
            try: result.default = result.items().__itr__().__next__()
            except: return fallback
            return result
        except Exception as e:
            logger.error(f'{e.__class__} while fetching @{section},{option}: {e}')
        return fallback

SALT_MARKER = b'$'
MAX_ITER = 65536
DEF_ITER = 10000
import struct
def encrypt(infile,outfile,passwd,algo,*,
            itr=None,kdf=None,hasher=None,macmod=None):
    logger.info(f'encrypting with {algo}...')
    logger.info(f'key-derivation function: {kdf}')
    logger.info(f'digest function: {hasher}')
    logger.info(f'pseudo-random function: {macmod}')
    bs = algo.block_size
    if not isinstance(bs,int): bs = max(bs)
    if itr is None: itr = SecurityModule.get('HS_KDF_ITER',fallback=DEF_ITER)
    logger.info(f'iterations: {itr}')
    if itr >= MAX_ITER: raise ValueError(f'too many iterations: {itr}/{MAX_ITER}')
    header = SALT_MARKER + struct.pack('!H',itr) + SALT_MARKER
    salt = os.urandom(bs - len(header))

    if isinstance(passwd,str): passwd = str.encode(passwd,'utf-8')
    try: 
        if hasher is None: hasher = SecurityModule.get('HS_KDF_DGST').default[1]
        if macmod is None: macmod = SecurityModule.get('HS_KDF_HMAC').default[1]
        ks = algo.key_size
        if not isinstance(ks,int): ks = max(ks)
        if kdf is None: kdf = SecurityModule.get('HS_KDF').default[1]
        logger.info(f'deriving key ({ks} bytes) with {kdf}[{hasher},{macmod}]')
        key = kdf(passwd,salt,itr,hasher,macmod).read(ks)
    except Exception as e:
        logger.critical(f'{e.__class__} while deriving key for {algo}: {e}')
        raise
    
    iv = os.urandom(bs)
    cipher = algo(key,iv)
    logger.debug(f'encrypting stream with {cipher}[{salt},{iv},{key}]')
    outfile.write(header + salt)
    outfile.write(iv)
    
    while True:
        chunk = infile.read(1024 * bs)
        if len(chunk) == 0 or len(chunk) % bs != 0:
            padlen = (bs - len(chunk) %bs) or bs
            chunk += (padlen*chr(padlen)).encode()
            outfile.write(cipher.encrypt(chunk))
            break
        outfile.write(cipher.encrypt(chunk))
    infile.seek(0)
    outfile.seek(0)

def decrypt(infile,outfile,passwd,algo,*,
            kdf=None,hasher=None,macmod=None):
    logger.info(f'decrypting with {algo}...')
    logger.info(f'key-derivation function: {kdf}')
    logger.info(f'digest function: {hasher}')
    logger.info(f'pseudo-random function: {macmod}')
    bs = algo.block_size
    if not isinstance(bs,int): bs = max(bs)
    _,itr,salt = infile.read(bs).split(SALT_MARKER)
    itr = struct.unpack('!H',itr)[0]
    logger.info(f'iterations: {itr}')

    if isinstance(passwd,str): passwd = str.encode(passwd,'utf-8')
    try:
        if hasher is None: hasher = SecurityModule.get('HS_KDF_DGST').default[1] 
        if macmod is None: macmod = SecurityModule.get('HS_KDF_HMAC').default[1] 
        ks = algo.key_size
        if not isinstance(ks,int): ks = max(ks)
        if kdf is None: kdf = SecurityModule.get('HS_KDF').default[1] 
        logger.info(f'deriving key ({ks} bytes) with {kdf}[{hasher},{macmod}]')
        key = kdf(passwd,salt,itr,hasher,macmod).read(ks)
    except Exception as e:
        logger.critical(f'{e.__class__} while deriving key for {algo}: {e}')
        raise

    iv = infile.read(bs)
    cipher = algo(key,iv)
    logger.debug(f'decrypting stream with {cipher}[{salt},{iv},{key}]')
    curr,next = None,b''
    
    while True:
        curr,next = next,cipher.decrypt(infile.read(1024*bs))
        if not next:
            if isinstance(curr[-1],str):
                padlen = ord(curr[-1])
                padding = padlen * curr[-1]
            else:
                padlen = curr[-1]
                padding = (padlen * chr(padlen)).encode('utf-8')
            if not padlen in range(1,bs+1): raise ValueError(f'bad padding len: {padlen}')
            if not curr[-padlen:] == padding: raise ValueError('bad padding string')

            curr = curr[:-padlen]
            outfile.write(curr)
            break
        outfile.write(curr)
    infile.seek(0)
    outfile.seek(0)
