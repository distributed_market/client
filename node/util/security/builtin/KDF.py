import io
import struct

class FallbackFastKDF(object):
    def __init__(self,passwd,salt,iters,hasher,macmod):
        self.__passwd__ = passwd if isinstance(passwd,bytes) \
                                 else bytes(passwd,'utf-8')
        self.__pwsalt__ = salt if isinstance(salt,bytes) \
                               else bytes(salt,'utf-8')
        self.__kditer__ = iters
        self.__hasher__ = hasher
        self.__buffer__ = b''

    def read(self,bytenum):
        with io.BytesIO() as bi:
            bi.write(self.__buffer__)
            di = b''
            while bi.tell() < bytenum:
                di = self.__hasher__.new(di + self.__passwd__ + self.__pwsalt__).digest()
                bi.write(di)
            bi.seek(0)
            self.__buffer__ = bi.getvalue()
            return bi.read(bytenum)

class FallbackPBKDF2(object):
    def __init__(self,passwd,salt,iters,hasher,macmod):
        self.__passwd__ = passwd if isinstance(passwd,bytes) \
                                 else bytes(passwd,'utf-8')
        self.__pwsalt__ = salt if isinstance(salt,bytes) \
                               else bytes(salt,'utf-8')
        self.__kditer__ = iters
        self.__hasher__ = hasher
        self.__macmod__ = macmod
        self.__prfunc__ = lambda v: self.__macmod__.new(
                                    self.__passwd__,v,self.__hasher__).digest()
        self.__buffer__ = b''
        self.__blocks__ = 0

    def read(self,bytenum):
        with io.BytesIO() as bi:
            bi.write(self.__buffer__)
            di = b''
            while bi.tell() < bytenum:
                di = self.__blkfun__(self.__blocks__)
                self.__blocks__ += 1
                bi.write(di)
            bi.seek(0)
            self.__buffer__ = bi.getvalue()
            return bi.read(bytenum)

    def __blkfun__(self,blknum):
        out = dgst = self.__prfunc__(self.__pwsalt__ + struct.pack('!L',blknum))
        itrnum = 1
        while itrnum < self.__kditer__:
            itrnum += 1
            dgst = self.__prfunc__(dgst)
            out = bytes(i^j for i,j in zip(out,dgst))
        return out
