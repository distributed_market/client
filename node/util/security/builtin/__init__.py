from ...logging import getPlainLogger
logger = getPlainLogger(__name__)

try:
    logger.info('initiating builtin module with hashlib...')
    class __hashwrapper__(object):
        def __init__(self,hashalgo): self.__algo__ = hashalgo
        def new(self,data): return self.__algo__(data)
    from hashlib import sha1,sha256,sha384,sha512,sha3_256,sha3_384,sha3_512
    DIGESTS = {
            'sha1' : __hashwrapper__(sha1),
            'sha' : {
                '256' : __hashwrapper__(sha256),
                '384' : __hashwrapper__(sha384),
                '512' : __hashwrapper__(sha512),
            },
            'sha3' : {
                '256' : __hashwrapper__(sha3_256),
                '384' : __hashwrapper__(sha3_384),
                '512' : __hashwrapper__(sha3_512),
            },
    }
#    DIGESTS['sha'] = DIGESTS['sha2']
#    DIGESTS['sha']['3'] = DIGESTS['sha3']
#    DIGESTS['sha']['2'] = DIGESTS['sha2']
#    DIGESTS['sha']['1'] = DIGESTS['sha1']
except Exception as e: 
    logger.error(f'{e.__class__} while initiating hashlib: {e}')
    DIGESTS = {}
import hmac
DIGESTS['hmac'] = hmac

from .KDF  import *

CIPHERS = {
}

ASYMALGS = {
}

### CONFIG MAPPINGS ###

HS_KDF = {
    'ffkdf' : FallbackFastKDF,
    'pbkdf2' : FallbackPBKDF2,
}
HS_KDF_DGST = DIGESTS
HS_KDF_HMAC = { 'hmac' : hmac }

HS_KEX = {}

HS_ENC = CIPHERS
HS_DGST = DIGESTS
