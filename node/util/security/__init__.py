from .core import encrypt,decrypt,SecurityModule

import logging
logger = logging.getLogger(__name__)
logger.propagate = False
from sys import stderr
logger.addHandler(logging.StreamHandler(stderr))
logger.handlers[0].setFormatter(logging.Formatter('[{levelname:^8}] {message}',style='{'))

try:
    logger.info('checking pycrypto')
    from .pycrypto import CIPHERS,DIGESTS,HS_KDF,HS_KDF_HMAC
    AES = CIPHERS['aes']['256']['cbc']
    SHA256 = DIGESTS['sha']['256']
    KDF = HS_KDF['pbkdf2']
    HMAC = HS_KDF_HMAC['hmac']
    SecurityModule.status = 1
except Exception as e:
    logger.error(f'{e.__class__} while importing pycrypto: {e}')
    logger.info('checking builtin')
    try:
        from .builtin import CIPHERS,DIGESTS,HS_KDF,HS_KDF_HMAC
        AES = CIPHERS['ffea']
        SHA256 = DIGESTS['sha']['256']
        KDF = HS_KDF['pbkdf2']
        HMAC = HS_KDF_HMAC['hmac']
        SecurityModule.status = 0
    except Exception as ee:
        logger.critical(f'{ee.__class__} while importing builtin: {ee}')
        SecurityModule.status = -1 
