import logging
logger = logging.getLogger(__name__)
from sys import stderr
logger.addHandler(logging.StreamHandler(stderr))
logger.handlers[0].setFormatter(logging.Formatter('[{levelname:^8}] {message}',style='{'))
logger.propagate = False
logger.info('initializing account security module...')

try:
    from ..util.security import AES,SHA256,KDF,HMAC,encrypt as enc,decrypt as dec
    logger.info(f'imported: AES={AES}')
    logger.info(f'imported: SHA256={SHA256}')
    logger.info(f'imported: KDF={KDF}')
    logger.info(f'imported: HMAC={HMAC}')
    def encrypt(infile,outfile,passwd):
        return enc(infile,outfile,passwd,AES,kdf=KDF,hasher=SHA256,macmod=HMAC)
    def decrypt(infile,outfile,passwd):
        return dec(infile,outfile,passwd,AES,kdf=KDF,hasher=SHA256,macmod=HMAC)
except Exception as e:
    logger.critical(f'ATTENTION')
    logger.critical(f'{e.__class__} while loading security modules: {e}')
    logger.critical(f'NO ENCRYPTION PROVIDED!')
    from hashlib import sha256
    SHA256 = lambda v: sha256(v)
    SHA256.new = SHA256
    def encrypt(infile,outfile,passwd):
        outfile.write(infile.read())
        outfile.seek(0)
        infile.seek(0)
    def decrypt(infile,outfile,passwd):
        outfile.write(infile.read())
        outfile.seek(0)
        infile.seek(0)
