import io
import os

from .core import *
from .options import *
from .session import *
from . import security

def __user_gen__():
    i = 0
    while True:
        i += 1
        yield { 'name'   : f'user{i}',
                'passwd' : f'hasło usera testowego {i}',
                'home'   : f'./user_{i}' }
user_gen = __user_gen__()

def test_user(user):
    assert user['name'] in get_accounts(), \
            f"no entry for user '{user['name']}'"
    assert os.path.isdir(user['home']), \
            f"no directory for user '{user['name']}'"
    with open(os.path.join(user['home'],'.test.enc'),'rb') as pfile, \
            io.BytesIO() as dpass:
        security.decrypt(pfile,dpass,user['passwd'])
        assert dpass.getvalue() == SHA256.new(str.encode(
                                                user['passwd'])).digest(), \
                'error decoding password SHA'

INIT_LEN = len(get_accounts())

## Test 1: single user create-and-delete
user = next(user_gen)
create_account(**user)
assert len(get_accounts()) == INIT_LEN + 1, \
        f"failed to write user entry for '{user['name']}'"
test_user(user)
delete_account(user['name'],user['passwd'])
assert len(get_accounts()) == INIT_LEN, \
        f"failed to clear user entry for '{user['name']}'"
assert not os.path.exists(user['home']), \
        f"failed to cleanup user directory for '{user['name']}'"

## Test 2: username collision
create_account(**user)
try: create_account(**user)
except RuntimeError as e:
    assert str(e) == f"user {user['name']} already exists", \
        f"invalid error message: {e}"
assert len(get_accounts()) == INIT_LEN + 1, \
        f"invalid user entries count added: {len(get_accounts())-INIT_LEN}"
delete_account(user['name'],user['passwd'])

## Test 3: invalid password on delete
create_account(**user)
for p in (user['passwd'][0:4],
          user['passwd'].replace(user['passwd'][-1],'_'),
          user['passwd'][0:-1]):
    try: delete_account(user['name'],p)
    except Exception as e: pass
    assert len(get_accounts()) == INIT_LEN + 1, \
        f"deleted user '{user['name']}' with password '{user['passwd']}'"
delete_account(user['name'],user['passwd'])

## Test 4: update user account
create_account(**user,
               TOR_PORT=39909,
               CHANNEL='SOCK',SOCK='ths.sock')
delete_account(user['name'],user['passwd'])
