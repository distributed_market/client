import os,io
os = os
io = io

from ...config import config
from ...errors import HomeFileTreeError
from ...util.logging import getLogger
from ...util.security import SecurityModule

from ..security import SHA256,encrypt,decrypt

LOGGER_STR = f'{__package__}.{{}}.auth'

def get_cert(*,overwrite=False):
    user,home,passwd = config.get_user() 
    logger = getLogger(LOGGER_STR.format(user),
                            os.path.join('accounts',f'{user}.log'))
    path = config.get('security','CERT')
    cert = None
    try:
        if os.path.exists(path):
            if not os.path.isfile(path): 
                raise HomeFileTreeError(f'{path} must be a file')
            with open(path,'rb') as enc,io.BytesIO() as dec:
                logger.info(f'reading existing certificate from {path}')
                decrypt(enc,dec,passwd)
                cert = dec.getvalue()
            if not overwrite: return cert
            with open(os.path.join(home,'data','peers','blacklist'),'a') as blacklist:
                blacklist.write(f'{SHA256.new(cert).digest()}\n')
                # TODO: handle broadcasting
        os.makedirs(os.path.dirname(path),exist_ok=True)
        #with open(path,'wb') as enc,io.BytesIO(str.encode(os.urandom(4096))) as dec:
        with open(path,'wb') as enc,io.BytesIO(str.encode(os.urandom(2048).hex(),'utf-8')) as dec:
            logger.info('storing new certificate...')
            encrypt(dec,enc,passwd)
            cert = dec.getvalue()
    except Exception as e:
        logger.error(f'{e.__class__} while fetching certificate from {path}')
        logger.error(e)
    return cert

def get_key(algo,keylen):
    user,home,passwd = config.get_user()
    logger = getLogger(LOGGER_STR.format(user),
                            os.path.join('accounts',f'{user}.log'))
    results = SecurityModule.get('HS_KEX',algo)
    if results is None:
        raise ValueError(f'algorithm {algo} not configured for this account')
    path = os.path.join(config.get('security','PGP_KEY_DIR'),
                        *results.default[0].split(':'),
                        f'{keylen}')
    os.makedirs(os.path.dirname(path),exist_ok=True)
    logger = getLogger(f'{__package__}.{user}.{auth}')
    logger.info(f'fetching key pair for {results.default[0]}...')

    algo = results.default[1]
    key = None
    try:
        path_enc,path_dec = f'{path}.enc',f'{path}.pub'
        if os.path.exists(path_enc):
            with open(path_enc,'rb') as enc,io.BytesIO() as dec, \
                 open(path_dec,'wb') as pub:
                logger.info(f'reading existing key pair from: {path_enc}, {path_dec}')
                decrypt(enc,dec,passwd)
                key = algo.import_key(dec.read())
                pub.write(key.publickey().exportKey())
        else:
            with open(path_enc,'wb') as enc,\
                 open(path_dec,'wb') as pub:
                logger.info('generating new key pair...')
                key = algo.generate(keylen)
                dec = io.BytesIO(key.exportKey())
                encrypt(dec,enc,passwd)
                dec.close()
                pub.write(key.publickey().exportKey())
                logger.info(f'stored: {path_enc}, {path_dec}')
    except Exception as e:
        logger.error(f'{e.__class__} while fetching key for {results.default[0]}')
        logger.error(e)
    return key
