import os
os = os

from ...config import config
from ...errors import LoginStatusError,InvalidUsernameError,HomeFileTreeError
from ...util.logging import getLogger
from ...util.security import SecurityModule

from ..manager.store import get_accounts

from .filetree import validate,read_homedir,OnLogoutLoader

LOGGER_STR = f'{__package__}.{{}}.{{}}'

def logout(dry=False):
    user,home,_ = config.get_user()
    logger = getLogger(LOGGER_STR.format(user,'logout'),
                       os.path.join('accounts',f'{user}.log'))
    try: 
        OnLogoutLoader()
        if not dry: SecurityModule.reload()
    except Exception as e:
        logger.error(f'{e.__class__} encountered while logging out: {e}')
        if not dry: raise e
    config.set('runtime','USER',None)

def login(name,passwd,*,dry=False):
    logger = getLogger(LOGGER_STR.format(name,'login'),
                       os.path.join('accounts',f'{name}.log'))
    logger.info(f'login attempt for {name}')
    e = None
    try:
        user,_,_ = config.get_user()
        e = LoginStatusError(f'already logged in as {user}')
    except LoginStatusError: pass
    if not e is None:
        logger.error(f'{e.__class__} encountered while logging in: {e}')
        raise e
    
    try: home = os.path.abspath(get_accounts()[name]['home'])
    except KeyError:
        e = InvalidUsernameError(f'no entry for username {name}')
        logger.error(f'{e.__class__} encountered while logging in: {e}')
        raise e
    logger.debug(f'decoding {home} with {passwd}')
    validate(home,passwd)

    config.set('runtime','HOME',home)
    config.set('runtime','PASS',passwd)
    config.set('runtime','USER',name)

    try: 
        read_homedir()
        if not dry: SecurityModule.reload()
    except Exception as e:
        logger.error(f'{e.__class__} while reading homedir: {e}')
        if not dry: raise HomeFileTreeError(e) from e
