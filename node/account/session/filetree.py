import os,io
os = os
io = io

from ...errors import HomeFileTreeError
from ...util.logging import getLogger
from ...util.parser.json import JSONParser
from ...data.auction import CategoryTree
from ...data.loader.dsync import AuctionLoader,PeerLoader
from ...config import config

from ..security import SHA256,encrypt,decrypt

from .auth import get_cert

LOGGER_STR = f'{__package__}.{{}}.{{}}'

def validate(home,passwd):
    path = os.path.join(home,'.test.enc')
    try:
        with open(path,'rb') as src,io.BytesIO() as dst:
            decrypt(src,dst,passwd)
            if dst.getvalue() != SHA256.new(str.encode(passwd,'utf-8')).digest():
                raise ValueError('invalid password')
    except OSError:
        raise HomeFileTreeError(f'missing PASSWD file at {path}')

def OnLoginLoader():
    user,home,passwd = config.get_user()
    al = AuctionLoader(home,JSONParser.__decoder__)
    with al:
        al.logger = getLogger(LOGGER_STR.format(user,'OnLogin'),
                              os.path.join('accounts',f'{user}.log'))
        try:
            with open('categories.json','r') as catfile:
                config.set('runtime','CATEGORIES',CategoryTree(
                            **JSONParser.__decoder__.decode(catfile.read())))
        except Exception as e:
            al.logger.error(f'{e.__class__} reading category tree: {e}')
        al.load(decrypt,passwd)
 
def OnLogoutLoader():
    user,home,passwd = config.get_user()
    al = AuctionLoader(home,JSONParser.__encoder__)
    with al:
        al.logger = getLogger(LOGGER_STR.format(user,'OnLogout'),
                              os.path.join('accounts',f'{user}.log'))
        al.flush(encrypt,passwd)
        try:
            with open('categories.json','w') as catfile:
                catfile.write(JSONParser.__encoder__.encode(
                            config.get('runtime','CATEGORIES').to_dict()))
        except Exception as e:
            al.logger.error(f'{e.__class__} writing category tree: {e}')

def read_homedir():
    user,home,passwd = config.get_user()
    logger = getLogger(LOGGER_STR.format(user,'read_homedir'),
                          os.path.join('accounts',f'{user}.log'))
    try:
        logger.info(f'reading config from {home}/.node.conf')
        path = os.path.join(home,'.node.conf')
        config.read(path)
    except Exception as e:
        logger.error(f'{e.__class__} while reading CONF: {e}')
    try: config.set('runtime','CERT',get_cert())
    except Exception as e:
        logger.error(f'{e.__class__} while reading CERT: {e}')
    try: OnLoginLoader()
    except Exception as e:
        logger.critical('error while reading data')
        logger.error(f'{e.__class__} encountered: {e}')
    try: config.set('runtime','SERVICE',get_tor_service())
    except Exception as e:
        logger.error(f'{e.__class__} while reading CERT: {e}')

def get_tor_service():
    user,home,_ = config.get_user()
    ths_dir = config.get('tor','THS_DIR')
    os.makedirs(ths_dir,exist_ok=True);
    # TODO: implement
