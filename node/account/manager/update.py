import os,io
os = os
io = io
import shutil
shutil = shutil
from pathlib import Path

from ...config import config,CP
from ...errors import LoginStatusError,InvalidUsernameError,UsernameTakenError,ConfigError

from ..security import SHA256,encrypt,decrypt
from ..session.filetree import validate
from ..session.status import login,logout

from .store import *

import re
ENCRYPTED = re.compile('\\.enc$')
LOGGER_STR = f'{__package__}.{{}}.{{}}'

def __mover_factory(home,passwd,newhome,newpasswd):
    if newpasswd == passwd:
        return lambda s,_: os.rename(s.path,os.path.join(newhome,s.path))
    def _mover(src,dst):
        dst = os.path.join(dst,src.name)
        if src.is_dir():
            os.makedirs(dst,exist_ok=True)
            for entry in os.scandir(src): _mover(entry,dst)
        elif src.is_file():
            if ENCRYPTED.match(src.name):
                with open(src.path,'rb') as enc,io.BytesIO() as dec,\
                        open(dst,'wb') as reenc:
                    decrypt(enc,dec,passwd)
                    encrypt(dec,reenc,newpasswd)
            else: 
                try: os.rename(src.path,dst)
                except: pass
    return _mover

def move_home_content(home,passwd,newhome,newpasswd,*,cleanup=True):
    if newhome is None: newhome = home
    if newpasswd is None: newpasswd = passwd
    home = os.path.abspath(home)
    newhome = os.path.abspath(newhome)
    if newhome == home and newpasswd == passwd:
        raise ValueError(f'directories and password identical')
    logger.info(f'moving homedir from {home} to {newhome}')

    mover = __mover_factory(home,passwd,newhome,newpasswd)
    os.makedirs(newhome,exist_ok=True)
    try:
        for entry in os.scandir(home): mover(entry,newhome)
        if cleanup and home != newhome:
            logger.info(f'cleaning up {home}...')
            shutil.rmtree(home)
        logger.info(f'changing password for {newhome}...')
        with io.BytesIO(SHA256.new(str.encode(newpasswd,'utf-8')).digest()) as src,\
             open(os.path.join(newhome,'.test.enc'),'wb') as dst:
            encrypt(src,dst,newpasswd)
    except Exception as e:
        logger.error(f'{e.__class__} while moving homedir: {e}')
    home = newhome
    passwd = newpasswd

def update_account(name:str,passwd:str,newhome:str=None,newpasswd:str=None,*,conf=None,
                   TOR_PORT=None,TOR_PASS=None,THS_DIR=None,THS_PORT=None,
                   CHANNEL=None,PORT=None,SOCK=None,MODE=None,RELAY=None,URL=None,
                   CERT=None,PGP_KEY_DIR=None,HS_SRC=None,
                   HS_KDF=None,HS_KDF_ITER=None,HS_KDF_DGST=None,HS_KDF_HMAC=None,
                   HS_KEX=None,HS_KEX_MLEN=None,HS_ENC=None,HS_DGST=None):
    from ...util import logging
    global logger
    logger = logging.getLogger(LOGGER_STR.format(name,'update'),
                               os.path.join('accounts',f'{name}.log'))

    logger.info(f'updating account {name}...')
    home = os.path.abspath(get_accounts()[name]['home'])
    validate(home,passwd)

    logger.info(f'assembling config....')
    if conf is None: conf = ()
    elif not isinstance(conf,(tuple,list)): conf = (conf,)
    cnfpath = os.path.join(home,'.node.conf')
    cnfargs = {
        'tor' : { 
            'TOR_PORT':TOR_PORT,'TOR_PASS':TOR_PASS,'THS_DIR':THS_DIR,'THS_PORT':THS_PORT,
            'CHANNEL':CHANNEL,'PORT':PORT,'SOCK':SOCK },
        'synchronization' : { 'MODE':MODE,'RELAY':RELAY,'URL':URL },
        'security' : { 
            'CERT' : CERT, 'PGP_KEY_DIR' : PGP_KEY_DIR,'HS_SRC' : HS_SRC, 
            'HS_KDF':HS_KDF,'HS_KDF_DGST':HS_KDF_DGST,'HS_KDF_HMAC':HS_KDF_HMAC,'HS_KDF_ITER':HS_KDF_ITER,
            'HS_KEX':HS_KEX,'HS_KEX_MLEN':HS_KEX_MLEN,'HS_ENC':HS_ENC,'HS_DGST':HS_DGST }
        }
    _conf = CP.get_cp(conf=[cnfpath,*conf],kwargs={
            k : { m : n for m,n in cnfargs[k].items() if not n is None }
                for k in cnfargs })
    logger.info(f'updating account {name} with configuration:')
    _conf.write(logger.handlers[0].stream)
    with open(cnfpath,'w') as cnffile: _conf.write(cnffile)
        
    try: 
        home,passwd = move_home_content(home,passwd,newhome,newpasswd)
        if config.get('runtime','USER',fallback=None) == name:
            config.set('runtime','PASS',passwd)
            config.set('runtime','HOME',home)
        write_account(name,newhome,overwrite=True)
    except Exception as e:
        logger.warning(f'{e.__class__} on UPDATE of {name}: {e}')

def create_account(name:str,passwd:str,home:str=None,*,conf=None,
                   TOR_PORT=None,TOR_PASS=None,THS_DIR=None,THS_PORT=None,
                   CHANNEL=None,PORT=None,SOCK=None,MODE=None,RELAY=None,URL=None,
                   CERT=None,PGP_KEY_DIR=None,HS_SRC=None,
                   HS_KDF=None,HS_KDF_ITER=None,HS_KDF_DGST=None,HS_KDF_HMAC=None,
                   HS_KEX=None,HS_KEX_MLEN=None,HS_ENC=None,HS_DGST=None):
    from ...util import logging
    global logger
    logger = logging.getLogger(LOGGER_STR.format(name,'create'),
                               os.path.join('accounts',f'{name}.log'))
    if passwd is None:
        logger.critical('password not set, aborting')
        raise ConfigError('NULL password')
    if home is None: home = os.path.join(config.get('paths','WORKING_DIR'),name)
    home = os.path.abspath(home)
    write_account(name,home)
    logger.info(f'preparing user homedir at {home}...')
    try:
        os.makedirs(home,exist_ok=True)
        with io.BytesIO(SHA256.new(str.encode(passwd,'utf-8')).digest()) as src,\
             open(os.path.join(home,'.test.enc'),'wb') as dst: encrypt(src,dst,passwd)
    except Exception as e:
        logger.critical(f'{e.__class__} while preparing user homedir: {e}')
        raise
    update_account(name,passwd,conf=conf,
                   TOR_PORT=TOR_PORT,TOR_PASS=TOR_PASS,THS_DIR=THS_DIR,
                   THS_PORT=THS_PORT,CHANNEL=CHANNEL,PORT=PORT,SOCK=SOCK,
                   MODE=MODE,RELAY=RELAY,URL=URL,CERT=CERT,PGP_KEY_DIR=PGP_KEY_DIR,
                   HS_SRC=HS_SRC,HS_KEX=HS_KEX,HS_KEX_MLEN=HS_KEX_MLEN,HS_ENC=HS_ENC,
                   HS_DGST=HS_DGST,HS_KDF=HS_KDF,HS_KDF_ITER=HS_KDF_ITER,
                   HS_KDF_HMAC=HS_KDF_HMAC,HS_KDF_DGST=HS_KDF_DGST)
    
    lastname,lastpasswd = None,None
    try: 
        lastname,_,lastpasswd = config.get_user()
        logout(dry=True)
    except LoginStatusError: pass
    except Exception as e:
        logger.error(f'{e.__class__} while resolving current login state: {e}')
        raise
    try:
        login(name,passwd,dry=True)
        try: os.makedirs(os.path.dirname(config.get('tor','THS_SOCK')))
        except: pass
        peerdir = os.path.join(home,'data','peers')
        os.makedirs(peerdir,exist_ok=True)
        with open(os.path.join(peerdir,'blacklist'),'w'): pass
        logout(dry=True)
    except Exception as e:
        logger.error(f'{e.__class__} while creating account: {e}')
        raise
    finally: 
        if not lastname is None:
            login(lastname,lastpasswd,dry=True)

def delete_account(name:str,passwd:str,*,full=True):
    from ...util import logging
    logger = logging.getLogger(LOGGER_STR.format(name,'delete'),
                               os.path.join('accounts',f'{name}.log'))
    logger.info(f'deleting account {name}, cleanup: {full}')
    if passwd is None:
        logger.critical('password not set, aborting')
        raise ValueError('NULL password')
    try:
        acc = get_accounts()[name]
        home = os.path.abspath(acc['home'])
        validate(home,passwd)
        clear_account(name)
        if(full):
            try: shutil.rmtree(home)
            except Exception as e:
                logger.error(f'{e.__class__} while trying to cleanup {home}: {e}')
    except KeyError: raise InvalidUsernameError(f'no entry for username {name}')
    except Exception as e:
        logger.error(f'{e.__class__} while deleting {name} account: {e}')
        raise
