import os,io
os = os
io = io
import re

from ...errors import UsernameTakenError
from ...config import config

import logging
from ... import logger as root_logger
logger = logging.getLogger(f'{root_logger.name}.daemon.accounts')

userspath = os.path.abspath(
                os.path.join(
                    config.get('paths','WORKING_DIR'),
                         '.users'))

__users_header__ = """#            .USERS FILE REFERENCE
# user entries will be automatically added here
# each valid user entry must adhere to pattern:
#     <user name> = <user home directory>
# invalid entries, empty lines and comments are
# ignored (invalid entries also issue log.warn)
"""
def __prep_env__():
    if not os.path.exists(userspath):
        logger.info('no .users file found, creating...')
        with open(userspath,'w') as userfile: userfile.write(__users_header__)

IGNORE = re.compile('^\\s*(#.*)?$')

def __parse_line__(line):
    if IGNORE.match(line): return None
    try: 
        name,home = line.split('=')
        name = name.lstrip().rstrip()
        home = home.lstrip().rstrip()
        logger.debug(f'parsed line: {name}, {home}')
    except: 
        logger.warn(f'invalid entry: {line}')
        raise
    return name,home

def clear_account(name):
    logger.info(f'clearing account: {name}')
    try:
        buf = io.StringIO()
        with open(userspath,'r') as userfile:
            for line in userfile:
                try: _name,_home = __parse_line__(line)
                except: pass
                else:
                    if name == _name: continue
                buf.write(line)
        with open(userspath,'w') as userfile:
            buf.seek(0)
            userfile.writelines(buf.readlines())
    except Exception as e:
        logger.error(e)
        raise
    finally: buf.close()

def write_account(name,home,*,overwrite=False):
    __prep_env__()
    try:
        buf = io.StringIO()
        entry = f'{name} = {home}'
        logger.info(f'writing line: ${entry}')
        with open(userspath,'r+') as userfile:
            for line in userfile:
                try: _name,_home = __parse_line__(line)
                except: pass
                else:
                    if name == _name: 
                        logger.warning(f'parsed line matches ${entry}')
                        if overwrite: buf.write(f'{entry}\n')
                        else: raise UsernameTakenError(f'user {name} already exists') 
                        continue
                buf.write(line)
            userfile.write(f'{entry}\n')
        if overwrite:
            logger.info(f'rewriting {userspath}')
            with open(userspath,'w') as userfile:
                buf.seek(0)
                userfile.writelines(buf.readlines())
    except Exception as e:
        logger.error(e)
        raise
    finally: buf.close()

def get_accounts():
    __prep_env__()
    try:
        logger.info('fetching accounts')
        res = {}
        with open(userspath,'r') as userfile:
            for line in userfile:
                try: name,home = __parse_line__(line)
                except: continue
                res[name] = {'name':name,'home':home}
            return res
    except OSError as e:
        logger.error(e)
        raise
