import struct
import os

try:
    from Crypto.Cipher import AES
    from Crypto.Hash import SHA256
    try: from pbkdf2 import PBKDF2
    except: 
        import io
        #from hashlib import md5
        def kdf(password,salt,key_len,hasher):
            out = io.BytesIO()
            di  = b''
            while out.tell() < key_len:
                di = hasher.new(di+password+salt).digest()
                out.write(di)
            out.seek(0)
            res = out.read(key_len)
            out.close()
            return res

    SALT_MARKER = b'$'
    ITERATIONS = 1000
        

    def encrypt(infile, outfile, password, key_size=32, salt_marker=SALT_MARKER,
            kdf_iterations=ITERATIONS, hashmod=SHA256):
        """Encrypt infile and write it to outfile using password to generate key.
        The encryption algorithm used is symmetric AES in cipher-block chaining
        (CBC) mode.
        ``key_size`` may be 16, 24 or 32 (default).
        The key is derived via the PBKDF2 key derivation function (KDF) from the
        password and a random salt of 16 bytes (the AES block size) minus the
        length of the salt header (see below).
        The hash function used by PBKDF2 is SHA256 per default. You can pass a
        different hash function module via the ``hashmod`` argument. The module
        must adhere to the Python API for Cryptographic Hash Functions (PEP 247).
        PBKDF2 uses a number of iterations of the hash function to derive the key,
        which can be set via the ``kdf_iterations` keyword argumeent. The default
        number is 1000 and the maximum 65535.
        The header and the salt are written to the first block of the encrypted
        file. The header consist of the number of KDF iterations encoded as a
        big-endian word bytes wrapped by ``salt_marker`` on both sides. With the
        default value of ``salt_marker = b'$'``, the header size is thus 4 and the
        salt 12 bytes. The salt marker must be a byte string of 1-6 bytes length.
        The last block of the encrypted file is padded with up to 16 bytes, all
        having the value of the length of the padding.
        """ 
        if not 1 <= len(salt_marker) <= 6:
            raise ValueError('The salt_marker must be one to six bytes long.')
        elif not isinstance(salt_marker, bytes):
            raise TypeError('salt_marker must be a bytes instance.')

        if kdf_iterations >= 65536:
            raise ValueError('kdf_iterations must be <= 65535.')

        password = str.encode(password)

        bs = AES.block_size
        header = salt_marker + struct.pack('>H', kdf_iterations) + salt_marker
        salt = os.urandom(bs - len(header))
        try: key = PBKDF2(password,salt,kdf_iterations,hashmod).read(key_size)
        except: key = kdf(password,salt,key_size,hashmod) 
        iv = os.urandom(bs)
        cipher = AES.new(key,AES.MODE_CBC, iv)
        outfile.write(header + salt)
        outfile.write(iv)
        finished = False

        while not finished:
            chunk = infile.read(1024 * bs)

            if len(chunk) == 0 or len(chunk) % bs != 0:
                padding_length = (bs - len(chunk) % bs) or bs
                chunk += (padding_length * chr(padding_length)).encode()
                finished = True

            outfile.write(cipher.encrypt(chunk))
        infile.seek(0)
        outfile.seek(0)

    def decrypt(infile, outfile, password, key_size=32, salt_marker=SALT_MARKER,
            hashmod=SHA256):
        """Decrypt infile and write it to outfile using password to derive key.
        See `encrypt` for documentation of the encryption algorithm and parameters.
        """
        mlen = len(salt_marker)
        hlen = mlen * 2 + 2

        if not 1 <= mlen <= 6:
            raise ValueError('The salt_marker must be one to six bytes long.')
        elif not isinstance(salt_marker, bytes):
            raise TypeError('salt_marker must be a bytes instance.')
        
        password = str.encode(password)
        
        bs = AES.block_size
        salt = infile.read(bs)

        if salt[:mlen] == salt_marker and salt[mlen + 2:hlen] == salt_marker:
            kdf_iterations = struct.unpack('>H', salt[mlen:mlen + 2])[0]
            salt = salt[hlen:]
        else:
            kdf_iterations = ITERATIONS

        if kdf_iterations >= 65536:
            raise ValueError('kdf_iterations must be <= 65535.')

        iv = infile.read(bs)
        try: key = PBKDF2(password,salt,kdf_iterations,hashmod).read(key_size)
        except: key = kdf(password,salt,key_size,hashmod)
        cipher = AES.new(key, AES.MODE_CBC, iv)
        next_chunk = b''
        finished = False

        while not finished:
            chunk, next_chunk = next_chunk, cipher.decrypt(infile.read(1024 * bs))

            if not next_chunk:
                padlen = chunk[-1]
                if isinstance(padlen, str):
                    padlen = ord(padlen)
                    padding = padlen * chr(padlen)
                else:
                    padding = (padlen * chr(chunk[-1])).encode()

                if padlen < 1 or padlen > bs:
                    raise ValueError("bad decrypt pad (%d)" % padlen)

                # all the pad-bytes must be the same
                if chunk[-padlen:] != padding:
                    # this is similar to the bad decrypt:evp_enc.c
                    # from openssl program
                    raise ValueError("bad decrypt")

                chunk = chunk[:-padlen]
                finished = True

            outfile.write(chunk)
        infile.seek(0)
        outfile.seek(0)
except ImportError:
    def encrypt(infile,outfile,password):
        outfile.write(infile.read())
        infile.seek(0)
        outfile.seek(0)
    def decrypt(infile,outfile,password):
        outfile.write(infile.read())
        infile.seek(0)
        outfile.seek(0)
