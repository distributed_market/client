from ..config import config

import sys
import logging
logger = logging.getLogger(__name__)
try: logger.addHandler(logging.FileHandler(os.path.join(
            config.get('paths','LOGGING_DIR'),'security.log'),'w'))
except: logger.addHandler(logging.StreamHandler(sys.stdout))

def init_security():
    for module in config.get('security','HS_SRC',fallback='').split(','):
        try:
