DISTORTION_VERSION = '0.1.0'
DISTORTION_SOURCE  = {
        'NODE' : './node',
        'GUI'  : '../gui',
        'CLI'  : './cli'
}
DISTORTION_CONFIG  = './etc'

import sys
sys = sys

import logging
logger = logging.getLogger('install')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))

import argparse
argparse = argparse
import configparser
configparser = configparser
class CPStore(argparse.Action):
    def __init__(self,option_strings,dest,**kwargs):
        super().__init__(option_strings,dest[1],**kwargs)
        self.confref = dest
    def __call__(self,parser,namespace,values,option_strings=None):
        if self.nargs == 0: values = 'True'
        cp.set(self.confref[0],self.confref[1],values)
        return namespace

PREFIX      = (configparser.DEFAULTSECT,'PREFIX')

DATA_DIR    = ('paths','DATA_DIR')
INSTALL_DIR = ('paths','INSTALL_DIR')
CONFIG_DIR  = ('paths','CONFIG_DIR')
LOGGING_DIR = ('paths','LOGGING_DIR')
WORKING_DIR = ('paths','WORKING_DIR')
TMP_DIR     = ('paths','TMP_DIR')
PIDFILE     = ('paths','PIDFILE')

CONFIGMODS  = ('global','CONFIGMODS')
VERSION     = ('global','VERSION')
RETENV      = ('global','RETENV')
DATALANG    = ('global','DATALANG')


def prepare_configparser():
    cp = configparser.ConfigParser(allow_no_value=True,
                                   empty_lines_in_values=False,
                                   interpolation=
                                        configparser.ExtendedInterpolation())
    cp.default_section = configparser.DEFAULTSECT
    cp.optionxform = str 
    cp.add_section('paths')
    cp.add_section('global')
    cp.add_section('components')
    cp.add_section('system')

    cp.set(configparser.DEFAULTSECT,'DEFAULT_CONFMODS','api,node')
    cp.set(*VERSION,DISTORTION_VERSION)
    cp.set(*DATALANG,'json')
    cp.set(*CONFIGMODS,'${DEFAULT_CONFMODS}')

    return cp

def prepare_commands(ap):
    ap.add_argument('-p','--platform',
                    dest='platform',choices=['Linux','Windows','Darwin'],
                    help='specifies target platform installation layout')
    cmd = ap.add_mutually_exclusive_group(required=True)
    cmd.add_argument('-i','--install',
                     dest='command',action='store_const',
                     const='install',help='performs fresh install with specified configuration')
    cmd.add_argument('-u','--uninstall',
                     dest='command',action='store_const',
                     const='uninstall',help='cleans up associated/specified installation')
    cmd.add_argument('-r','--reinstall',
                     dest='command',action='store_const',
                     const='reinstall',help='attempts to refresh associated/specified installation')

def prepare_paths(ap):
    _paths = ap.add_argument_group('paths')
    _paths.add_argument('--prefix',
                    action=CPStore,
                    dest=PREFIX,
                    help='installation directories common prefix')
    _paths.add_argument('--data-dir',
                    action=CPStore,
                    dest=DATA_DIR,
                    help='source files directory')
    _paths.add_argument('--install-dir',
                    action=CPStore,
                    dest=INSTALL_DIR,
                    help='executables directory')
    _paths.add_argument('--config-dir',
                    action=CPStore,
                    dest=CONFIG_DIR,
                    help='configuration directory')
    _paths.add_argument('--logging-dir',
                    action=CPStore,
                    dest=LOGGING_DIR,
                    help='runtime-logging directory')
    _paths.add_argument('--working-dir',
                    action=CPStore,
                    dest=WORKING_DIR,
                    help='directory holding runtime-generated, user-specific data')

def prepare_globals(ap):
    _global = ap.add_argument_group('globals')
    _global.add_argument('--configmods',
                    action=CPStore,
                    dest=CONFIGMODS,
                    help='comma-separated list of configuration modules to load on daemon start; unless you\'ve really got a reason, use ${DEFAULT_CONFMODS} as the first argument')
    _global.add_argument('--retent-env-vars',
                    action=CPStore,
                    dest=RETENV,
                    help='comma-separated list of environment variables to be copied into [env] section of daemon config')
    _global.add_argument('--data-languages',
                    action=CPStore,
                    dest=DATALANG,
                    help='comma-separated list of data tranmission languages (modules) handled by daemon, defaults to json; any changes may require additional configuration modules')

if __name__ == '__main__':
    from sys import argv

    cp = prepare_configparser()
    ap = argparse.ArgumentParser()
    prepare_commands(ap)
    cmds = 4 if argv[1]=='-p' \
            else 2
    pa = ap.parse_args(argv[1:cmds])

    if pa.platform is None:
        import platform
        pa.platform = platform.system()
    import importlib
    setupmod = importlib.import_module(f'setup-{pa.platform}')
    cp.set('system','PLATFORM',pa.platform)
    
    ap = argparse.ArgumentParser(f'{argv[0]} --{pa.command}')
    prepare_paths(ap)
    prepare_globals(ap)

    _comp = ap.add_argument_group(f'components')
    for k,v in DISTORTION_SOURCE.items():
        _comp.add_argument(f'--disable-{k.lower()}',
                           dest=k,action='store_const',
                           const=True,default=False,
                           help=f'if specified, component {k} will not be installed')

    _system = ap.add_argument_group(f'{pa.platform}-specific')
    for k,v in setupmod.ARGUMENTS.items():
        _system.add_argument(k,dest=('system',k[2:].replace('-','_').upper()),
                             action=CPStore,**v)
    pa2 = ap.parse_args(argv[cmds:])
    action = getattr(setupmod,pa.command)
    for k,v in DISTORTION_SOURCE.items():
        if not getattr(pa2,k):
            cp.set('components',k,v)
    for k,v in action.DEFAULTS.items():
        try: cp.set(*k,cp.get(*k,fallback=v))
        except configparser.InterpolationError:
            cp.set(*k,cp.get(*k,raw=True))
    
    try: action(cp)
    except: setupmod.uninstall(cp,ignore_err=True)
