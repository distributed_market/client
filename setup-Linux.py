import importlib
setupmod = importlib.import_module('setup')
DISTORTION_CONFIG = setupmod.DISTORTION_CONFIG
PREFIX = setupmod.PREFIX
DATA_DIR = setupmod.DATA_DIR
INSTALL_DIR = setupmod.INSTALL_DIR
CONFIG_DIR = setupmod.CONFIG_DIR
LOGGING_DIR = setupmod.LOGGING_DIR
WORKING_DIR = setupmod.WORKING_DIR
TMP_DIR = setupmod.TMP_DIR
PIDFILE = setupmod.PIDFILE
CONFIGMODS = setupmod.CONFIGMODS
VERSION = setupmod.VERSION
RETENV = setupmod.RETENV
DATALANG = setupmod.DATALANG
configparser = setupmod.configparser 

import os,sys,shutil,pathlib
os = os
sys = sys
shutil = shutil

ARGUMENTS = {
}

import logging
logger = logging.getLogger('distortion')
try: logger.addHandler(logging.FileHandler(
            f'/var/log/distortion.install','w'))
except PermissionError: logger.addHandler(logging.StreamHandler(sys.stdout))
logger.setLevel(logging.INFO)
logger.handlers[0].setFormatter(logging.Formatter(
    '[%(asctime)s]%(name)s:%(levelname)s## %(message)s'))

__success__ = (0,None)

def __copy__(src,dst):
    if isinstance(src,str): src = pathlib.Path(src)
    path = getattr(src,'path',src)
    if src.is_symlink(): return
    os.makedirs(dst,exist_ok=True)
    dst = os.path.join(dst,src.name)
    if src.is_dir():
        os.makedirs(dst,exist_ok=True)
        for entry in os.scandir(src): __copy__(entry,dst)
    elif src.is_file():
        if src.name == '__main__.py':
            with open(path,'r') as infile, \
                 open(dst,'w') as outfile:
                outfile.write(f'#!{sys.executable}\n\n')
                outfile.write(infile.read())
            os.popen(f'chmod +x {dst}').close()
        else: shutil.copy(path,dst)

def __prep_mods__(cp,logger):
    logger.info('copying data files...')
    expath = os.path.abspath(os.path.join(cp.get(*DATA_DIR),'__main__.py'))
    mainpath = os.path.abspath(os.path.join(cp.get(*INSTALL_DIR),'distortion'))
    LINE = 'PYTHONPATH="{}" "{}" -m {} {}'
    with open('__main__.py','r') as infile, open(expath,'w') as outfile:
        outfile.write(f'#!{sys.executable}\n')
        outfile.write(f'LINE = \'{LINE}\'\n\n')
        outfile.writelines(infile.readlines())
    task = os.popen(f'chmod -v 0755 {expath}')
    logger.info(task.read())
    if not task.close() in __success__:
        raise OSError(f'failed to set executable flag on {expath}')
    os.symlink(expath,mainpath)
    for cname,csrc in cp['components'].items():
        if cname in cp[cp.default_section]: continue
        path = os.path.abspath(os.path.join(cp.get(*DATA_DIR),cname.lower()))
        logger.info(f'{cname} data: {csrc} -> {path}')
        expath = f'{mainpath}-{cp.get(*VERSION)}-{cname.lower()}'
        os.makedirs(path,exist_ok=True)
        for entry in os.scandir(csrc): __copy__(entry,path)
        line = LINE.format(os.path.abspath(cp.get(*DATA_DIR)),sys.executable,
                           cname.lower(),'$@')
        with open(expath,'w') as exe: exe.write(f'#!/bin/sh\n{line}\n')
        task = os.popen(f'chmod -v 0755 {expath}')
        logger.info(task.read())
        if not task.close() in __success__: 
            raise OSError(f'failed to set executable flag on {expath}')
        cp.set('components',cname,f'{path},{expath}')

def __prep_data__(cp,logger):
    try:
        os.makedirs(cp.get(*DATA_DIR),exist_ok=True)
        os.makedirs(cp.get(*INSTALL_DIR),exist_ok=True)
        os.makedirs(cp.get(*CONFIG_DIR),exist_ok=True)
        os.makedirs(os.path.dirname(cp.get(*PIDFILE)),exist_ok=True)
    except Exception as e:
        logger.error(f'{e.__class__} encountered while preparing environment: {e}')
        raise
    try: os.makedirs(cp.get(*TMP_DIR),exist_ok=True)
    except configparser.InterpolationError:
        logger.warning(f'{cp.get(*TMP_DIR,raw=True)} depends on runtime variables, skipped')
    try: os.makedirs(cp.get(*LOGGING_DIR),exist_ok=True)
    except configparser.InterpolationError:
        logger.warning(f'{cp.get(*LOGGING_DIR,raw=True)} depends on runtime variables, skipped')
    try: os.makedirs(cp.get(*WORKING_DIR),exist_ok=True)
    except configparser.InterpolationError:
        logger.warning(f'{cp.get(*WORKING_DIR,raw=True)} depends on runtime variables, skipped')

    logger.info('copying config files...')
    for entry in os.scandir(DISTORTION_CONFIG):
        __copy__(entry,cp.get(*CONFIG_DIR))
        __copy__(entry,os.path.join(cp.get(*CONFIG_DIR),'default'))

def __prep_config__(cp,logger):
    for cname,csrc in cp['components'].items():
        if cname in cp[cp.default_section]: continue
        path,_ = csrc.split(',')
        path = os.path.join(path,'config','assets')
        logger.info(f'config file location: {path}')
        os.makedirs(path,exist_ok=True)
        with open(os.path.join(path,'daemon.conf'),'w') as daemonconf, \
             open(os.path.join(path,'global.conf'),'w') as globconf:
            logger.info('writing paths...')
            daemonconf.write('[paths]\n')
            for oname in cp['paths']:
                if oname in cp[cp.default_section]: continue
                try: daemonconf.write(
                        f'{oname} = {os.path.abspath(cp.get("paths",oname))}\n')
                except configparser.InterpolationError:
                    daemonconf.write(f'{oname} = {cp.get("paths",oname,raw=True)}\n')
            logger.info('writing globals...')
            for sname in 'global','components','system':
                globconf.write(f'[{sname}]\n')
                for oname in cp[sname]:
                    if oname in cp[cp.default_section]: continue
                    try: globconf.write(f'{oname} = {cp.get(sname,oname)}\n')
                    except configparser.InterpolationError:
                        globconf.write(f'{oname} = {cp.get(sname,oname,raw=True)}\n')

def install(cp:configparser.ConfigParser,*,ignore_err=False):
    logger.info('installing with configuration:')
    cp.write(logger.handlers[0].stream)

    logger.info('configuring dependencies...')
    try: import Crypto
    except:
        task = os.popen('pip install pycrypto')
        logger.info(task.read())
        if not task.close() in __success__:
            raise RuntimeError('failed to install dependencies')

    logger.info('installing...')
    try: 
        __prep_data__(cp,logger)
        __prep_mods__(cp,logger)
    except Exception as e:
        logger.error(f'{e.__class__.__name__}: {e}')
        if not ignore_err: raise

    logger.info('saving configuration...')
    try: __prep_config__(cp,logger)
    except Exception as e:
        logger.error(f'{e.__class__.__name__}: {e}')
        if not ignore_err: raise
    
install.DEFAULTS = {
        PREFIX : '/usr/local',
        DATA_DIR : '${PREFIX}/lib/distortion-${global:VERSION}',
        INSTALL_DIR : '${PREFIX}/bin',
        CONFIG_DIR : '${PREFIX}/etc/distortion-${global:VERSION}',
        LOGGING_DIR : '/var/log/distortion',
        WORKING_DIR : '${env:HOME}/.distortion',
        PIDFILE : '${TMP_DIR}/distortion.lock',
        RETENV : 'USER,HOME,LANGUAGE',
        TMP_DIR : '/tmp/distortion-${global:VERSION}',
}

def uninstall(cp:configparser.ConfigParser,*,ignore_err=False):
    logger.info('uninstalling with configuration:')
    cp.write(logger.handlers[0].stream)

    for cname,csrc in cp['components'].items():
        if cname in cp[cp.default_section]: continue
        logger.info(f'stopping currently running {cname} process...')
        try:
            task = os.popen(f'{cp.get(*INSTALL_DIR)}/distortion {cname.lower()} -v --stop')
            logger.info(task.read())
            if not task.close() in __success__: 
                raise OSError(f'failed to stop running daemon')
        except Exception as e:
            logger.error(f'{e.__class__.__name__}: {e}')
            if not ignore_err: raise

    logger.info('unlinking binaries...')
    try: 
        task = os.popen(f'rm -fv {cp.get(*INSTALL_DIR)}/distortion*')
        logger.info(task.read())
        if not task.close() in __success__: raise OSError('failed to unlink binaries')
        try: os.removedirs(cp.get(*INSTALL_DIR))
        except: pass
    except Exception as e:
        logger.error(f'{e.__class__.__name__}: {e}')
        if not ignore_err: raise
    
    for opt in CONFIG_DIR,LOGGING_DIR,DATA_DIR,TMP_DIR,WORKING_DIR:
        logger.info(f'cleaning {opt[1]}...')
        try:
            task = os.popen(f'rm -fr {cp.get(*opt)}/*')
            logger.info(task.read())
            if not task.close() in __success__: 
                raise OSError(f'failed to cleanup {opt[1]}')
            try: os.removedirs(cp.get(*opt))
            except: pass
        except configparser.InterpolationError:
            logger.warn(f'{cp.get(*opt,raw=True)} requires manual cleanup')
        except Exception as e:
            logger.error(f'{e.__class__.__name__}: {e}')
            if not ignore_err: raise
   
uninstall.DEFAULTS = { }
def __prep_defaults__():
    _cp = configparser.ConfigParser()
    try:
        with open(os.path.join(
                    os.path.abspath(
                        os.path.dirname(__file__)),
                    'config',
                    'assets',
                    'global.conf'),
                'r') as conf_file:
           _cp.read_file(conf_file)
        for conf_path in [i for i in [
                            os.getenv('DISTORTION_CONF'),
                            _cp.get('global','DISTORTION_CONF',fallback=None),
                            os.path.join(
                                os.path.abspath(
                                    os.path.dirname(__file__)),
                                'config',
                                'assets',
                                'daemon.conf')] if i]:
            with open(conf_path,'r') as conf_file:
                _cp.read_file(conf_file)
                break
        else: raise FileNotFoundError('daemon.conf not found')
    except IOError:
        distpath = shutil.which('distortion')
        if not distpath is None:
            task = os.popen(f'{distpath} --config')
            _cp.read_file(task)
            task.close()
    for sname,section in _cp.items():
       for oname,option in section.items():
           uninstall.DEFAULTS[sname,oname] = option
__prep_defaults__()
if len(uninstall.DEFAULTS) == 0: uninstall.DEFAULTS = install.DEFAULTS

def reinstall(cp:configparser.ConfigParser,*,ignore_err=False):
    raise NotImplementedError('please use Gitlab sources instead')
    #uninstall(cp,ignore_err=True)
    #install(cp,ignore_err=ignore_err)
reinstall.DEFAULTS = uninstall.DEFAULTS
