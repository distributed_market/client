import os,sys,shutil
os = os
shutil = shutil

import importlib
setupmod = importlib.import_module('setup')

configparser = setupmod.configparser

ARGUMENTS = {
        '--create-user' : {
            'nargs' : 0,
            'help'  : 'specifies if new user is to be created for Distortion daemon'
        },'--create-group' : {
            'nargs' : 0,
            'help'  : 'specifies if new group is to be created for Distortion daemon'
        },'--group-users' : {
            'help'  : 'comma-separated list of users to add to daemon control group'
        }
}

def install(cp:configparser.ConfigParser,*,ignore_err=False):
    cp.write(sys.stdout)
install.DEFAULTS = {
        setupmod.PREFIX : '/usr/local',
        setupmod.DATA_DIR : '${PREFIX}/lib/distortion-${global:VERSION}',
        setupmod.INSTALL_DIR : '${PREFIX}/bin',
        setupmod.CONFIG_DIR : '${PREFIX}/etc/distortion-${global:VERSION}',
        setupmod.LOGGING_DIR : '/var/log/distortion',
        setupmod.WORKING_DIR : '${env:HOME}/.distortion',
        setupmod.RETENV : 'USER,HOME,LANGUAGE',
        ('system','CREATE_USER') : 'True',
        ('system','CREATE_GROUP') : 'True',
}

def uninstall(cp:configparser.ConfigParser,*,ignore_err=False):
    cp.write(sys.stdout)
uninstall.DEFAULTS = {
}
_cp = configparser.ConfigParser()
try:
    #print('opening global.conf...')
    with open(os.path.join(
                os.path.abspath(
                    os.path.dirname(__file__)),
                'config',
                'assets',
                'global.conf'),
            'r') as conf_file:
       _cp.read_file(conf_file)
    #print('searching for daemon.conf...')
    for conf_path in [i for i in [
                        os.getenv('DISTORTION_CONF'),
                        _cp.get('global','DISTORTION_CONF',fallback=None),
                        os.path.join(
                            os.path.abspath(
                                os.path.dirname(__file__)),
                            'config',
                            'assets',
                            'daemon.conf')] if i]:
        #print(f'  checking {conf_path}...')
        with open(conf_path,'r') as conf_file:
            #print('  ok')
            _cp.read_file(conf_file)
            break
    else: raise FileNotFoundError('daemon.conf not found')
except IOError:
    #print('searching for distortion command...')
    distpath = shutil.which('distortion')
    if not distpath is None:
        #print(f'  found: {distpath}')
        _cp.read_file(os.popen(f'{distpath} --config'))

for sname,section in _cp.items():
   for oname,option in section.items():
       uninstall.DEFAULTS[sname,oname] = option

def reinstall(cp:configparser.ConfigParser,*,ignore_err=False):
    uninstall(cp,ignore_err=True)
    install(cp,ignore_err=ignore_err)
reinstall.DEFAULTS = uninstall.DEFAULTS
