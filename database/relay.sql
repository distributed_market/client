set GLOBAL event_scheduler = ON;

create schema
if not exists
    RELAYDB;

grant usage
    on RELAYDB.*
    to 'RELAYDBroot'@'localhost';
drop user
    'RELAYDBroot'@'localhost';
create user
    'RELAYDBroot'@'localhost'
    identified by password '';

grant usage
    on RELAYDB.*
    to 'request'@'%';
drop user
    'request'@'%';
create user
    'request'@'%'
    identified by password '';

use RELAYDB;

--- USER/TRANSACTION MANAGEMENT ---
--- table declarations
create table
if not exists
    USERS (
        id      int             auto_increment,
        
        login   varchar(120)    not null,
        cert    varbinary(4096) not null,
        service varchar(120)    not null unique,
        
        status  bit(1)          not null default 1,
        lastact datetime,
        
        constraint id_user
            primary key(id)
    );
create table
if not exists
    AUCTIONS (
        id      int          auto_increment,
        vendor  int          not null,
        
        title   varchar(120) not null,
        descr   tinytext     not null,
        
        ppu     float        not null,
        min_q   int          null,
        max_q   int          null,

        constraint id_auction
            primary key (id),
        constraint fk_auctions_vendor_users
            foreign key (vendor)
            references USERS(id)
            on update CASCADE
            on delete CASCADE
    );
--- view declarations
create or replace view
    USERS_ACTIVE as
        select login,cert,service
            from  USERS
            where status = 1;
create or replace view
    USERS_INACTIVE as
        select login,cert,service,lastact
            from  USERS
            where status = 0;
--- external interaction API
--delimiter //
drop procedure
if exists
    USERS_DEACTIVATE;
create procedure USERS_DEACTIVATE(service varchar(120))
    update USERS as u
        set   u.status  = 0,
              u.lastact = now()
        where u.service like service;
drop procedure
if exists
    USERS_ACTIVATE;
create procedure USERS_ACTIVATE(service varchar(120))
    update RELAYS as u
        set   u.status = 1
        where u.service like service;
--delimiter ;
--- lifetime management
--delimiter //
create event
if not exists
    USERS_DELETEINACTIVE
    on schedule every 5 MINUTE
    do
        delete from USERS_INACTIVE
            where timestampdiff(MINUTE,
                                lastact,
                                now()) >= 5;
--delimiter ;

grant all privileges
    on   RELAYDB.*
    to   'RELAYDBroot'@'localhost';

grant select
    on   RELAYDB.*
    to   'request'@'%';

flush privileges;
